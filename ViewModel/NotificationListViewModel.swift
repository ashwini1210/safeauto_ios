//
//  AllNoficicationViewModel.swift
//  SafeAuto
//
//  Created by ashwini on 28/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class NotificationListViewModel: NSObject {
    
    var noficicationViewController : NoficicationListViewController? = nil
    
    init(noticicationListViewController : NoficicationListViewController?) {
        super.init()
        guard let allNoficicationVC =  noticicationListViewController else {
            return
        }
        self.noficicationViewController = allNoficicationVC
    }
    
    /*
     * Method to create left Button of Navigation Bar
     */
    func addLeftBarButtonItem() -> [UIBarButtonItem]?{
        var navBarButtonItem : [UIBarButtonItem] = []
        var leftBarButtonItem1: UIBarButtonItem? = nil
        
        leftBarButtonItem1 = AppUtils.createFontAwesomeButton(buttonName: "chevronleft", viewController: noficicationViewController!, buttTag: AppConstants.BACK_BUTTON)
        if(leftBarButtonItem1 != nil){
            navBarButtonItem.append(leftBarButtonItem1!)
        }
        
        //Add fixedSpace to make titleview in centre
        let fixedItem = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        fixedItem.width = 40
        navBarButtonItem.append(fixedItem)
        
        //Add flexibleSpace to make titleview in centre
        let flexibleSpace = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        flexibleSpace.width = 40
        navBarButtonItem.append(flexibleSpace)
        
        return navBarButtonItem
    }
    
    /*
     * Method to create Right Button of Navigation Bar
     */
    func addRightBarButtonItem() -> [UIBarButtonItem]?{
        var navBarButtonItem : [UIBarButtonItem] = []
        
        var rightBarButtonItem1: UIBarButtonItem? = nil
        var rightBarButtonItem2: UIBarButtonItem? = nil
        
        rightBarButtonItem1 = AppUtils.createFontAwesomeButton(buttonName: "bell", viewController: noficicationViewController!, buttTag: AppConstants
            .NAVIGATION_NOTIFICATION_BUTTON)
        if(rightBarButtonItem1 != nil){
            navBarButtonItem.append(rightBarButtonItem1!)
        }
        
        rightBarButtonItem2 = AppUtils.createFontAwesomeButton(buttonName: "addresscard", viewController: noficicationViewController!, buttTag: AppConstants.NAVIGATION_ADDRESS_CARD_BUTTON)
        if(rightBarButtonItem2 != nil){
            navBarButtonItem.append(rightBarButtonItem2!)
        }
        return navBarButtonItem
    }
    
    /*
     * Method to add text in centre of Navigation Bar
     */
    func addTextInTitleBar() -> UILabel?{
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        label.numberOfLines = 0
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 10.0, weight: .bold)
        label.textAlignment = NSTextAlignment.center
        let formattedString = NSMutableAttributedString()
        formattedString.bold("Notifications", textSize: 20, textColor: UIColor.white)
        label.attributedText = formattedString
        return label
        
    }
    
    /*
     * Method to resize labels of Header view
     */
    func resizeLabels(){
        if noficicationViewController != nil {
            noficicationViewController!.dateLabel.frame = CGRect(x: 0, y: 0, width: 25 * noficicationViewController!.view.bounds.width/100, height: noficicationViewController!.dateLabel.frame.height)
            noficicationViewController!.typeLabel.frame = CGRect(x: 25 * noficicationViewController!.view.bounds.width/100, y: 0, width: 18 * noficicationViewController!.view.bounds.width/100, height: noficicationViewController!.typeLabel.frame.height)
            noficicationViewController!.descriptionLabel.frame = CGRect(x: 43 * noficicationViewController!.view.bounds.width/100, y: 0, width: 57 * noficicationViewController!.view.bounds.width/100, height: noficicationViewController!.descriptionLabel.frame.height)
            
        }
    }
    
    /*
     * add border to view
     */
    func addBorderToView(){
        if noficicationViewController != nil {
            noficicationViewController!.headerView.addTopBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#e5e5e5"), width: 1)
            noficicationViewController!.dateLabel.addLeftBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#e5e5e5"), width: 1)
            noficicationViewController!.typeLabel.addLeftBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#e5e5e5"), width: 1)
            noficicationViewController!.descriptionLabel.addRightBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#e5e5e5"), width: 1)
            noficicationViewController!.descriptionLabel.addLeftBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#e5e5e5"), width: 1)
        }
    }
}
