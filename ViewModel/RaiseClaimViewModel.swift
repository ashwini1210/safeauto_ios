//
//  RaiseClaimViewModel.swift
//  SafeAuto
//
//  Created by ashwini on 05/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit
import CoreLocation

class RaiseClaimViewModel: NSObject {

    var raiseAClaimVC : RaiseAClaimViewController? = nil
    
    init(raiseAClaimViewController : RaiseAClaimViewController?) {
        super.init()
        guard let raiseAClaimVC =  raiseAClaimViewController else {
            return
        }
        self.raiseAClaimVC = raiseAClaimVC
    }
    
    func setUpTableView(){
        self.raiseAClaimVC!.vehicleAffectedContainerView.addBorders(borderWidth: 1.0, borderColor: UIColor.lightGray.cgColor)
        self.raiseAClaimVC!.vehicleAffectedContainerView.roundCorners(cornerRadius: 10.0)
        self.raiseAClaimVC!.vehicleAffectedContainerViewHeightConstraint.constant = 35 + (70 * 3)
        self.raiseAClaimVC!.vehicleAffectedTableView.rowHeight = 70
       
    }
    
    func setUpPassengerInfoView(){
        let passengerInfoViewHeight = 250
        self.raiseAClaimVC?.passengerInfoContainerHeightConstraint.constant = CGFloat(passengerInfoViewHeight)
    self.raiseAClaimVC?.numberOfPassengerSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], for: UIControl.State.normal)
        self.raiseAClaimVC?.numberOfPassengerSegmentedControl.tintColor = UIColor.lightGray
        self.raiseAClaimVC?.numberOfPassengerSegmentedControl.selectedSegmentIndex = UISegmentedControl.noSegment
        self.raiseAClaimVC?.commentDescriptionTextView.addBorders()
        self.raiseAClaimVC?.passengerInfoContainerView.addBorders(borderWidth: 1.0, borderColor: UIColor.lightGray.cgColor)
        self.raiseAClaimVC?.passengerInfoContainerView.roundCorners(cornerRadius: 10.0)
    }
    
    func setUpUploadPhotoMainView(){
        let uploadPhotoMainViewInitialHeight = 120
        self.raiseAClaimVC?.uploadPhotoViewHeightConstraint.constant = CGFloat(uploadPhotoMainViewInitialHeight)
        self.raiseAClaimVC?.uploadPhotoCollectionView.isHidden = true
        self.raiseAClaimVC?.uploadPhotoSubView.isHidden = true
        self.raiseAClaimVC?.uploadPhotoView.addBorders(borderWidth: 1.0, borderColor: UIColor.lightGray.cgColor)
        self.raiseAClaimVC?.uploadPhotoView.roundCorners(cornerRadius: 10.0)
        
    }
    
    
    func setUpWhoWasDrivingView(){
        let whoWasDrivingTitleHeight = 35
        let whoWasDrivingTableViewHeight = 60 * 3
        let addUnlistedDriverBtnHeight = 40
        self.raiseAClaimVC?.whoWasDrivingTableViewHeightConstraint.constant = CGFloat(whoWasDrivingTableViewHeight)
        
        self.raiseAClaimVC?.whoWasDrivingContainerViewHeightConstraint.constant = CGFloat(whoWasDrivingTitleHeight + whoWasDrivingTableViewHeight + addUnlistedDriverBtnHeight)
        
        self.raiseAClaimVC?.addUnlistedDriverBtn.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
        self.raiseAClaimVC?.whoWasDrivingContainerView.addBorders(borderWidth: 1.0, borderColor: UIColor.lightGray.cgColor)
        self.raiseAClaimVC?.whoWasDrivingContainerView.roundCorners(cornerRadius: 10.0)
        
    }
    
    func setUpIncidentTimeAndLocation(){
    
        self.raiseAClaimVC?.incidentTimeAndLocationMainViewHeightConstraint.constant = 260
        self.raiseAClaimVC!.incidentTimeAndLocationMainView.addBorders(borderWidth: 1.0, borderColor: UIColor.lightGray.cgColor)
        self.raiseAClaimVC!.incidentTimeAndLocationMainView.roundCorners(cornerRadius: 10.0)
        self.raiseAClaimVC?.selectDateAndTimeJustNowImageView.image = UIImage(named: "checkBoxSquareIcon")
        self.raiseAClaimVC?.selectDateAndTimeJustNowImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
        self.raiseAClaimVC?.selectCurrentLocationImageView.image = UIImage(named: "checkBoxSquareIcon")
        self.raiseAClaimVC?.selectCurrentLocationImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
    }
    
   
    
    func reasonForClaimData(){
        let reasonForClaimModel1 = ReasonForClaimModel(reasonImageName: "carFrontGlassIcon", reasonName: "Glass", isReasonSelected: false)
        let reasonForClaimModel2 = ReasonForClaimModel(reasonImageName: "weatherIcon", reasonName: "Weather", isReasonSelected: false)
        let reasonForClaimModel3 = ReasonForClaimModel(reasonImageName: "animalIcon", reasonName: "Animal", isReasonSelected: false)
        let reasonForClaimModel4 = ReasonForClaimModel(reasonImageName: "theftIcon", reasonName: "Theft", isReasonSelected: false)
        let reasonForClaimModel5 = ReasonForClaimModel(reasonImageName: "colisionIcon", reasonName: "Collision", isReasonSelected: false)
        let reasonForClaimModel6 = ReasonForClaimModel(reasonImageName: "breakDownIcon", reasonName: "Breakdown", isReasonSelected: false)
        
        self.raiseAClaimVC!.reasonForClaimArr.append(reasonForClaimModel1)
        self.raiseAClaimVC!.reasonForClaimArr.append(reasonForClaimModel2)
        self.raiseAClaimVC!.reasonForClaimArr.append(reasonForClaimModel3)
        self.raiseAClaimVC!.reasonForClaimArr.append(reasonForClaimModel4)
        self.raiseAClaimVC!.reasonForClaimArr.append(reasonForClaimModel5)
        self.raiseAClaimVC!.reasonForClaimArr.append(reasonForClaimModel6)
        
    }
    
    func vehicleAffectedData(){
        let vehicleModel1 = VehicleModel(vehicleName: "2018 | BMW | Z4", vehicleCode: "LJCPCBLCX11000232", isVehicleAffected: false)
        let vehicleModel2 = VehicleModel(vehicleName: "2017 | Mercedes-Benz | 4Matic GLC", vehicleCode: "LJCPCBLCX11449823", isVehicleAffected: false)
        let vehicleModel3 = VehicleModel(vehicleName: "2016 | Audi | A4", vehicleCode: "LJCPCBLCX11263746", isVehicleAffected: false)
        
        self.raiseAClaimVC!.vehicleAffectedArr.append(vehicleModel1)
        self.raiseAClaimVC!.vehicleAffectedArr.append(vehicleModel2)
        self.raiseAClaimVC!.vehicleAffectedArr.append(vehicleModel3)
        
    }
    
    func needAssistanceData(){
        let needAssistanceModel1 = NeedAssistanceModel(needAssistanceTypeImageName: "ambulanceIcon", needAssistanceTypeName: "Ambulance", isAssistanceTypeSelected: false)
         let needAssistanceModel2 = NeedAssistanceModel(needAssistanceTypeImageName: "rentalReimbursementIcon", needAssistanceTypeName: "Rental Reimbursement", isAssistanceTypeSelected: false)
         let needAssistanceModel3 = NeedAssistanceModel(needAssistanceTypeImageName: "towIcon", needAssistanceTypeName: "Towing", isAssistanceTypeSelected: false)
         let needAssistanceModel4 = NeedAssistanceModel(needAssistanceTypeImageName: "bodyShopIcon", needAssistanceTypeName: "Body Shop", isAssistanceTypeSelected: false)
        
        self.raiseAClaimVC?.needAssistanceArr.append(needAssistanceModel1)
        self.raiseAClaimVC?.needAssistanceArr.append(needAssistanceModel2)
        self.raiseAClaimVC?.needAssistanceArr.append(needAssistanceModel3)
        self.raiseAClaimVC?.needAssistanceArr.append(needAssistanceModel4)
        
     //   self.raiseAClaimVC?.needAssistanceCollectionView.reloadData()

    }
    
    func whoWasDrivingData(){
        let whoWasDrivingModel1 = WhoWasDrivingObjectModel(driverName: "Ted Lawson", driverDateOfBirth: "11-09-1970", driverLicense: "NJ 123456789", driverProfileImage: "driver1", isDriverSelected: false)
        let whoWasDrivingModel2 = WhoWasDrivingObjectModel(driverName: "Jimmy Lawson", driverDateOfBirth: "112-01-1987", driverLicense: "NJ 543216789", driverProfileImage: "driver2", isDriverSelected: false)
        let whoWasDrivingModel3 = WhoWasDrivingObjectModel(driverName: "Martin Lawson", driverDateOfBirth: "16-06-1990", driverLicense: "NJ 123897645", driverProfileImage: "driver3", isDriverSelected: false)
        
        raiseAClaimVC?.whoWasDrivingArr.append(whoWasDrivingModel1)
        raiseAClaimVC?.whoWasDrivingArr.append(whoWasDrivingModel2)
        raiseAClaimVC?.whoWasDrivingArr.append(whoWasDrivingModel3)

    }
    
    //MARK: - Select Time and Date Just Now
    func selectIncidentTimeAndLocationJustNow(){
        
        if !(self.raiseAClaimVC?.isSelectDateAndTimeJustNowImageViewSelected)!{
            self.raiseAClaimVC?.isSelectDateAndTimeJustNowImageViewSelected = true
            self.raiseAClaimVC?.selectDateAndTimeJustNowImageView.image = UIImage(named: "checkBoxSquareSelectedIcon")
            self.raiseAClaimVC?.selectDateAndTimeJustNowImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
            let currentDate = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = AppConstants.DATE_FORMAT
            self.raiseAClaimVC?.showDateTextField.text = formatter.string(from: currentDate)
            
            
        }else{
            self.raiseAClaimVC?.isSelectDateAndTimeJustNowImageViewSelected = false
            self.raiseAClaimVC?.selectDateAndTimeJustNowImageView.image = UIImage(named: "checkBoxSquareIcon")
            self.raiseAClaimVC?.selectDateAndTimeJustNowImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
            self.raiseAClaimVC?.showDateTextField.text = ""
            
        }

    }
   
    func selectCurrentLocation() {
        let address: String? = getCurrentAddress()
        if !self.raiseAClaimVC!.isSelectCurrentLocationImageViewSelected{
            self.raiseAClaimVC!.isSelectCurrentLocationImageViewSelected = true
            self.raiseAClaimVC!.selectCurrentLocationImageView.image = UIImage(named: "checkBoxSquareSelectedIcon")
            self.raiseAClaimVC!.selectCurrentLocationImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
            if(address != nil){
                self.raiseAClaimVC!.showAddressTextField.text = address
            }
        }else{
            self.raiseAClaimVC!.isSelectCurrentLocationImageViewSelected = false
            self.raiseAClaimVC!.selectCurrentLocationImageView.image = UIImage(named: "checkBoxSquareIcon")
            self.raiseAClaimVC!.selectCurrentLocationImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
            self.raiseAClaimVC!.showAddressTextField.text = ""
        }
    }
    
    
    /*
     * Get current address based on lat long
     */
    func getCurrentAddress() -> String?{
        var address: String? = nil
        let city: String? = AppPreference.getInstance().getString(AppConstants.CURRENT_CITY, defaultvalue: "")
        let state: String? = AppPreference.getInstance().getString(AppConstants.CURRENT_STATE, defaultvalue: "")
        let country: String? = AppPreference.getInstance().getString(AppConstants.CURRENT_COUNTRY, defaultvalue: "")
        if(city != nil){
            address = city
        }
        if(state != nil){
            address = "\(address!), \(state!)"
        }
        if(country != nil){
            address = "\(address!), \(country!)"
        }
       return address
    }
    
    
}
