//
//  AddDriverViewModel.swift
//  SafeAuto
//
//  Created by ashwini on 03/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class AddDriverViewModel: NSObject {
    
    var addDriverVC : AddDriverViewController? = nil
    
    init(addDriverViewController : AddDriverViewController?) {
        super.init()
        guard let addDriverVC =  addDriverViewController else {
            return
        }
        self.addDriverVC = addDriverVC
    }
    
    func changeBorderOfSegmentView(){
        if(addDriverVC != nil){
        addDriverVC!.titleSegmentController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], for: UIControl.State.normal)
        addDriverVC!.titleSegmentController.tintColor = UIColor.lightGray
        addDriverVC!.titleSegmentController.selectedSegmentIndex = UISegmentedControl.noSegment

        addDriverVC!.suffixSegmentController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], for: UIControl.State.normal)
        addDriverVC!.suffixSegmentController.tintColor = UIColor.lightGray
        addDriverVC!.suffixSegmentController.selectedSegmentIndex = UISegmentedControl.noSegment

        addDriverVC!.genderSegmentController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], for: UIControl.State.normal)
        addDriverVC!.genderSegmentController.tintColor = UIColor.lightGray
        addDriverVC!.genderSegmentController.selectedSegmentIndex = UISegmentedControl.noSegment

        addDriverVC!.acciedentSegmentController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], for: UIControl.State.normal)
        addDriverVC!.acciedentSegmentController.tintColor = UIColor.lightGray
        addDriverVC!.acciedentSegmentController.selectedSegmentIndex = UISegmentedControl.noSegment

        addDriverVC!.noOfAccidentSegmentController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], for: UIControl.State.normal)
        addDriverVC!.noOfAccidentSegmentController.tintColor = UIColor.lightGray
        addDriverVC!.noOfvoilenceSegmentController.selectedSegmentIndex = UISegmentedControl.noSegment

        addDriverVC!.noOfvoilenceSegmentController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], for: UIControl.State.normal)
        addDriverVC!.noOfvoilenceSegmentController.tintColor = UIColor.lightGray
        addDriverVC!.noOfAccidentSegmentController.selectedSegmentIndex = UISegmentedControl.noSegment
        }
    }
    
    /*
     * Get selected title
     */
    func onTitleIndex(index: Int){
        switch index{
        case 0:
            print("Mr")
            break
        case 1:
            print("Mrs")
            break
        case 2:
            print("Miss")
            break
        case 3:
            print("Dr")
            break
        default: break
            
        }
    }
    
    func onSuffixIndex(index: Int){
        switch index{
        case 0:
            break
        case 1:
            break
        default: break
            
        }
    }
    
    func onGenderIndex(index: Int){
        switch index{
        case 0:
            break
        case 1:
            break
        case 2:
            break
        default: break
            
        }
    }
    
    func onAccidentIndex(index: Int){
        switch index{
        case 0:
            if(addDriverVC != nil){
               showAccidentView()
            }
            break
        case 1:
            if(addDriverVC != nil){
                hideAccidentView()
            }
            break
        default: break
            
        }
    }
    
    func onNoOfVoilenceIndex(index: Int){
        switch index{
        case 0:
            break
        case 1:
            break
        case 2:
            break
        case 3:
            break
        case 4:
            break
        default: break
            
        }
    }
    
    func onNoOfAccidentIndex(index: Int){
        switch index{
        case 0:
            break
        case 1:
            break
        case 2:
            break
        case 3:
            break
        case 4:
            break
        default: break
            
        }
    }
    
    /*
     * Show Accident View when accept about Accident or Violence
     */
    func showAccidentView(){
        self.addDriverVC!.accidentView.isHidden = false
        self.addDriverVC!.mainView.frame = CGRect(x: 0, y: 0, width: self.addDriverVC!.view.frame.width, height: self.addDriverVC!.informationView.frame.size.height + self.addDriverVC!.accidentView.frame.size.height)
        self.addDriverVC!.scrollView.contentSize = CGSize(width: self.addDriverVC!.view.frame.size.width, height: self.addDriverVC!.mainView.frame.size.height)
    }
    
    /*
     * Hide Accident View if Accident or Violence not done
     */
    func hideAccidentView(){
        self.addDriverVC!.accidentView.isHidden = true
        self.addDriverVC!.mainView.frame = CGRect(x: 0, y: 0, width: self.addDriverVC!.view.frame.width, height: self.addDriverVC!.informationView.frame.size.height)
        self.addDriverVC!.scrollView.contentSize = CGSize(width: self.addDriverVC!.view.frame.size.width, height: self.addDriverVC!.mainView.frame.size.height)
    }
    
}
