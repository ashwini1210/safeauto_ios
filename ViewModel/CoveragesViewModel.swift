//
//  CoveragesViewModel.swift
//  SafeAuto
//
//  Created by ashwini on 10/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class CoveragesViewModel: NSObject {
    
    var coveragesViewController : CoveragesViewController? = nil
    
    init(coveragesViewController : CoveragesViewController?) {
        super.init()
        guard let coveragesVC =  coveragesViewController else {
            return
        }
        self.coveragesViewController = coveragesVC
    }
    
    //MARK: - Temporary Methods
    
    func tempData(){
        if(coveragesViewController != nil){
            let liabilityCoverage1 = LiabilityCoverageModel(coverageName: "Bodily Injury Liability", coverageAmountPerAccident: "$10,000.00", coverageAmountPerPerson: "$10,000.00")
            let liabilityCoverage2 = LiabilityCoverageModel(coverageName: "Property Damage Liability", coverageAmountPerAccident: "$10,000.00", coverageAmountPerPerson: "-")
            let liabilityCoverage3 = LiabilityCoverageModel(coverageName: "Uninsured - Bodily Injury", coverageAmountPerAccident: "$10,000.00", coverageAmountPerPerson: "$10,000.00")
            let liabilityCoverage4 = LiabilityCoverageModel(coverageName: "Uninsured - property Damage", coverageAmountPerAccident: "$10,000.00", coverageAmountPerPerson: "-")
            let liabilityCoverage5 = LiabilityCoverageModel(coverageName: "Personal Injury Protection", coverageAmountPerAccident: "$10,000.00", coverageAmountPerPerson: "-")
            
            self.coveragesViewController!.liabilityTypeArr.append(liabilityCoverage1)
            self.coveragesViewController!.liabilityTypeArr.append(liabilityCoverage2)
            self.coveragesViewController!.liabilityTypeArr.append(liabilityCoverage3)
            self.coveragesViewController!.liabilityTypeArr.append(liabilityCoverage4)
            self.coveragesViewController!.liabilityTypeArr.append(liabilityCoverage5)
            
            let vehicleCoverageModel1 = VehicleCoverageModel(coverageType: "Comprehensive", isCoverageProvided: true)
            let vehicleCoverageModel2 = VehicleCoverageModel(coverageType: "Collision", isCoverageProvided: true)
            let vehicleCoverageModel3 = VehicleCoverageModel(coverageType: "Towing & Labor", isCoverageProvided: false)
            let vehicleCoverageModel4 = VehicleCoverageModel(coverageType: "Rental Reimbursement", isCoverageProvided: true)
            
            let vehicleCoverageModel5 = VehicleCoverageModel(coverageType: "Comprehensive", isCoverageProvided: true)
            let vehicleCoverageModel6 = VehicleCoverageModel(coverageType: "Collision", isCoverageProvided: false)
            let vehicleCoverageModel7 = VehicleCoverageModel(coverageType: "Towing & Labor", isCoverageProvided: false)
            let vehicleCoverageModel8 = VehicleCoverageModel(coverageType: "Rental Reimbursement", isCoverageProvided: true)
            
            self.coveragesViewController!.vehiclesCoverageArr.append(vehicleCoverageModel1)
            self.coveragesViewController!.vehiclesCoverageArr.append(vehicleCoverageModel2)
            self.coveragesViewController!.vehiclesCoverageArr.append(vehicleCoverageModel3)
            self.coveragesViewController!.vehiclesCoverageArr.append(vehicleCoverageModel4)
            self.coveragesViewController!.vehiclesCoverageArr.append(vehicleCoverageModel5)
            self.coveragesViewController!.vehiclesCoverageArr.append(vehicleCoverageModel6)
            self.coveragesViewController!.vehiclesCoverageArr.append(vehicleCoverageModel7)
            self.coveragesViewController!.vehiclesCoverageArr.append(vehicleCoverageModel8)
            
            let nonCoverageModel1 = NonCoverageModel(imageName: "tyreIcon", nonCoverageTypeName: "TYRE, TUBES & ENGINE")
            let nonCoverageModel2 = NonCoverageModel(imageName: "carIcon", nonCoverageTypeName: "NON-ACCIDENTAL DAMAGE")
            let nonCoverageModel3 = NonCoverageModel(imageName: "illegalDrivingIcon", nonCoverageTypeName: "ILLEGAL DRIVING")
            
            self.coveragesViewController!.nonCoverageArr.append(nonCoverageModel1)
            self.coveragesViewController!.nonCoverageArr.append(nonCoverageModel2)
            self.coveragesViewController!.nonCoverageArr.append(nonCoverageModel3)
            
            self.coveragesViewController!.nonCoverageCollectionView.reloadData()
            
            self.coveragesViewController!.coverageTableView.reloadData()
        }
    }
    
    
}
