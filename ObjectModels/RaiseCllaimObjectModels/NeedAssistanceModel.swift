//
//  NeedAssistanceModel.swift
//  SafeAuto
//
//  Created by ems on 08/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
struct NeedAssistanceModel{
    var needAssistanceTypeImageName: String
    var needAssistanceTypeName: String
    var isAssistanceTypeSelected: Bool
}
