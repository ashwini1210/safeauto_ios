//
//  WhoWasDrivingObjectModel.swift
//  SafeAuto
//
//  Created by ems on 08/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
struct WhoWasDrivingObjectModel{
    var driverName: String
    var driverDateOfBirth: String
    var driverLicense: String
    var driverProfileImage:String
    var isDriverSelected: Bool
}
