//
//  CoverageModels.swift
//  PractiseMajesco
//
//  Created by ems on 27/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import Foundation

struct LiabilityCoverageModel{
   
    var coverageName: String
    var coverageAmountPerAccident: String
    var coverageAmountPerPerson: String
    
    
}

struct VehicleCoverageModel {
    
    var coverageType: String
    var isCoverageProvided: Bool
}

struct NonCoverageModel{
    var imageName: String
    var nonCoverageTypeName: String
}
