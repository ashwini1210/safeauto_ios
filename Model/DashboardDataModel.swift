//
//  DashboardDataModel.swift
//  SafeAuto
//
//  Created by ashwini on 08/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit
import SwiftyJSON

class DashboardDataModel: BaseModel {
    
    var dashboardModelArray = [DashboardModel]()
    
    override init(delegate: HttpResponseProtocol) {
        super.init(delegate: delegate)
        setData()
    }
    
    func setData(){
        let dashboardModel1 = DashboardModel(policyNumber: "SF294AK885", MakeModel: "2017-AUDI-A4")
        dashboardModelArray.append(dashboardModel1)
        let dashboardModel2 = DashboardModel(policyNumber: "SF294AK886", MakeModel: "2017-AUDI-A5")
        dashboardModelArray.append(dashboardModel2)
        let dashboardModel3 = DashboardModel(policyNumber: "SF294AK887", MakeModel: "2017-AUDI-A6")
        dashboardModelArray.append(dashboardModel3)
        let dashboardModel4 = DashboardModel(policyNumber: "SF294AK887", MakeModel: "2017-AUDI-A6")
        dashboardModelArray.append(dashboardModel4)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
            self.delegate.onHttpResponse(isSuccess: true, responseJson: nil, error: nil, anyIdentifier: nil)
        }
    }
}
