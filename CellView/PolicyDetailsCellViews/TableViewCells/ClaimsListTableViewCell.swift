//
//  ClaimsListTableViewCell.swift
//  PractiseMajesco
//
//  Created by ems on 19/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class ClaimsListTableViewCell: UITableViewCell {

    @IBOutlet weak var claimIdLabel: UILabel!
    
    @IBOutlet weak var claimDateLabel: UILabel!
    
    @IBOutlet weak var claimStatusLabel: UILabel!
    
    @IBOutlet weak var claimDetailButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func initViews(){
        self.contentView.addBorders(borderWidth: 0.7, borderColor: UIColor.lightGray.cgColor)
    }
}
