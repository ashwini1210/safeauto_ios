//
//  CoverageTableViewCell.swift
//  PractiseMajesco
//
//  Created by ems on 20/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class CoverageTableViewCell: UITableViewCell {
    

    @IBOutlet weak var liabilityCoverageTypeLabel: UILabel!
    @IBOutlet weak var liabilityCoverageAmountPerAccidentLabel: UILabel!
    
    @IBOutlet weak var liabilityCoverageAmountPerPersonLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDateForFirstSection(){
        //UI Setup of View
        liabilityCoverageTypeLabel.font = UIFont.systemFont(ofSize: 11, weight: UIFont.Weight.bold)
        liabilityCoverageTypeLabel.textColor = UIColor.black
        contentView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR )
        liabilityCoverageTypeLabel.backgroundColor = UIColor.clear
        liabilityCoverageAmountPerPersonLabel.backgroundColor = UIColor.clear
        liabilityCoverageAmountPerAccidentLabel.backgroundColor = UIColor.clear
        
        
        //Assign Values to UI
        liabilityCoverageTypeLabel.text = " Liability Coverage"
        liabilityCoverageAmountPerAccidentLabel.text = "Per Accident"
        liabilityCoverageAmountPerPersonLabel.text = "Per Person"
    }
    
    func setDateOfCoverageCell(indexPath: IndexPath, liabilityTypeArr:[LiabilityCoverageModel]){
        //Assign Values to UI
        liabilityCoverageTypeLabel.text = " \(liabilityTypeArr[indexPath.row].coverageName)"
        liabilityCoverageAmountPerAccidentLabel.text = liabilityTypeArr[indexPath.row].coverageAmountPerAccident
        liabilityCoverageAmountPerPersonLabel.text = liabilityTypeArr[indexPath.row].coverageAmountPerPerson
        
        //UI setup
        contentView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
    }
    
}
