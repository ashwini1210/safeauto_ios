//
//  GlassTypeBothWindowsCollectionViewCell.swift
//  PractiseMajesco
//
//  Created by ems on 01/04/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class GlassTypeBothWindowsCollectionViewCell: UICollectionViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var checkBothGlassTypeMainView: UIView!
    
    @IBOutlet weak var frontGlassTypeImageView: UIImageView!
    
    @IBOutlet weak var sideOrRearGlassTypeImageView: UIImageView!
    
    @IBOutlet weak var checkGlassTypeImageView: UIImageView!
    
    @IBOutlet weak var glassTypeLabel: UILabel!
    
    //MARK: - Variables
    var selectGlassTypeCellDelegateProtocol: SelectGlassTypeCellDelegateProtocol?
    var indexNo: Int?
    
    //MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //setUpGestures()
    }
    
    func setUpGestures(){
        self.checkBothGlassTypeMainView.isUserInteractionEnabled = true
        let checkBothGlassTypeMainViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(checkBothGlassTypeMainViewTapped))
        self.checkBothGlassTypeMainView.addGestureRecognizer(checkBothGlassTypeMainViewTapGesture)
    }
    
    //Called in cellForRow
    func setUpAppearance(){
        self.checkGlassTypeImageView.isHidden = true
        self.checkBothGlassTypeMainView.addBorders()
        self.checkBothGlassTypeMainView.roundCorners()
    }
    
    //MARK: - Selector Methods
    @objc func checkBothGlassTypeMainViewTapped(){
        
        
        self.selectGlassTypeCellDelegateProtocol?.glassTypeSelected(indexNo: self.indexNo!)
    }
    

}
