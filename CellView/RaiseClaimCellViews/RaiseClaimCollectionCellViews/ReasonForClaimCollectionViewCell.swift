//
//  ReasonForClaimCollectionViewCell.swift
//  PractiseMajesco
//
//  Created by ems on 22/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class ReasonForClaimCollectionViewCell: UICollectionViewCell {

    //MARK: - Outlets
    @IBOutlet weak var reasonForClaimMainView: UIView!
    
    @IBOutlet weak var reasonImageView: UIImageView!
    
    @IBOutlet weak var reasonNameLabel: UILabel!
    
   
    @IBOutlet weak var isSelectedRadioBtnImageView: UIImageView!
    
    //MARK: - Variables
    var indexNo: Int?
    var raiseClaimCellDelegateProtocol: RaiseClaimCellDelegateProtocol?
    var isCallFromNeedAssistanceCollectionView: Bool?
    
    //MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpGestures()
    }
    
    func setUPUIAppearance(){
        self.isSelectedRadioBtnImageView.image = UIImage(named: "radioBtnUncheckedIcon")
        self.isSelectedRadioBtnImageView.tintColor = UIColor.lightGray
        self.reasonForClaimMainView.addBorders(borderWidth: 1, borderColor: UIColor.gray.cgColor)
        self.reasonForClaimMainView.roundCorners(cornerRadius: 20)
        self.contentView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_REASONFORCLAIM_BACKGROUND_COLOR)
    }
    
    func setUPUIAppearanceForNeedAssistance(){
        self.isSelectedRadioBtnImageView.image = UIImage(named: "checkBoxSquareIcon")
        self.isSelectedRadioBtnImageView.tintColor = UIColor.lightGray
        self.reasonForClaimMainView.addBorders(borderWidth: 1, borderColor:  UIColor.gray.cgColor)
        self.reasonForClaimMainView.roundCorners(cornerRadius: 20)
    }
    
    func setUpGestures(){
        let reasonForClaimMainViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(reasonForClaimMainViewTapped))
        self.reasonForClaimMainView.addGestureRecognizer(reasonForClaimMainViewTapGesture)
    }
    
    func setUpCell(indexPath: IndexPath, reasonForClaimArr:[ReasonForClaimModel]){
        //Assign Values to UI
        reasonImageView.image = UIImage(named: reasonForClaimArr[indexPath.row].reasonImageName)
        reasonNameLabel.text = reasonForClaimArr[indexPath.row].reasonName
        
        //if it is selected logic
        if reasonForClaimArr[indexPath.item].isReasonSelected{
            reasonForClaimMainView.addBorders(borderWidth: 2.0, borderColor: UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR).cgColor)
            isSelectedRadioBtnImageView.image = UIImage(named: "radioBtnCheckedIcon")
            isSelectedRadioBtnImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
            self.contentView.backgroundColor = UIColor.white
        }else{
            setUPUIAppearance()
        }
    }
    
    func setUpCellForNeedAssistance(indexPath: IndexPath, needAssistanceArr:[NeedAssistanceModel]){
        //Assign Values to UI
        reasonImageView.image = UIImage(named: needAssistanceArr[indexPath.row].needAssistanceTypeImageName)
        reasonNameLabel.text = needAssistanceArr[indexPath.row].needAssistanceTypeName
       
        //if it is selected logic
        if needAssistanceArr[indexPath.row].isAssistanceTypeSelected{
            reasonForClaimMainView.addBorders(borderWidth: 2.0, borderColor: UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR).cgColor)
            isSelectedRadioBtnImageView.image = UIImage(named: "checkBoxSquareSelectedIcon")
            isSelectedRadioBtnImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
            
        }else{
            setUPUIAppearanceForNeedAssistance()
        }
        
    }
    
    //MARK: - Selector Methods
    @objc func reasonForClaimMainViewTapped(){
        if isCallFromNeedAssistanceCollectionView!{
            raiseClaimCellDelegateProtocol?.ifNeedAsistanceTypeSelected(indexNo: indexNo!)
        }else{
            raiseClaimCellDelegateProtocol?.reasonTypeSelected(indexNo: self.indexNo!)
            if indexNo == 0{
                raiseClaimCellDelegateProtocol?.ifGlassTypeSelected()
            }
        }
       
    }
}
