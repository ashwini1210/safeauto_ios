//
//  SelectGlassTypeCellDelegateProtocol.swift
//  SafeAuto
//
//  Created by ems on 04/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
protocol SelectGlassTypeCellDelegateProtocol {
    func glassTypeSelected(indexNo: Int)
}
