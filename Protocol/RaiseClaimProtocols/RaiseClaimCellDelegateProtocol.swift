//
//  RaiseClaimCellDelegateProtocol.swift
//  SafeAuto
//
//  Created by ems on 03/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
protocol RaiseClaimCellDelegateProtocol{
    func reasonTypeSelected(indexNo: Int)
    func ifGlassTypeSelected()
    func vehicleAffectedSelected(indexNo: Int)
    func ifNeedAsistanceTypeSelected(indexNo: Int)
    func whoWasDrivingSelected(indexNo: Int)
}
