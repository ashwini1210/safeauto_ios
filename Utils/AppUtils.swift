
import UIKit

class AppUtils: NSObject {
    
    /*
     * Make any view in circular shape
     */
    static func createCircularView(view: UIView, borderWidth: CGFloat? = nil, borderColor: CGColor? = nil){
        if(borderWidth != nil){
            view.layer.borderWidth = borderWidth!
        }
        if(borderColor != nil){
            view.layer.borderColor = borderColor
        }
        view.layer.masksToBounds = false
        view.layer.cornerRadius = view.frame.size.width / 2
        view.clipsToBounds = true
    }
    
    /*
     * Make any view corner rounded by proving radius
     */
    static func createRoundCornerView(view: UIView, cornerRadius: CGFloat, borderWidth: CGFloat? = nil, borderColor: CGColor? = nil ) {
        view.layer.cornerRadius = cornerRadius
        view.clipsToBounds = true
        if(borderWidth != nil){
            view.layer.borderWidth = borderWidth!
        }
        if(borderColor != nil){
            view.layer.borderColor = borderColor!
        }
        
    }
    
    /*
     * Method to add background image as FontAwsom to Button
     */
    static func addFontAwsomToButton(button:UIButton, fontAwsomName: String,textSize: CGFloat,textcolour: UIColor){
        button.titleLabel?.textColor = textcolour
        button.titleLabel!.font = UIFont.icon(from: .fontAwesome, ofSize: textSize)
        button.setTitle(String.fontAwesomeIcon(fontAwsomName), for: .normal)
    }

    
    /*
     * Method to add background image as FontAwsom to ImageView
     */
    static func textToImage(drawText text: String, inImage image: UIImage = UIImage(), atPoint point: CGPoint,fontAwsomName: String, textSize: CGFloat, textcolour: UIColor) -> UIImage {
        let textColor = textcolour
        let textFont = UIFont.icon(from: .fontAwesome, ofSize: textSize)
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
            ] as [NSAttributedString.Key : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        let rect = CGRect(origin: point, size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    /*
     * Method to create custom fontAwesomeIcon Button
     */
    static func createFontAwesomeButton(buttonName: String,viewController: UIViewController, buttTag : Int) -> UIBarButtonItem{
        let drawerButton = UIButton(type: .custom)
        drawerButton.frame = CGRect(x:0,y:0,width: 40, height:40)
        drawerButton.titleLabel?.textColor = UIColor.white
        drawerButton.titleLabel!.font = UIFont.icon(from: .fontAwesome, ofSize: 25)
        drawerButton.setTitle(String.fontAwesomeIcon(buttonName), for: .normal)
        drawerButton.tag = buttTag
        drawerButton.addTarget(viewController, action: #selector(viewController.fontAwsomButtonClicked(sender:)), for: .touchUpInside)
        let barButtonItem = UIBarButtonItem.init(customView: drawerButton)
        return barButtonItem
    }
    
    /*
     * Method to create background view with alpha 0.5
     */
    static func createBackgroundShadowView(X: Int, Y:Int, Width:Int, Height:Int) -> UIView?{
        let customView = UIView(frame: CGRect(x: X, y: Y, width: Width, height: Height))
        customView.backgroundColor = UIColor(white: 0, alpha: 0.5)
        return customView
    }
}
