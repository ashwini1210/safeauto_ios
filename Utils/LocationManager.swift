//
//  LocationManager.swift
//  SafeAuto
//
//  Created by ashwini on 05/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    
    override init() {
        super.init()
        isAuthorizedtoGetUserLocation()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
    }
    
    //if we have no permission to access user location, then ask user for permission.
    func isAuthorizedtoGetUserLocation() {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
        else if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    func startLocationService(){
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            print("User allowed us to access location")
            //do whatever init activities here.
        }
        else if(status == .authorizedAlways ){
            print("User allowed us to access location always")
        }
    }
    
    
    //this method is called by the framework on         locationManager.requestLocation();
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks![0]
                //                print("locality: \(placemark.locality!)")
                //                print("administrativeArea: \(placemark.administrativeArea!)")
                //                print("country: \(placemark.country!)")
                
                AppPreference.getInstance().setString(AppConstants.CURRENT_CITY, value: placemark.locality!)
                AppPreference.getInstance().setString(AppConstants.CURRENT_STATE, value: placemark.administrativeArea!)
                AppPreference.getInstance().setString(AppConstants.CURRENT_COUNTRY, value: placemark.country!)
                
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }
    
    func stopLocatinService(){
        if CLLocationManager.locationServicesEnabled() {
            locationManager.stopUpdatingLocation()
        }
    }
}
