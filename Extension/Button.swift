//
//  Button.swift
//  SafeAuto
//
//  Created by ems on 02/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import UIKit

extension UIButton{
    
    func makeRoundCorners(radius: CGFloat){
        self.layer.cornerRadius = radius
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.clear.cgColor
    }
    
}
