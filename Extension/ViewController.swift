//
//  ViewController.swift
//  SafeAuto
//
//  Created by ashwini on 27/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    @objc func fontAwsomButtonClicked(sender: Any){
        
    }
    
    /*
     * Add Tap gesture to close Keyboard on outer touch
     */
    func registerTapGesture(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}
