//
//  NavigationItem.swift
//  SafeAuto
//
//  Created by ems on 02/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationItem{
    func setUpNavigationTitle(title: String, subtitle: String, heightOfNavigationBar: CGFloat?){
        let topText = NSLocalizedString(title, comment: "")
        let bottomText = NSLocalizedString(subtitle, comment: "")
        
        let titleParameters = [NSAttributedString.Key.foregroundColor : UIColor.white]
        let subTitleParameters = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        let title: NSMutableAttributedString = NSMutableAttributedString(string: topText, attributes: titleParameters)
        let subTitle: NSAttributedString = NSAttributedString(string: bottomText, attributes: subTitleParameters)
        
        title.append(NSAttributedString(string: "\n"))
        title.append(subTitle)
        
        let size = title.size()
        let width = size.width
        
        guard let height = heightOfNavigationBar
            else {
                return
        }
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: height))
        titleLabel.attributedText = title
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        
        self.titleView = titleLabel
        
    }
    
    
}
