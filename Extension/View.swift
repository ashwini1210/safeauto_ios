//
//  View.swift
//  SafeAuto
//
//  Created by ashwini on 27/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    
    
    func addBorderAndShadow(borderWidth: CGFloat = 0.2, borderColor: CGColor = UIColor.lightGray.cgColor){
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
        self.layer.shadowOffset = CGSize(width: -1, height: 5)
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 1
    }
    
    func addBorders(borderWidth: CGFloat = 0.2, borderColor: CGColor = UIColor.lightGray.cgColor){
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor
    }
    
    func addShadowToView(shadowRadius: CGFloat = 2, alphaComponent: CGFloat = 0.6) {
        
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: alphaComponent).cgColor
        self.layer.shadowOffset = CGSize(width: -1, height: 5)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = 1
    }
    
    func addShadowAndRoundCorner(){
        
      
        
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        outerView.clipsToBounds = false
        outerView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
        outerView.layer.shadowOpacity = 1
        outerView.layer.shadowOffset = CGSize(width: -1, height: 5)
        outerView.layer.shadowRadius = 10
        outerView.layer.shadowPath = UIBezierPath(roundedRect: outerView.bounds, cornerRadius: 10).cgPath
        outerView.backgroundColor = UIColor.clear
        
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
        
        self.addSubview(outerView)
    }
    
    func roundCorners(cornerRadius: CGFloat = 5){
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
    }
    
    func dropShadow(shadowColor: UIColor = UIColor.black,
                    fillColor: UIColor = UIColor.white,
                    opacity: Float = 0.2,
                    offset: CGSize = CGSize(width: 0.0, height: 1.0),
                    radius: CGFloat = 10) {
        
        let shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius).cgPath
        shadowLayer.fillColor = fillColor.cgColor
        shadowLayer.shadowColor = shadowColor.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = offset
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = radius
        layer.insertSublayer(shadowLayer, at: 0)
        
    }
    
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func makeDatePicker(parentFrame: CGRect, width: CGFloat, height: CGFloat) -> UIView{
        
        let view = UIView()
        view.frame.size = CGSize(width: width, height: height)
        view.tag = 20191
        view.backgroundColor = UIColor.white
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.frame = CGRect(x: 0, y: 0, width: width, height: height * 0.2)
        toolbar.tag = 20192
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: nil)
        doneButton.tag = 20193
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: nil)
        cancelButton.tag = 20194
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        //Date Picker
        let datePicker = UIDatePicker()
        datePicker.frame = CGRect(x: 0, y: toolbar.frame.maxY, width: width, height: height * 0.8)
        datePicker.tag = 20195
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = UIColor.white
        
        view.addSubview(toolbar)
        view.addSubview(datePicker)
        

        return view
        
    }
    
    func temp(){
//        var layer = CALayer()
//        layer.bounds = CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: self.bounds.height)
//        layer.position = self.center
//        layer.backgroundColor = UIColor.clear.cgColor
        self.layer.shadowOpacity = 0.55
        self.layer.cornerRadius = 8.0
        self.layer.borderWidth = 0.5
        
   
        
//        self.layer.addSublayer(layer)
        
//        testView().layer().shadowOpacity = 0.55
//        testView().layer().shadowRadius = 15.0
//        testView().layer().cornerRadius = 8.0
//        testView().layer().borderWidth = 1.0
     }
    
    //MARK: - TableView Programatically
    
    func createTableView(senderView: CGRect) -> UIView{
        let view = UIView(frame: CGRect(x: frame.minX, y: frame.minY, width: 0, height: 0))
        view.tag = AppConstants.MAKE_TABLEVIEW_CONTAINERVIEW_TAG
        view.backgroundColor = UIColor.white
        
        let tableView = UITableView(frame: CGRect(x: view.frame.minX, y: view.frame.minY, width: 0, height: 0))
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
        tableView.separatorStyle = .none
        tableView.tag = AppConstants.MAKE_TABLEVIEW_TAG
        
        view.addSubview(tableView)
        
        return view
    }
}
