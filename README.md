# Safe Auto

## Summery

Start your Insurance policy in just a few minutes with a low down payment and affordable monthly installments.


## Version

This app is developed for iPhone using Swift 4

## Build and Runtime Requirements

1. Xcode 6.0 or later
2. iOS 8.0 or later
3. OS X v10.10 or later

## Configuring the Project

1. Clone this repository 
2. 'pod install' on the Terminal
3. Open SafeAuto.xcworkspace

## Pods:

1.  Alamofire'
2.  SwiftyJSON'
3.  SwiftIconFont'
4.  SkyFloatingLabelTextField'
5.  RealmSwift'
6.  DLRadioButton'
7.  FacebookCore'
8.  FacebookLogin'
9.  FacebookShare'
10. GoogleSignIn'

## Features:

1. Signup with Facebook or Gmail.
2. Pay your bill, view your proof of insurance, and seamlessly access your insurance documents online.
3. Get Notification once insurance is going to expire
4. History of complete insurance
