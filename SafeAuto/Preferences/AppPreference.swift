//
//  AppPreference.swift
//  effy
//
//  Created by joyson on 14/03/16.
//  Copyright © 2016 TechnoPurple. All rights reserved.
//

import Foundation

open class AppPreference {

    fileprivate static let sharedPref = UserDefaults.standard
    fileprivate static let appPreference = AppPreference()
    
    fileprivate init() {} //This prevents others from using the default '()' initializer for this class.
    
    public static func getInstance() -> AppPreference
    {
        return appPreference
    }
    
    fileprivate func getSharedPreference() -> UserDefaults
    {
        return AppPreference.sharedPref;
    }
    
    open func setString(_ key: String, value: String)
    {
        getSharedPreference().set(value, forKey: key)
    }
    
    open func getString(_ key: String, defaultvalue: String) -> String
    {
        if let value = getSharedPreference().string(forKey: key)
        {
            return value
        }
        return defaultvalue
    }
    
    open func setInt(_ key: String, value: Int)
    {
        getSharedPreference().set(value, forKey: key)
    }
    
    open func getInt(_ key: String, defaultvalue: Int) -> Int
    {
        if let value = getSharedPreference().object(forKey: key)
        {
            return value as! Int
        }
        return defaultvalue
    }
  
  open func setDouble(_ key: String, value: Double)
  {
    getSharedPreference().set(value, forKey: key)
  }
  
  open func getDouble(_ key: String, defaultvalue: Double) -> Double
  {
    if let value = getSharedPreference().object(forKey: key)
    {
      return value as! Double
    }
    return defaultvalue
  }
  
  open func setBoolean(_ key: String, value: Bool)
  {
    getSharedPreference().set(value, forKey: key)
  }
  
  open func getBoolean(_ key: String, defaultvalue: Bool) -> Bool
  {
    
    if getSharedPreference().object(forKey: key) != nil
    {
      return getSharedPreference().bool(forKey: key)
    }
    return defaultvalue
  }
  
  open func setObject(_ key: String, value: AnyObject)
  {
    getSharedPreference().setValue(value, forKey: key)
  }
  
  open func getObject(_ key: String, defaultvalue: AnyObject?) -> AnyObject?
  {
    if let value = getSharedPreference().object(forKey: key)
    {
      return value as AnyObject
    }
    return defaultvalue
  }
  
  
}
