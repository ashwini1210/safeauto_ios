//
//  GlassTypePopUpCollectionViewCell.swift
//  PractiseMajesco
//
//  Created by ems on 01/04/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class GlassTypePopUpCollectionViewCell: UICollectionViewCell {

    //MARK: - Outlets
    @IBOutlet weak var checkGlassTypeMainView: UIView!
    
    @IBOutlet weak var checkGlassTypeImageView: UIImageView!
    
    @IBOutlet weak var glassTypeImageView: UIImageView!
    
    @IBOutlet weak var glassTypeLabel: UILabel!
    
    //MARK: - Variables
    
    var selectGlassTypeCellDelegateProtocol: SelectGlassTypeCellDelegateProtocol?
    var indexNo: Int?
    
    //MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     //   setUpGestures()
    }
    
    func setUpGestures(){
        
        self.checkGlassTypeMainView.isUserInteractionEnabled = true
    
        let checkGlassTypeMainViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(checkGlassTypeMainViewTapped))
        checkGlassTypeMainViewTapGesture.cancelsTouchesInView = true
        self.checkGlassTypeMainView.addGestureRecognizer(checkGlassTypeMainViewTapGesture)
    }
    
    //Called in cellForRow
    func setUpAppearance(){
        self.checkGlassTypeImageView.isHidden = true
        self.checkGlassTypeMainView.addBorders()
        self.checkGlassTypeMainView.roundCorners()
    }
    
    @objc func checkGlassTypeMainViewTapped(){
        
        print(":::::tapped")
        self.selectGlassTypeCellDelegateProtocol?.glassTypeSelected(indexNo: self.indexNo!)
    }
    
    

}
