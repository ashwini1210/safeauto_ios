//
//  ClaimPhotoCollectionViewCell.swift
//  SafeAuto
//
//  Created by ashwini on 05/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class ClaimPhotoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func  setImageOfClaimVeicle(indexPath: IndexPath, identifier: String, image: UIImage?) {
        if(identifier == AppConstants.VEHICLE_IMAGE_OF_CLAIM_DETAIL_SCR){
            if(image != nil){
                photoImageView.image = image
            }else{
            photoImageView.image = UIImage(named: "safeAuto.png")
            }
        }
        else if(identifier == AppConstants.VEHICLE_IMAGE_OF_RAISE_CLAIM_SCR){
            if(image != nil){
                photoImageView.image = image
            }else{
            photoImageView.image = UIImage(named: "safeAuto.png")
            }
        }
    }

}
