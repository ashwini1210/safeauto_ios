//
//  WhoWasDrivingTableViewCell.swift
//  SafeAuto
//
//  Created by ems on 08/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class WhoWasDrivingTableViewCell: UITableViewCell {

    //MARK: - Outlets
    
    @IBOutlet weak var whoWasDrivingMainView: UIView!
    @IBOutlet weak var isDriverSelectedRadioBtnImageView: UIImageView!
    @IBOutlet weak var driverProfilePicImageView: UIImageView!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var driverLicenseNoLabel: UILabel!
    @IBOutlet weak var driverDateOfBirthLabel: UILabel!
  
    //MARK: - Variables
    var raiseClaimCellDelegateProtocol: RaiseClaimCellDelegateProtocol?
    var indexNo: Int?
    
    //MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpGestures()
        // Initialization code
        self.setUpCellUIAppearance()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpCellUIAppearance(){
    //    self.whoWasDrivingMainView.addBorders()
        self.contentView.addBorders(borderWidth: 0.7, borderColor: UIColor.lightGray.cgColor)
    }
    
    func setUpCell(indexPath: IndexPath, whoWasDrivingModel: WhoWasDrivingObjectModel){
        self.driverNameLabel.text = whoWasDrivingModel.driverName
        self.driverDateOfBirthLabel.text = whoWasDrivingModel.driverDateOfBirth
        self.driverLicenseNoLabel.text = whoWasDrivingModel.driverLicense
        self.driverProfilePicImageView.image = UIImage(named: whoWasDrivingModel.driverProfileImage)
        
        if whoWasDrivingModel.isDriverSelected{
            self.isDriverSelectedRadioBtnImageView.image = UIImage(named: "radioBtnCheckedIcon")
            self.isDriverSelectedRadioBtnImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
        }else{
            self.isDriverSelectedRadioBtnImageView.image = UIImage(named: "radioBtnUncheckedIcon")
            self.isDriverSelectedRadioBtnImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
           
        }
    }
    
    //MARK: - Set Up Gestures
    func setUpGestures(){
        self.whoWasDrivingMainView.isUserInteractionEnabled = true
        let whoWasDrivingMainViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(whoWasDrivingMainViewTapped))
        self.whoWasDrivingMainView.addGestureRecognizer(whoWasDrivingMainViewTapGesture)
    }
    
    //MARK: - Selector Methods
    @objc func whoWasDrivingMainViewTapped(){
        self.raiseClaimCellDelegateProtocol?.whoWasDrivingSelected(indexNo: self.indexNo!)
        
    }
    
}
