//
//  VehicleAffectedTableViewCell.swift
//  PractiseMajesco
//
//  Created by ems on 22/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class VehicleAffectedTableViewCell: UITableViewCell {
    //MARK: - Outlets
    @IBOutlet weak var vehicleAffectedTableCellMainView: UIView!
    
    @IBOutlet weak var isVehicleSelectedImageView: UIImageView!
    
    @IBOutlet weak var vehicleNameLabel: UILabel!
    
    @IBOutlet weak var vehicleRegistrationCodeLabel: UILabel!
    
    @IBOutlet weak var vehicleImageButton: UIButton!
    //MARK: - Variables
    var raiseClaimCellDelegateProtocol: RaiseClaimCellDelegateProtocol?
    var indexNo: Int?
    var isTheftReasonSelected: Bool?
    var isCellSelected: Bool?
    
    //MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        setUpGestures()
        // Configure the view for the selected state
    }
    
    //Called in cellForRow
    func setUpUIAppearance(){
        self.contentView.addBorders(borderWidth: 0.7, borderColor: UIColor.lightGray.cgColor)
        AppUtils.addFontAwsomToButton(button: vehicleImageButton, fontAwsomName: "car", textSize: 15, textcolour: UIColor.black)
        AppUtils.createCircularView(view: vehicleImageButton)
    }
    
    //MARK: - Set Up Gestures
    func setUpGestures(){
        self.vehicleAffectedTableCellMainView.isUserInteractionEnabled = true
        let vehicleAffectedTableCellMainViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(vehicleAffectedTableCellMainViewTapped))
        self.vehicleAffectedTableCellMainView.addGestureRecognizer(vehicleAffectedTableCellMainViewTapGesture)
    }
    
    func setUpCell(indexPath: IndexPath, vehicleAffectedArr: [VehicleModel]){
        //UI Setup
        setUpUIAppearance()
        vehicleNameLabel.text = vehicleAffectedArr[indexPath.row].vehicleName
        vehicleRegistrationCodeLabel.text = vehicleAffectedArr[indexPath.row].vehicleCode
        
        if isTheftReasonSelected!{
            if vehicleAffectedArr[indexPath.row].isVehicleAffected{
                isVehicleSelectedImageView.image = UIImage(named: "checkBoxSquareSelectedIcon")
                isVehicleSelectedImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
            }else{
                isVehicleSelectedImageView.image = UIImage(named: "checkBoxSquareIcon")
                isVehicleSelectedImageView.tintColor = UIColor.lightGray
            }
            
        }else{
            if vehicleAffectedArr[indexPath.row].isVehicleAffected{
                isVehicleSelectedImageView.image = UIImage(named: "radioBtnCheckedIcon")
                isVehicleSelectedImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
            }else{
                isVehicleSelectedImageView.image = UIImage(named: "radioBtnUncheckedIcon")
                isVehicleSelectedImageView.tintColor = UIColor.lightGray
            }
        }
     
    }
    
    //MARK: - Selector Methods
    @objc func vehicleAffectedTableCellMainViewTapped(){
        self.raiseClaimCellDelegateProtocol?.vehicleAffectedSelected(indexNo: self.indexNo!)
        
    }
    
    
    
}
