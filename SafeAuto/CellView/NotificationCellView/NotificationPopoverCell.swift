//
//  NotificationTableViewCell.swift
//  SafeAuto
//
//  Created by ashwini on 27/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class NotificationPopoverCell: UITableViewCell {
    
    @IBOutlet weak var dotButton: UIButton!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var newLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        initViews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initViews(){
        AppUtils.addFontAwsomToButton(button: deleteButton, fontAwsomName: "trash", textSize: 20, textcolour: UIColor().hexStringToUIColor(hex: "#007AFF"))
        AppUtils.addFontAwsomToButton(button: dotButton, fontAwsomName: "circle", textSize: 10, textcolour: UIColor().hexStringToUIColor(hex: "#C91A00"))
        AppUtils.createRoundCornerView(view: newLabel, cornerRadius: 5)
    }
    
    /*
     * Change color of all Notification which we read
     */
    func changeColorOfNotificationRead(indexPath: IndexPath){
        AppUtils.addFontAwsomToButton(button: dotButton, fontAwsomName: "circle", textSize: 10, textcolour: UIColor.lightGray)
        paymentLabel.textColor = UIColor.lightGray
        newLabel.isHidden = true
    }
    
    @IBAction func deleteButtonClicked(_ sender: Any) {

    }
    
    
    func setData(notificationModel : NotificationModel?){
        paymentLabel.text = notificationModel!.paymentDetail
        dateTimeLabel.text = notificationModel!.date
        if(notificationModel!.newNotification){
            newLabel.isHidden = false
        }else{
            newLabel.isHidden = true
            paymentLabel.textColor = UIColor.lightGray
        }
    }
}
