//
//  NotificationListViewCell.swift
//  SafeAuto
//
//  Created by ashwini on 29/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class NotificationListViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionView: UIView!
    var noficicationViewModel : NotificationListViewModel? = nil

    
    override func awakeFromNib() {
        super.awakeFromNib()
        resizeLabels()
        addBorderToView()
    }
    
    /*
     * Method to resize labels of Header view
     */
    func resizeLabels(){
        dateLabel.frame = CGRect(x: 0, y: 0, width: 25 * self.contentView.bounds.width/100, height: dateLabel.frame.height)
        typeLabel.frame = CGRect(x: 25 * contentView.bounds.width/100, y: 0, width: 18 * contentView.bounds.width/100, height: typeLabel.frame.height)
        descriptionView.frame = CGRect(x: 43 * contentView.bounds.width/100, y: 0, width: 57 * contentView.bounds.width/100, height: descriptionView.frame.height)
    }
    
    /*
     * add border to view
     */
    func addBorderToView(){
        contentView.addTopBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#e5e5e5"), width: 0.5)
        contentView.addBottomBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#e5e5e5"), width: 0.5)
        self.dateLabel.addLeftBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#e5e5e5"), width: 1)
        self.typeLabel.addLeftBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#e5e5e5"), width: 1)
        self.descriptionView.addLeftBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#e5e5e5"), width: 1)
        self.descriptionView.addRightBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#e5e5e5"), width: 1)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(notificationModel : NotificationModel?){
        if(notificationModel!.newNotification){
            //bold text
            var formattedString = NSMutableAttributedString()
            formattedString.bold("\(notificationModel!.paymentDetail)", textSize: 12, textColor: UIColor.black)
            descriptionLabel.attributedText = formattedString
            
            formattedString = NSMutableAttributedString()
            formattedString.bold("\(notificationModel!.date)", textSize: 12, textColor: UIColor.black)
            dateLabel.attributedText = formattedString
            
            formattedString = NSMutableAttributedString()
            formattedString.bold("Policy", textSize: 12, textColor: UIColor.black)
            typeLabel.attributedText = formattedString

        }else{
            descriptionLabel.text = notificationModel!.paymentDetail
            dateLabel.text = notificationModel!.date
            typeLabel.text = "Policy"
        }
        
    }
}
