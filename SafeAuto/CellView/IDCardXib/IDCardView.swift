//
//  IDCardView.swift
//  SafeAuto
//
//  Created by ashwini on 14/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class IDCardView: UIView {

    class func instanceFromNib() -> UIView {
        
        return UINib(nibName: "IDCardView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}

