//
//  VehiclesListTableViewCell.swift
//  PractiseMajesco
//
//  Created by ems on 20/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class VehiclesListTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var vehicleImageView: UIImageView!
    
    @IBOutlet weak var vehicleModelDetailLabel: UILabel!
    
    @IBOutlet weak var vehicleUniqueNumberLabel: UILabel!
    
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var carImageButton: UIButton!
    //MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        initViews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initViews(){
        
        self.contentView.addBorders(borderWidth: 0.7, borderColor: UIColor.lightGray.cgColor)
        AppUtils.addFontAwsomToButton(button: editButton, fontAwsomName: "pencil", textSize: 15, textcolour: UIColor().hexStringToUIColor(hex: "#337ab7"))
        AppUtils.addFontAwsomToButton(button: carImageButton, fontAwsomName: "car", textSize: 15, textcolour: UIColor.black)
        AppUtils.createCircularView(view: carImageButton)
        
    }
    
    func setupData(vehicleModel: VehicleModel){
        self.vehicleModelDetailLabel.text = vehicleModel.vehicleName
        self.vehicleUniqueNumberLabel.text = vehicleModel.vehicleCode
    }
}
