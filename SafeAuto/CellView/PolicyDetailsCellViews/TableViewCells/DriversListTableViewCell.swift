//
//  DriversListTableViewCell.swift
//  PractiseMajesco
//
//  Created by ems on 19/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class DriversListTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var driversProfilePicImageView: UIImageView!
    
    @IBOutlet weak var driversNameLabel: UILabel!
    
    @IBOutlet weak var driversDateOfBirthLabel: UILabel!
    
    @IBOutlet weak var driversLicenseLabel: UILabel!
    
    @IBOutlet weak var driversEditBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initViews()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initViews(){
        self.contentView.addBorders(borderWidth: 0.7, borderColor: UIColor.lightGray.cgColor)
        AppUtils.addFontAwsomToButton(button: driversEditBtn, fontAwsomName: "pencil", textSize: 15, textcolour: UIColor().hexStringToUIColor(hex: "#337ab7"))
    }
    
}
