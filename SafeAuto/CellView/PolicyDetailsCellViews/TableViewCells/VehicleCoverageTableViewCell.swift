//
//  VehicleCoverageTableViewCell.swift
//  PractiseMajesco
//
//  Created by ems on 27/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class VehicleCoverageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var coverageTypeLabel: UILabel!
    
    @IBOutlet weak var isCoverageTypeApprovedButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setUIOfSection(vehicleName: String){
        
        //UI Setup
        coverageTypeLabel.font = UIFont.systemFont(ofSize: 11, weight: .bold)
        coverageTypeLabel.textColor = UIColor.black
        contentView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
        coverageTypeLabel.backgroundColor = UIColor.clear
        isCoverageTypeApprovedButton.backgroundColor = UIColor.clear
        
        coverageTypeLabel.text = " \(vehicleName)"
    }
    func setUIOfSceondSection(){
        //UI Setup
        coverageTypeLabel.font = UIFont.systemFont(ofSize: 11, weight: .bold)
        coverageTypeLabel.textColor = UIColor.black
        contentView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
        coverageTypeLabel.backgroundColor = UIColor.clear
        isCoverageTypeApprovedButton.backgroundColor = UIColor.clear
        
        //Assign values to UI
        coverageTypeLabel.text = " 2018 Audi A3 2.0T"
    }
    
    func setUIOfThirdSection(){
        //UI Setup
        coverageTypeLabel.font = UIFont.systemFont(ofSize: 11, weight: .bold)
        coverageTypeLabel.textColor = UIColor.black
        contentView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
        coverageTypeLabel.backgroundColor = UIColor.clear
        isCoverageTypeApprovedButton.backgroundColor = UIColor.clear
        coverageTypeLabel.text = " 2018 BMW Z4 2LT"
    }
    

    
    func setDataForVehicleCell1(indexPath: IndexPath, vehiclesCoverageArr:[VehicleCoverageModel]){
        //Assign Values to UI
        coverageTypeLabel.text = " \(vehiclesCoverageArr[indexPath.row].coverageType)"
        if(vehiclesCoverageArr[indexPath.row].isCoverageProvided){
            isCoverageTypeApprovedButton.tintColor = UIColor().hexStringToUIColor(hex: "#337ab7")
            AppUtils.addFontAwsomToButton(button: isCoverageTypeApprovedButton, fontAwsomName: "check", textSize: 15, textcolour: UIColor().hexStringToUIColor(hex: "#337ab7"))
        }else{
            isCoverageTypeApprovedButton.tintColor = UIColor.red
            AppUtils.addFontAwsomToButton(button: isCoverageTypeApprovedButton, fontAwsomName: "times", textSize: 15, textcolour: UIColor.red)
            
        }
        
        
        //UI Setup
        //        contentView.backgroundColor = UIColor.white
    }
    
    func setDataForVehicleCell2(indexPath: IndexPath, vehiclesCoverageArr:[VehicleCoverageModel]){
        coverageTypeLabel.text = " \(vehiclesCoverageArr[indexPath.row].coverageType)"
        if(vehiclesCoverageArr[indexPath.row].isCoverageProvided){
            isCoverageTypeApprovedButton.tintColor = UIColor().hexStringToUIColor(hex: "#337ab7")
            AppUtils.addFontAwsomToButton(button: isCoverageTypeApprovedButton, fontAwsomName: "check", textSize: 15, textcolour: UIColor().hexStringToUIColor(hex: "#337ab7"))
        }else{
            isCoverageTypeApprovedButton.tintColor = UIColor.red
            AppUtils.addFontAwsomToButton(button: isCoverageTypeApprovedButton, fontAwsomName: "times", textSize: 15, textcolour: UIColor.red)
            
        }
        //UI Setup
        //        contentView.backgroundColor = UIColor.white
    }
    
}
