//
//  PolicyPeriodTableViewCell.swift
//  SafeAuto
//
//  Created by ems on 11/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class PolicyPeriodTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var policyPeriodMainView: UIView!
    @IBOutlet weak var policyPeriodLabel: UILabel!
    
    //MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.policyPeriodMainView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
