//
//  PolicyDetailTableViewCell.swift
//  SafeAuto
//
//  Created by ashwini on 19/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class PolicyDetailTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var carImageButton: UIButton!
    @IBOutlet weak var policyNumberLabel: UILabel!
    @IBOutlet weak var makeModelLabel: UILabel!
    @IBOutlet weak var arrowButton: UIButton!
    @IBOutlet weak var activeLabel: UILabel!
    var dashboardDataModel: DashboardDataModel? = nil
    var dashboardViewController: DashboardViewController? = nil
    var indexNo: Int?
    
    //MARK: - Variables
    var onCellElementClickProtocol: OnCellElementClickProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initViews()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initViews(){
        AppUtils.createCircularView(view: carImageButton, borderWidth: 1, borderColor: UIColor.red.cgColor)
        AppUtils.addFontAwsomToButton(button: carImageButton, fontAwsomName: "car", textSize: 18, textcolour: UIColor.white)
        AppUtils.addFontAwsomToButton(button: arrowButton, fontAwsomName: "chevronright", textSize: 22, textcolour: UIColor.white)
    }
    
    @IBAction func arrowButtonClicked(_ sender: Any) {
        onCellElementClickProtocol?.onPolicyClicked(policyNumber: (self.dashboardDataModel?.dashboardModelArray[indexNo!].policyNumber)!)
    }
    
    func setDataInView(index:Int){
        if(dashboardDataModel != nil){
            if(dashboardDataModel!.dashboardModelArray.count > 0){
                let dashboardModel = dashboardDataModel!.dashboardModelArray[index]
                policyNumberLabel.text = dashboardModel.policyNumber
                makeModelLabel.text = dashboardModel.MakeModel
                // dashboardViewController?.policyDetailTableView.reloadData()
            }
        }
    }
}


