//
//  NonCoveragesCollectionViewCell.swift
//  PractiseMajesco
//
//  Created by ems on 20/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class NonCoveragesCollectionViewCell: UICollectionViewCell {
    

    @IBOutlet weak var nonCoverageTypeImageView: UIImageView!
    
    @IBOutlet weak var nonCoverageTypeNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setUIOfCell(indexPath : IndexPath, nonCoverageArr: [NonCoverageModel]){
        nonCoverageTypeImageView.image = UIImage(named: nonCoverageArr[indexPath.row].imageName)
        nonCoverageTypeNameLabel.text = nonCoverageArr[indexPath.row].nonCoverageTypeName
        self.contentView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_YOUARENOTCOVERFOR_COLLECTIONVIEW_BACKGROUND_COLOR)
        
    }
}
