//
//  DashboardCellDelegateProtocol.swift
//  SafeAuto
//
//  Created by ems on 02/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
protocol OnCellElementClickProtocol{
    func onTableViewCellClicked()
    func onPolicyClicked(policyNumber: String)
}

extension OnCellElementClickProtocol{
    func onTableViewCellClicked(){}
    func onPolicyClicked(policyNumber: String){}
}
