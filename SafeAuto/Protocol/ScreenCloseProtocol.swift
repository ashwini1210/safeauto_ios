//
//  PopoverProtocol.swift
//  SafeAuto
//
//  Created by ashwini on 28/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation

protocol ScreenCloseProtocol {
    func onScreenClose(isClosed:Bool,value:Any?)
}
