//
//  RegisterViewModel.swift
//  SafeAuto
//
//  Created by Prathamesh Salvi on 31/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
class RegisterViewModel: NSObject{
    
    var registerViewController: RegisterViewController? = nil
    var registerRealmManagerProtocol: RegisterRealmManagerProtocol = RegisterRealmManager()
    var policyRealmManagerProtocol: PolicyRealmManagerProtocol = PolicyRealmManager()
    var vehicleRealmManagerProtocol: VehicleRealmManagerProtocol = VehicleRealmManager()
    var vehicleCoverageRealmManagerProtocol: VehicleCoverageRealmManagerProtocol = VehicleCoverageRealmManager()
    
    init(registerViewController: RegisterViewController?) {
        super.init()
        guard let registerVC =  registerViewController else {
            return
        }
        self.registerViewController = registerVC
    }
    
    func registerUser(registerModel: RegisterModel){
        let policyNumber = registerModel.policyNumber
        self.registerRealmManagerProtocol.saveClaim(registerModel: registerModel)
     
        self.addPolicy(policyNumber: policyNumber)
        self.addVehicle(policyNumber: policyNumber)
        self.addVehicleCoverageList()
     
        
    }
    
    func addPolicy(policyNumber: String){
        let policyModel = PolicyModel(policyNumber: policyNumber)
        self.policyRealmManagerProtocol.addPolicy(policyObj: policyModel)
    }
    
    func addVehicle(policyNumber: String){
         let vehicleModel = VehicleModel(vehicleName: "2019 | Jaguar | F-Type", vehicleCode: "LJCPCBLCX11000499", isVehicleAffected: false, policyNumber: policyNumber)
        self.vehicleRealmManagerProtocol.addVehicle(vehicleModel: vehicleModel)
    }
    
    func addVehicleCoverageList(){
        var vehicleCoverageArr = [VehicleCoverageModel]()
        
        
        let vehicleCoverageModel1 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11000499", coverageType: "Comprehensive", isCoverageProvided: true)
        let vehicleCoverageModel2 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11000499", coverageType: "Collision", isCoverageProvided: true)
        let vehicleCoverageModel3 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11000499", coverageType: "Towing & Labor", isCoverageProvided: false)
        let vehicleCoverageModel4 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11000499", coverageType: "Rental Reimbursement", isCoverageProvided: true)
        
        vehicleCoverageArr.append(vehicleCoverageModel1)
        vehicleCoverageArr.append(vehicleCoverageModel2)
        vehicleCoverageArr.append(vehicleCoverageModel3)
        vehicleCoverageArr.append(vehicleCoverageModel4)
        
        self.vehicleCoverageRealmManagerProtocol.saveVehicleCoverageList(vehicleCoverageList: vehicleCoverageArr)
    }
}
