//
//  ClaimListViewModel.swift
//  SafeAuto
//
//  Created by ems on 16/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import RealmSwift
class ClaimListViewModel: NSObject{
    
    var claimsListViewController : ClaimsListViewController? = nil
    var claimArr: [ClaimModel] = [ClaimModel]()
    var claimRealmManagerProtocol: ClaimRealmManagerProtocol = ClaimRealmManager()
    
    init(claimsListViewController : ClaimsListViewController?) {
        super.init()
        guard let claimVC =  claimsListViewController else {
            return
        }
        self.claimsListViewController = claimVC
    }
    
    func getClaimList(){
        let claim1 = ClaimModel(claimName: "CLM1032", claimDate: "Jan 12, 2019", claimStatus: "Approved")
        let claim2 = ClaimModel(claimName: "CLM1045", claimDate: "Dec 26, 2018", claimStatus: "Settled")
        
        self.claimArr.append(claim1)
        self.claimArr.append(claim2)
        
        let claimList = self.claimRealmManagerProtocol.getClaimList()
        
        if claimList.count <= 0{
            self.claimRealmManagerProtocol.saveclaimList(claimListArr: claimArr)
            sendClaimList()
        }else{
            sendClaimList(claimListRealmArr: claimList)
        }
        
    }
    
    func sendClaimList(){
        
        var claimListArr = [ClaimModel]()
        let claimListRealmArr = claimRealmManagerProtocol.getClaimList()
        
        for claimRealm in claimListRealmArr{
            let claimModel = ClaimModel(claimName: claimRealm.claimName, claimDate: claimRealm.claimDate, claimStatus: claimRealm.claimStatus)
            
            claimListArr.append(claimModel)
        }
        self.claimsListViewController?.getClaimList(claimListArr: claimListArr)
    }
    
    func sendClaimList(claimListRealmArr: Results<ClaimRealm>){
        var claimListArr = [ClaimModel]()
        for claimRealm in claimListRealmArr{
            let claimModel = ClaimModel(claimName: claimRealm.claimName, claimDate: claimRealm.claimDate, claimStatus: claimRealm.claimStatus)
            
            claimListArr.append(claimModel)
        }
        self.claimsListViewController?.getClaimList(claimListArr: claimListArr)
    }
}
