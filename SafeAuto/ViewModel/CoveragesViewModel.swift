//
//  CoveragesViewModel.swift
//  SafeAuto
//
//  Created by ashwini on 10/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class CoveragesViewModel: NSObject {
    
    var coveragesViewController : CoveragesViewController? = nil
    var vehicleCoverageRealmManagerProtocol: VehicleCoverageRealmManagerProtocol = VehicleCoverageRealmManager()
    var vehicleRealmManagerProtocol: VehicleRealmManagerProtocol = VehicleRealmManager()
    
    init(coveragesViewController : CoveragesViewController?) {
        super.init()
        guard let coveragesVC =  coveragesViewController else {
            return
        }
        self.coveragesViewController = coveragesVC
    }
    
    //MARK: - Temporary Methods
    
    func tempData(){
        if(coveragesViewController != nil){
            let liabilityCoverage1 = LiabilityCoverageModel(coverageName: "Bodily Injury Liability", coverageAmountPerAccident: "$10,000.00", coverageAmountPerPerson: "$10,000.00")
            let liabilityCoverage2 = LiabilityCoverageModel(coverageName: "Property Damage Liability", coverageAmountPerAccident: "$10,000.00", coverageAmountPerPerson: "-")
            let liabilityCoverage3 = LiabilityCoverageModel(coverageName: "Uninsured - Bodily Injury", coverageAmountPerAccident: "$10,000.00", coverageAmountPerPerson: "$10,000.00")
            let liabilityCoverage4 = LiabilityCoverageModel(coverageName: "Uninsured - property Damage", coverageAmountPerAccident: "$10,000.00", coverageAmountPerPerson: "-")
            let liabilityCoverage5 = LiabilityCoverageModel(coverageName: "Personal Injury Protection", coverageAmountPerAccident: "$10,000.00", coverageAmountPerPerson: "-")
            
            self.coveragesViewController!.liabilityTypeArr.append(liabilityCoverage1)
            self.coveragesViewController!.liabilityTypeArr.append(liabilityCoverage2)
            self.coveragesViewController!.liabilityTypeArr.append(liabilityCoverage3)
            self.coveragesViewController!.liabilityTypeArr.append(liabilityCoverage4)
            self.coveragesViewController!.liabilityTypeArr.append(liabilityCoverage5)
            
            
            
            let nonCoverageModel1 = NonCoverageModel(imageName: "tyreIcon", nonCoverageTypeName: "TYRE, TUBES & ENGINE")
            let nonCoverageModel2 = NonCoverageModel(imageName: "carIcon", nonCoverageTypeName: "NON-ACCIDENTAL DAMAGE")
            let nonCoverageModel3 = NonCoverageModel(imageName: "illegalDrivingIcon", nonCoverageTypeName: "ILLEGAL DRIVING")
            
            self.coveragesViewController!.nonCoverageArr.append(nonCoverageModel1)
            self.coveragesViewController!.nonCoverageArr.append(nonCoverageModel2)
            self.coveragesViewController!.nonCoverageArr.append(nonCoverageModel3)
            
            self.coveragesViewController!.nonCoverageCollectionView.reloadData()
            self.getCoverageDetail()
            self.coveragesViewController!.coverageTableView.reloadData()
        }
    }
    
    func getCoverageDetail(){
        guard let policyNumber = self.coveragesViewController?.policyNumber else { return }
        let vehicleList = self.vehicleRealmManagerProtocol.getVehicleList(policyNumber: policyNumber)
        var coverageDetailArr = [CoverageDetailModel]()
        
        for vehicleRealm in vehicleList{
            let vehicleCoverageList = self.vehicleCoverageRealmManagerProtocol.getVehicleCoverageList(vehicleCode: vehicleRealm.vehicleCode)
            var vehicleCovArr = [VehicleCoverageModel]()
            
            for vehicleCovRealm in vehicleCoverageList{
                let vehicleCovModel = VehicleCoverageModel(vehicleCode: vehicleCovRealm.vehicleCode, coverageType: vehicleCovRealm.coverageType, isCoverageProvided: vehicleCovRealm.isCoverageProvided)
                
                vehicleCovArr.append(vehicleCovModel)
                
            }
            
            let coverageDetailModel = CoverageDetailModel(vehicleName: vehicleRealm.vehicleName, vehicleCoverageArr: vehicleCovArr)
            coverageDetailArr.append(coverageDetailModel)
        }
        
        self.coveragesViewController?.coverageDetailArr = coverageDetailArr
        
    }
    
}
