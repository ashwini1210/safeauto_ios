//
//  DashboardViewModel.swift
//  SafeAuto
//
//  Created by ashwini on 18/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class DashboardViewModel: NSObject {
    
    var dashboardViewController : DashboardViewController? = nil
    
    init(dashboardViewController : DashboardViewController?) {
        super.init()
        guard let dashboardVC =  dashboardViewController else {
            return
        }
        self.dashboardViewController = dashboardVC
    }
    
    /*
     * Method to create left Button of Navigation Bar
     */
    func addLeftBarButtonItem() -> [UIBarButtonItem]?{
        var navBarButtonItem : [UIBarButtonItem] = []
        var leftBarButtonItem1: UIBarButtonItem? = nil
        
        leftBarButtonItem1 = AppUtils.createFontAwesomeButton(buttonName: "bars", viewController: dashboardViewController!, buttTag: AppConstants.NAVIGATION_DRAWER_BUTTON)
        if(leftBarButtonItem1 != nil){
            navBarButtonItem.append(leftBarButtonItem1!)
        }
        
        //Add fixedSpace to make titleview in centre
        let fixedItem = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        fixedItem.width = 40
        navBarButtonItem.append(fixedItem)
        
        //Add flexibleSpace to make titleview in centre
        let flexibleSpace = UIBarButtonItem.init(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        flexibleSpace.width = 40
        navBarButtonItem.append(flexibleSpace)
        
        return navBarButtonItem
    }
    
    
    
    /*
     * Method to create Right Button of Navigation Bar
     */
    func addRightBarButtonItem() -> [UIBarButtonItem]?{
        var navBarButtonItem : [UIBarButtonItem] = []
        
        var rightBarButtonItem1: UIBarButtonItem? = nil
        var rightBarButtonItem2: UIBarButtonItem? = nil
        
        rightBarButtonItem1 = AppUtils.createFontAwesomeButton(buttonName: "bell", viewController: dashboardViewController!, buttTag: AppConstants
            .NAVIGATION_NOTIFICATION_BUTTON)
        if(rightBarButtonItem1 != nil){
            navBarButtonItem.append(rightBarButtonItem1!)
        }
        
        rightBarButtonItem2 = AppUtils.createFontAwesomeButton(buttonName: "addresscard", viewController: dashboardViewController!, buttTag: AppConstants.NAVIGATION_ADDRESS_CARD_BUTTON)
        if(rightBarButtonItem2 != nil){
            navBarButtonItem.append(rightBarButtonItem2!)
        }        
        return navBarButtonItem
    }
    
    /*
     * Method to create imageview at centre of Navigation Bar
     */
    func addImageInTitleBar() -> UIImageView?{
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        let image = UIImage(named: "safeAuto")
        imageView.image = image
        return imageView
    }
    
    
    /*
     * Method to adjust tableview height as per policy detail field data
     */
    func adjustTableViewHeightForPolicyDetail(){
        if(dashboardViewController!.dashboardDataModel != nil){
            let array = dashboardViewController!.dashboardDataModel!.dashboardModelArray
            if(array.count<3){
                DispatchQueue.main.async {
                    //This code will run in the main thread:
                    var tbframe = self.dashboardViewController!.tableViewFrame //change tableview frame
                    let viewFrame = self.dashboardViewController!.policyDetailView //change frame of view containing table view
                    tbframe!.size.height = self.dashboardViewController!.policyDetailTableView.contentSize.height
                    viewFrame!.frame.size.height = tbframe!.size.height
                    self.dashboardViewController!.policyDetailTableView.frame = tbframe!
                    self.dashboardViewController!.policyDetailView = viewFrame!
                    self.dashboardViewController!.policyDetailTableView.reloadData()
                }
            }else{
                self.dashboardViewController!.policyDetailTableView.frame = self.dashboardViewController!.tableViewFrame!
                self.dashboardViewController!.policyDetailView.frame = self.dashboardViewController!.policyDetailViewFrame!                
            }
            
        }      
    }
    
}



