//
//  VehicleListViewModel.swift
//  SafeAuto
//
//  Created by ems on 16/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
class VehicleListViewModel: NSObject{
    var vehicleListViewController: VehiclesListViewController? = nil
    var vehicleRealmManagerProtocol: VehicleRealmManagerProtocol = VehicleRealmManager()
    
    init(vehicleListViewController : VehiclesListViewController?) {
        super.init()
        guard let vehicleListVC =  vehicleListViewController else {
            return
        }
        self.vehicleListViewController = vehicleListVC
    }
    
    
    func getData(){
        getVehicleListData()
    }
    
    func vehicleListData(){
        //        var vehicleListArr: [VehicleModel] = [VehicleModel]()
        //        let vehicleModel1 = VehicleModel(vehicleName: "2018 | BMW | Z4", vehicleCode: "LJCPCBLCX11000232", isVehicleAffected: false, policyNumber: "")
        //        let vehicleModel2 = VehicleModel(vehicleName: "2017 | Mercedes-Benz | 4Matic GLC", vehicleCode: "LJCPCBLCX11449823", isVehicleAffected: false, policyNumber: "")
        //        let vehicleModel3 = VehicleModel(vehicleName: "2016 | Audi | A4", vehicleCode: "LJCPCBLCX11263746", isVehicleAffected: false, policyNumber: "")
        //
        //        vehicleListArr.append(vehicleModel1)
        //        vehicleListArr.append(vehicleModel2)
        //        vehicleListArr.append(vehicleModel3)
        //
        //        vehicleRealmManagerProtocol.removeVehicleList()
        //        vehicleRealmManagerProtocol.saveVehicleList(vehicleArr: vehicleListArr)
        //        getVehicleListData()
        
        
    }
    
    func getVehicleListData(){
        print(">>>>>>@@@@@\(self.vehicleListViewController!.policyNumber)")
        let vehicleListArray = vehicleRealmManagerProtocol.getVehicleList(policyNumber: self.vehicleListViewController!.policyNumber)
        
        
        self.vehicleListViewController?.getVehicleListData(vehicleListArray: vehicleListArray)
    }
    
}

