
import Foundation

class AppConstants{
    
    static let TITLE_SEGMENT_CLICKED = 1
    static let SUFFIX_SEGMENT_CLICKED = 2
    static let GENDER_SEGMENT_CLICKED = 3
    static let ACCIDENT_SEGMENT_CLICKED = 4
    static let NO_OF_VIOLENCE_SEGMENT_CLICKED = 5
    static let NO_OF_ACCIDENT_SEGMENT_CLICKED = 6
    static let NAVIGATION_DRAWER_BUTTON = 1;
    static let NAVIGATION_NOTIFICATION_BUTTON = 2;
    static let NAVIGATION_ADDRESS_CARD_BUTTON = 3;
    static let BACK_BUTTON = 4;
 
    static let VEHICLE_IMAGE_OF_CLAIM_DETAIL_SCR = "VEHICLE_IMAGE_OF_CLAIM_DETAIL_SCR"
    static let VEHICLE_IMAGE_OF_RAISE_CLAIM_SCR = "VEHICLE_IMAGE_OF_RAISE_CLAIM_SCR"
    static let DATE_FORMAT = "dd/MM/yyyy"
    static let TIME_FORMAT = "hh:mm:ss"
    static let CURRENT_CITY = "CURRENT_CITY"
    static let CURRENT_STATE = "CURRENT_STATE"
    static let CURRENT_COUNTRY = "CURRENT_COUNTRY"
    static let PREVIOUS_SCREEN = "PREVIOUS_SCREEN"
    static let SUBMIT_CLAIM_VC = "SUBMIT_CLAIM_VC"
    static let EMAIL_ERROR_MESSAGE = "Please Enter Correct Email"
    static let PHONE_ERROR_MESSAGE = "Please Enter Correct Mobile Number"
    static let POLICY_ERROR_MESSAGE = "Please Enter Correct Policy Number"
    static let PASSWORD_ERROR_MESSAGE = "Please Enter Correct Password"
    static let FIRST_NAME_ERROR_MESSAGE = "Please Enter First Name"
    static let LAST_NAME_ERROR_MESSAGE = "Please Enter Last Name"
    static let POLICY_NUMBER_ERROR_MESSAGE = "Please Enter Correct Policy Number"
    static let VEHICLE_NUMBER_ERROR_MESSAGE = "Please Enter correct Vehicle Number"
    static let REGISTER_SCREEN_CLOSE_IDENTIFIER = "REGISTER_SCREEN_CLOSE_IDENTIFIER"
    static let ALERT_TITLE = "Alert"
    static let POLICY_NUMBER_NOT_EXIST_ERROR = "Policy Number Does Not Exist"
    static let OK_BUTTON = "OK"
    static let CANCEL_BUTTON = "CANCEL"
    static let TRY_AGAIN_ERROR = "Something Went Wrong \nPlease Try Again"
    static let FACEBOOK_LOGIN = "FACEBOOK_LOGIN"
    static let GMAIL_LOGIN = "GMAIL_LOGIN"
    static let NORMAL_LOGIN = "NORMAL_LOGIN"
    static let TOUCH_ID_ALER_ERROR_TITLE = "Please Authenticate"
    static let TOUCH_ID_ALERT_MESSAGE1 = "SafeAuto protects your data to avoid unauthorised access.\nPlease unlock SafeAuto to continue"
    static let TOUCH_ID_ALERT_MESSAGE2 = "Authentication Is Required For Access"

    //Colors
    static let NAV_BAR_COLOR: Int = 0xC91A00
    static let CUSTOM_BLACK_BACKGROUND_COLOR: Int = 0x333333
    static let CUSTOM_BLUE_COLOR: Int = 0x509EC0
    static let CUSTOM_GRAY_COLOR: Int = 0xF2F2F2
    static let CUSTOM_RED_COLOR: Int = 0xC91A00
    static let CUSTOM_REASONFORCLAIM_BACKGROUND_COLOR: Int = 0xFBFBFB
    static let CUSTOM_YOUARENOTCOVERFOR_COLLECTIONVIEW_BACKGROUND_COLOR: Int = 0xF9F9F9

    //Notifications
    static let NOTIFICATION_POLICY_NUMBER = "NOTIFICATION_POLICY_NUMBER"
    //PREFERENCE
    static let PREFERENCE_POLICY_NUMBER = "PREFERENCE_POLICY_NUMBER"
    static let PREFERENCE_PASSWORD = "PREFERENCE_PASSWORD"
    static let PREFERENCE_IS_LOGIN = "PREFERENCE_IS_LOGIN"


    //MARK: - Tags Constant
    static let MAKE_TABLEVIEW_CONTAINERVIEW_TAG = 1994
    static let MAKE_TABLEVIEW_TAG = 19941
    
    //MARK: - StoryBoard Ids
    static let HOME_NAVIGATION_VC = "HOME_VIEW_CONTROLLER"
    static let SIDE_MENU_VC = "SIDE_MENU_VC"
    static let HAMBURGER_CONTAINER_VC = "HAMBURGER_CONTAINER_VC"
}

