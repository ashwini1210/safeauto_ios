//
//  GlassTypeModel.swift
//  SafeAuto
//
//  Created by ems on 04/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
struct GlassTypeModel{
    var glassTypeName: String
    var glassTypeFirstImageName: String
    var glassTypeSecondImageName: String
    var isGlassTypeSelected: Bool
}
