//
//  VehicleModel.swift
//  SafeAuto
//
//  Created by ems on 04/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
struct VehicleModel{
    var vehicleName: String
    var vehicleCode: String
    var isVehicleAffected: Bool
    var policyNumber: String
}
