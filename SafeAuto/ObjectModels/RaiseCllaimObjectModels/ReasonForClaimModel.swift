//
//  ReasonForClaimModel.swift
//  SafeAuto
//
//  Created by ems on 03/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
struct ReasonForClaimModel{
    var reasonImageName: String
    var reasonName: String
    var isReasonSelected: Bool
}
