//
//  NotificationModel.swift
//  SafeAuto
//
//  Created by ashwini on 20/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
struct NotificationModel{
    
    var paymentDetail: String
    var date: String
    var newNotification: Bool

}
