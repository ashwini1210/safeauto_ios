//
//  CoverageDetailModel.swift
//  SafeAuto
//
//  Created by Prathamesh Salvi on 29/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
struct CoverageDetailModel{
    
    var vehicleName: String
    var vehicleCoverageArr: [VehicleCoverageModel]
}
