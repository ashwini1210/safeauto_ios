//
//  DriverModel.swift
//  PractiseMajesco
//
//  Created by ems on 26/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import Foundation
struct DriverModel{
    
    var name: String
    var dateOfBirth: String
    var license: String
    var profileImage:String

}
