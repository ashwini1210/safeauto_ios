//
//  ClaimModel.swift
//  PractiseMajesco
//
//  Created by ems on 26/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import Foundation

struct ClaimModel{
    
    var claimName: String
    var claimDate: String
    var claimStatus: String
    
}
