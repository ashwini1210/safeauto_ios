//
//  RegisterModel.swift
//  SafeAuto
//
//  Created by ashwini on 24/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
struct RegisterModel{
    var email: String = ""
    var password: String = ""
    var policyNumber: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var phoneNumber: String = ""
    var vehicleNumber: String = ""
    var gender: String = ""
}
