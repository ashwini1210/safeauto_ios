//
//  DashboardDataModel.swift
//  SafeAuto
//
//  Created by ashwini on 08/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit
import SwiftyJSON

class DashboardDataModel: BaseModel {
    
    var dashboardModelArray = [DashboardModel]()
    var policyRealmManagerProtocol: PolicyRealmManagerProtocol = PolicyRealmManager()
    var vehicleRealmManagerProtocol: VehicleRealmManagerProtocol = VehicleRealmManager()
    var vehicleCoverageRealmManagerProtocol: VehicleCoverageRealmManagerProtocol = VehicleCoverageRealmManager()
    var vehicleListArr: [VehicleModel] = [VehicleModel]()
    var policyListArr: [PolicyModel] = [PolicyModel]()
    
    override init(delegate: HttpResponseProtocol) {
        super.init(delegate: delegate)
        setData()
    }
    
    func setData(){
        //        let dashboardModel1 = DashboardModel(policyNumber: "SF294AK885", MakeModel: "2018-BMW-Z4 \n2017-Merc-4matic")
        //        dashboardModelArray.append(dashboardModel1)
        //        let dashboardModel2 = DashboardModel(policyNumber: "SF294AK885", MakeModel: "2017-Audi-A4")
        //        dashboardModelArray.append(dashboardModel2)
        //
        //
        //        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
        //            self.delegate.onHttpResponse(isSuccess: true, responseJson: nil, error: nil, anyIdentifier: nil)
        //        }
        
        let policyListRealm = self.policyRealmManagerProtocol.getPolicyList()
        self.setVehicleCoverageData()
        
        if policyListRealm.count > 1{
            getList(policyListArr: policyListRealm)
            
        }else{
            populateRealm()
            addPoliciesRealm(policiesArr: self.policyListArr)
            addVehiclesRealm(vehiclesArr: self.vehicleListArr)
            getList()
        }
    }
    
    func populateRealm(){
        let vehicleModel1 = VehicleModel(vehicleName: "2018 | BMW | Z4", vehicleCode: "LJCPCBLCX11000232", isVehicleAffected: false, policyNumber: "SF294AK885")
        let vehicleModel2 = VehicleModel(vehicleName: "2017 | Mercedes-Benz | 4Matic GLC", vehicleCode: "LJCPCBLCX11449823", isVehicleAffected: true, policyNumber: "SF294AK885")
        let vehicleModel3 = VehicleModel(vehicleName: "2016 | Audi | A4", vehicleCode: "LJCPCBLCX11263746", isVehicleAffected: false, policyNumber: "SF294AK886")
        
        vehicleListArr.append(vehicleModel1)
        vehicleListArr.append(vehicleModel2)
        vehicleListArr.append(vehicleModel3)
        
        
        let policyModel1 = PolicyModel(policyNumber: "SF294AK885")
        let policyModel2 = PolicyModel(policyNumber: "SF294AK886")
        
        policyListArr.append(policyModel1)
        policyListArr.append(policyModel2)
    }
    
    func addDataInVehiclesRealm(){
        let policyListRealm = self.policyRealmManagerProtocol.getPolicyList()
        
        if policyListRealm.count > 1{
            getList(policyListArr: policyListRealm)
            
        }else{
            populateRealm()
            addPoliciesRealm(policiesArr: self.policyListArr)
            addVehiclesRealm(vehiclesArr: self.vehicleListArr)
            getList()
        }
        
    }
    
    func addPoliciesRealm(policiesArr: [PolicyModel]){
        self.policyRealmManagerProtocol.savePolicyList(policyArr: policiesArr)
        
    }
    
    func addVehiclesRealm(vehiclesArr: [VehicleModel]){
        self.vehicleRealmManagerProtocol.saveVehicleList(vehicleArr: vehiclesArr)
    }
    
    func getList(policyListArr: [PolicyModel]){
        
        for policyObj in policyListArr{
            var vehicleListArr = self.vehicleRealmManagerProtocol.getVehicleList(policyNumber: policyObj.policyNumber)
            var makeModel: String = ""
            
            if vehicleListArr.count > 0{
                if vehicleListArr.count > 1{
                    makeModel = "\(vehicleListArr[0].vehicleName) \n\(vehicleListArr[1].vehicleName)"
                }else{
                    makeModel = "\(vehicleListArr[0].vehicleName)"
                }
            }
            
            
            let dashBoardModel = DashboardModel(policyNumber: policyObj.policyNumber, MakeModel: makeModel)
            self.dashboardModelArray.append(dashBoardModel)
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
            self.delegate.onHttpResponse(isSuccess: true, responseJson: nil, error: nil, anyIdentifier: nil)
        }
        
    }
    
    func setVehicleCoverageData(){
        let vehicleCovList = self.vehicleCoverageRealmManagerProtocol.getVehicleCoverageList()
        var vehiclesCoverageArr = [VehicleCoverageModel]()
        if vehicleCovList.count <= 4{
            let vehicleCoverageModel1 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11000232", coverageType: "Comprehensive", isCoverageProvided: true)
            let vehicleCoverageModel2 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11000232", coverageType: "Collision", isCoverageProvided: true)
            let vehicleCoverageModel3 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11000232", coverageType: "Towing & Labor", isCoverageProvided: false)
            let vehicleCoverageModel4 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11000232", coverageType: "Rental Reimbursement", isCoverageProvided: true)
            
            let vehicleCoverageModel5 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11449823", coverageType: "Comprehensive", isCoverageProvided: true)
            let vehicleCoverageModel6 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11449823", coverageType: "Collision", isCoverageProvided: false)
            let vehicleCoverageModel7 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11449823", coverageType: "Towing & Labor", isCoverageProvided: false)
            let vehicleCoverageModel8 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11449823", coverageType: "Rental Reimbursement", isCoverageProvided: true)
            
            let vehicleCoverageModel9 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11263746", coverageType: "Comprehensive", isCoverageProvided: true)
            let vehicleCoverageModel10 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11263746", coverageType: "Collision", isCoverageProvided: false)
            let vehicleCoverageModel11 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11263746", coverageType: "Towing & Labor", isCoverageProvided: false)
            let vehicleCoverageModel12 = VehicleCoverageModel(vehicleCode: "LJCPCBLCX11263746", coverageType: "Rental Reimbursement", isCoverageProvided: true)
            
            vehiclesCoverageArr.append(vehicleCoverageModel1)
            vehiclesCoverageArr.append(vehicleCoverageModel2)
            vehiclesCoverageArr.append(vehicleCoverageModel3)
            vehiclesCoverageArr.append(vehicleCoverageModel4)
            vehiclesCoverageArr.append(vehicleCoverageModel5)
            vehiclesCoverageArr.append(vehicleCoverageModel6)
            vehiclesCoverageArr.append(vehicleCoverageModel7)
            vehiclesCoverageArr.append(vehicleCoverageModel8)
            vehiclesCoverageArr.append(vehicleCoverageModel9)
            vehiclesCoverageArr.append(vehicleCoverageModel10)
            vehiclesCoverageArr.append(vehicleCoverageModel11)
            vehiclesCoverageArr.append(vehicleCoverageModel12)
            
            self.vehicleCoverageRealmManagerProtocol.saveVehicleCoverageList(vehicleCoverageList: vehiclesCoverageArr)
        }
    }
    
    //    func getVehicleModelArrFromRealmResults(vehicleListResults: Results<VehicleRealm>) -> [VehicleModel]{
    //        var vehicleArr = [VehicleModel]()
    //        for vehicleObj in vehicleListResult!{
    //
    //            print(">>>>>>>>> \(vehicleObj.policyNumber)")
    //            let vehicleModel = VehicleModel(vehicleName: vehicleObj.vehicleName, vehicleCode: vehicleObj.vehicleCode, isVehicleAffected: vehicleObj.isVehicleAffected, policyNumber: vehicleObj.policyNumber)
    //
    //            vehicleArr!.append(vehicleModel)
    //        }
    //
    //        return vehicleArr
    //    }
    
    func getList(){
        let policyListArr = self.policyRealmManagerProtocol.getPolicyList()
        for policyObj in policyListArr{
            var vehicleListArr = self.vehicleRealmManagerProtocol.getVehicleList(policyNumber: policyObj.policyNumber)
            var makeModel: String = ""
            if vehicleListArr.count > 1{
                makeModel = "\(vehicleListArr[0].vehicleName) \n\(vehicleListArr[1].vehicleName)"
            }else{
                makeModel = "\(vehicleListArr[0].vehicleName)"
            }
            
            let dashBoardModel = DashboardModel(policyNumber: policyObj.policyNumber, MakeModel: makeModel)
            self.dashboardModelArray.append(dashBoardModel)
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
            self.delegate.onHttpResponse(isSuccess: true, responseJson: nil, error: nil, anyIdentifier: nil)
        }
    }
    
    
}
