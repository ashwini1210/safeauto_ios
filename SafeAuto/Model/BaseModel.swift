//
//  BaseModel.swift
//  SafeAuto
//
//  Created by ashwini on 08/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit
import UIKit
import SwiftyJSON
import Alamofire

class BaseModel: NSObject {
    
    //Variable to hold value of HttpResponseProtocol
    public var delegate : HttpResponseProtocol!
    
    //Variable to store value of JSON Responses of server
    public var responseJSONData: JSON = JSON.null
    
    //Variable to store success Responses of server
    public var isSuccess = false
    
    
    override init(){
        super.init()
    }
    
    init(delegate : HttpResponseProtocol, identifier : String? = nil) {
        super.init()
        self.delegate = delegate
        
    }
    
    init(delegate : HttpResponseProtocol) {
        super.init()
        self.delegate = delegate
        
    }
}
