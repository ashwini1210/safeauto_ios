//
//  String.swift
//  SafeAuto
//
//  Created by ashwini on 26/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String,textSize: Int,textColor: UIColor) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Bold", size: CGFloat(textSize))!,NSAttributedString.Key.foregroundColor : textColor]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String,textSize: Int,textColor: UIColor) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Medium", size: CGFloat(textSize))!,NSAttributedString.Key.foregroundColor : textColor]
        let normal = NSMutableAttributedString(string:text, attributes: attrs)
        append(normal)
        
        return self
    }
}

extension Range where Bound == Int {
    var random: Int {
        return Int.random(in: self)
    }
    func random(_ n: Int) -> [Int] {
        return (0..<n).map { _ in random }
    }
}
extension ClosedRange where Bound == Int {
    var random: Int {
        return Int.random(in: self)
    }
    func random(_ n: Int) -> [Int] {
        return (0..<n).map { _ in random }
    }
}
