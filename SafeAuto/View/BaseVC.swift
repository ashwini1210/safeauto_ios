import UIKit
import SwiftyJSON
import Alamofire

class BaseVC: UIViewController,HttpResponseProtocol   {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var activityIndicator: UIActivityIndicatorView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //changeStatusBarColour()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func fontAwsomButtonClicked(sender: Any) {
        super.fontAwsomButtonClicked(sender: sender)
    }
    
    /*
     * Method to change colour of Staus bar
     */
    func changeStatusBarColour(){
        guard let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else {
            return
        }
        statusBarView.backgroundColor = UIColor.white
    }
    
    /*
     * Callback method of HttpresponseProtocol
     */
    func onHttpResponse(isSuccess: Bool, responseJson: JSON?, error: AnyObject?, anyIdentifier: String?) {
        
    }
    
    /*
     * Display alert with two buttons
     */
    func displayAlert(title:String,message: String,btn1:String?,btn2:String?){
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        if(btn1 != nil){
            alertController.addAction(UIAlertAction(title: btn1, style: UIAlertAction.Style.default,handler: nil))
        }
        if(btn2 != nil){
            alertController.addAction(UIAlertAction(title: btn2, style: UIAlertAction.Style.cancel,handler: nil))
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    func displayConfirmationAlert(title:String,message: String,btn1:String?,btn2:String?, handler : ((UIAlertAction) -> Void)? = nil){
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        if(handler != nil){
            if(btn1 != nil){
                alertController.addAction(UIAlertAction(title: btn1, style: UIAlertAction.Style.default,handler: handler))
            }
            if(btn2 != nil){
                alertController.addAction(UIAlertAction(title: btn2, style: UIAlertAction.Style.default,handler: nil))
            }
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
}
