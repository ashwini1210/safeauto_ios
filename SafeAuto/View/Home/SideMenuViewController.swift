//
//  SideMenuViewController.swift
//  SafeAuto
//
//  Created by Prathamesh Salvi on 03/06/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var profileContainerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    //MARK: - Variables
    var registerRealmManagerProtocol: RegisterRealmManagerProtocol = RegisterRealmManager()
    
    //MARK: - Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Setup UI
    func setUpViews(){
        self.profileImageView.makeRound()
        self.profileImageView.image = UIImage(named: "profileIcon")
        
        let registerModel = self.registerRealmManagerProtocol.getRegisterModel()
        self.nameLabel.text = "\(registerModel.firstName) \(registerModel.lastName)"
        
        self.profileContainerView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_RED_COLOR)
        
        
    }
    
    //MARK: - Action Methods
    @IBAction func logoutBtnAction(_ sender: UIButton) {
        self.callLogout()
    }
    
    //MARK: - Logic Methods
    func callLogout(){
        AppPreference.getInstance().setBoolean(AppConstants.PREFERENCE_IS_LOGIN, value: true)
        launchScreen()
    }
    
    func launchScreen(){
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
            let storyBoard = UIStoryboard(name: "InitialScreen", bundle: nil)
            let VC1 = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(VC1, animated: true, completion: nil)
//        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
