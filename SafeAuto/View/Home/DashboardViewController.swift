//
//  ViewController.swift
//  SafeAuto
//
//  Created by ashwini on 18/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit
import SwiftIconFont
import SwiftyJSON

class DashboardViewController: BaseVC,ScreenCloseProtocol,UIGestureRecognizerDelegate {
    
    /******* Payment Detail View *******/
    @IBOutlet weak var paymentDetailView: UIView!
    @IBOutlet weak var upcomingPaymentLabel: UILabel!
    @IBOutlet weak var upcomingAmountLabel: UILabel!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var scheduleDateLabel: UILabel!
    
    /******* Policy Detail View *******/
    @IBOutlet weak var policyDetailView: UIView!
    @IBOutlet weak var policyDetailTableView: UITableView!
    
    /******* Product Detail View *******/
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productCollectionView: UICollectionView!
    var policyDetailTableViewFrame : CGRect? = nil
    
    /******* Bottom View *******/
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var toeButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var startDriveButton: UIButton!
    @IBOutlet weak var startDriveCarImageButton: UIButton!
    @IBOutlet weak var startDriveLabel: UILabel!
    
    var tableViewFrame : CGRect?
    var policyDetailViewFrame : CGRect?
    var dashboardViewModel: DashboardViewModel? = nil
    var backgorundShadowView : UIView? = nil
    var dashboardDataModel: DashboardDataModel? = nil
    var iDCardView : IDCardView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeVar()
        //        UIFont.familyNames.forEach({ familyName in
        //            let fontNames = UIFont.fontNames(forFamilyName: familyName)
        //            print(familyName, fontNames)
        //        })
        //
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addDrawerButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    /*
     * Method to initialize all global variables
     */
    func initializeVar(){
        dashboardViewModel = DashboardViewModel(dashboardViewController: self)
        dashboardDataModel = DashboardDataModel.init(delegate: self)
        AppUtils.createRoundCornerView(view: payButton, cornerRadius: 15)
        AppUtils.addFontAwsomToButton(button: chatButton, fontAwsomName: "comments", textSize: 30, textcolour: UIColor.white)
        AppUtils.addFontAwsomToButton(button: startDriveCarImageButton, fontAwsomName: "car", textSize: 25, textcolour: UIColor.white)
        tableViewFrame = self.policyDetailTableView.frame
        policyDetailViewFrame = self.policyDetailView.frame
        addTapGestureOnView()
    }
    
    func addTapGestureOnView(){
        let gestureRecognizer = UITapGestureRecognizer(target: self,action: #selector(dismissView))
        gestureRecognizer.cancelsTouchesInView = false
        gestureRecognizer.delegate = self
        self.view.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func dismissView(){
        self.backgorundShadowView?.removeFromSuperview()
        self.iDCardView?.removeFromSuperview()
    }
    
    /*
     * Method to add Buttons on Navigation Bar
     */
    func addDrawerButton(){
        guard dashboardViewModel != nil else {
            return
        }
        self.navigationItem.leftBarButtonItems = dashboardViewModel!.addLeftBarButtonItem()
        self.navigationItem.rightBarButtonItems = dashboardViewModel!.addRightBarButtonItem()
        
        guard let titleView = dashboardViewModel!.addImageInTitleBar() else{
            return
        }
        self.navigationItem.titleView = titleView
    }
    
    /*
     * Method call when click on any FontAwsom button
     */
    override func fontAwsomButtonClicked(sender: Any) {
        let button = sender as! UIButton
        if (button.tag == AppConstants.NAVIGATION_DRAWER_BUTTON){
            HamburgerMenu().triggerSideMenu()
        }
        else if(button.tag == AppConstants.NAVIGATION_NOTIFICATION_BUTTON){
            presentNotificationPopover(sender: sender)
        }
        else if(button.tag == AppConstants.NAVIGATION_ADDRESS_CARD_BUTTON){
            backgorundShadowView = AppUtils.createBackgroundShadowView(X: 0, Y: 0, Width: Int(self.view.frame.width), Height: Int(self.view.frame.height))!
            self.view.addSubview(backgorundShadowView!)
            
            iDCardView = IDCardView.instanceFromNib() as? IDCardView
            iDCardView!.center = self.view.convert(self.view.center, from:self.view.superview)
            self.view.addSubview(iDCardView!)
        }
    }
    
    
    @IBAction func payButtonClicked(_ sender: Any) {
        
    }
    
    @IBAction func toeButtonClicked(_ sender: Any) {
        
    }
    
    @IBAction func chatButtonClicked(_ sender: Any) {
        
    }
    
    @IBAction func startDriveButtonClicked(_ sender: Any) {
        
    }
    
    
    /*
     * Show Popover on click Notification Button
     */
    func presentNotificationPopover(sender: Any){
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "NotificationPopoverViewController") as! NotificationPopoverViewController
        VC1.modalPresentationStyle = .popover
        VC1.preferredContentSize = CGSize(width: VC1.view.frame.width/1.2, height: VC1.view.frame.height/2)
        VC1.popoverPresentationController!.delegate = self
        VC1.popoverPresentationController!.permittedArrowDirections = [.up,.right]
        VC1.popoverPresentationController!.sourceView = sender as? UIView
        VC1.popoverPresentationController!.sourceRect = (sender as! UIButton).bounds
        VC1.delegate = self
        VC1.navController = self.navigationController
        backgorundShadowView = AppUtils.createBackgroundShadowView(X: 0, Y: 0, Width: Int(view.frame.width), Height: Int(view.frame.height))!
        self.view.addSubview(backgorundShadowView!)
        self.present(VC1, animated: true, completion: nil)
    }
    
    func onScreenClose(isClosed: Bool, value: Any?) {
        self.backgorundShadowView?.removeFromSuperview()
    }
    
    override func onHttpResponse(isSuccess: Bool, responseJson: JSON?, error: AnyObject?, anyIdentifier: String?) {
        if(dashboardViewModel != nil){
            dashboardViewModel!.adjustTableViewHeightForPolicyDetail()
        }
        
    }
}

//MARK: - Cell Delegate Protocol
extension DashboardViewController: OnCellElementClickProtocol{
    func onPolicyClicked(policyNumber: String) {
        let storyBoard = UIStoryboard(name: "PolicyDetails", bundle: nil)
        let navController = storyBoard.instantiateViewController(withIdentifier: "DASHBOARD_NAV_CONTROLLER") as! UINavigationController
        let policyVC = navController.topViewController as! PolicyViewController
        policyVC.policyNumber = policyNumber
        self.present(navController, animated: true, completion: nil)
    }
    
    
    
}


/*
 * MARK: - Extension Of UITableView
 */
extension DashboardViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(dashboardDataModel != nil){
            if(dashboardDataModel!.dashboardModelArray.count > 0){
                return dashboardDataModel!.dashboardModelArray.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PolicyDetailTableViewCell", for: indexPath) as! PolicyDetailTableViewCell
        cell.onCellElementClickProtocol = self
        cell.dashboardDataModel = self.dashboardDataModel
        cell.dashboardViewController = self
        cell.indexNo = indexPath.row
        cell.setDataInView(index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

/*
 * Extension Of UICollectionView
 */
extension DashboardViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
        cell.companyLogoImageView.image = UIImage(named: "safeAuto.png")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.productCollectionView.frame.width, height: self.productCollectionView.frame.height)
    }
    
}

/*
 * Extension Of UIPopoverPresentationController
 */
extension DashboardViewController: UIPopoverPresentationControllerDelegate{
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { context in
            // self.popOverViewController?.dismiss(animated: true)
        })
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}



