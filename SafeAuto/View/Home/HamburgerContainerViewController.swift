//
//  HamburgerContainerViewController.swift
//  SafeAuto
//
//  Created by Prathamesh Salvi on 03/06/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class HamburgerContainerViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var sideMenuView: UIView!
    
    //MARK: - Constraints
    
    @IBOutlet weak var sideMenuLeadingConstraint: NSLayoutConstraint!
    
    
    //MARK: - ViewController Variables
    lazy var dashBoardVC: UIViewController? = {
        let dashBoard = self.storyboard?.instantiateViewController(withIdentifier: AppConstants.HOME_NAVIGATION_VC)
        return dashBoard
    }()
    
    lazy var sideMenuVC: UIViewController? = {
        let sideMenu = self.storyboard?.instantiateViewController(withIdentifier: AppConstants.SIDE_MENU_VC)
        return sideMenu
    }()
    
    var openCloseFlag: Bool = true
    var initialPos: CGPoint?
    var touchPos: CGPoint?
    var sideMenuViewMidX: CGFloat?
    var sideTouch: CGFloat = 50
    var sideMenuIsPresent: Bool = false
    let shadowViewTag = 27021994
    var alphaValue: CGFloat?
    
    
    
    //MARK: - Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNotifications()
        displayContainerView()
        //        sideMenuLeadingConstraint.constant = -self.sideMenuView.bounds.width
        
        // Do any additional setup after loading the view, typically from a nib.
        
        //        self.view.isUserInteractionEnabled = true
        // Pan Gesture for Container view
        let panGestureContainerView = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(panGesture:)))
        self.view.addGestureRecognizer(panGestureContainerView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sideMenuLeadingConstraint.constant = -self.sideMenuView.bounds.width
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("before >>>>>>>\(self.sideMenuView.frame.midX)")
        
        
        print("after >>>>>>>\(self.sideMenuView.frame.midX)")
        self.sideMenuViewMidX = self.sideMenuView.frame.midX
        print("side menu width :::::::::\(self.sideMenuView.bounds.width) view width:::::: \(self.view.bounds.width)")
    }
    
    func setUpNotifications(){
        let openCloseSideMenuNotificationName = Notification.Name("notificationOpenOrCloseSideMenu")
        NotificationCenter.default.addObserver(self, selector: #selector(openSideAndCloseMenu), name: openCloseSideMenuNotificationName, object: nil)
        
        let closeSideMenuNotificationName = Notification.Name("notificationCloseSideMenu")
        NotificationCenter.default.addObserver(self, selector: #selector(closeSideMenu), name: closeSideMenuNotificationName, object: nil)
        
        let closeSideMenuWithoutAnimationNotificationName = Notification.Name("notificationCloseSideMenuWithoutAnimation")
        NotificationCenter.default.addObserver(self, selector: #selector(closeWithoutAnimation), name: closeSideMenuWithoutAnimationNotificationName, object: nil)
    }
    
    func displayContainerView(){
        if let vc = dashBoardVC {
            
            self.addChild(vc)
            vc.didMove(toParent: self)
            
            vc.view.frame = self.containerView.bounds
            self.containerView.addSubview(vc.view)
            //            self.currentViewController = vc
        }
    }
    
    func displaySideMenu(){
        if let vc = sideMenuVC{
            self.addChild(vc)
            vc.didMove(toParent: self)
            
            vc.view.frame = self.sideMenuView.bounds
            self.sideMenuView.addSubview(vc.view)
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Selector Methods
    @objc func handlePanGesture(panGesture: UIPanGestureRecognizer){
        touchPos = panGesture.location(in: self.view)
        let translation = panGesture.translation(in: self.view)
        let shadowView = self.containerView.viewWithTag(shadowViewTag)
        if panGesture.state == .began{
            initialPos = touchPos
        }else if panGesture.state == .changed{
            
            let newXpos = ((touchPos?.x)! - (initialPos?.x)!)
            
            if newXpos < 0 && sideMenuIsPresent{
                
                shadowView?.alpha = self.sideMenuView.frame.maxX/(self.view.frame.maxX * 1.5)
                self.sideMenuView.frame = CGRect(x: newXpos, y: 0, width: self.sideMenuView.frame.width, height: self.sideMenuView.frame.height)
                //                self.sideMenuView.center = CGPoint(x: sideMenuView.center.x + translation.x, y: sideMenuView.bounds.midY)
                //                 print(">>>>>>>>>>>>>>\(sideMenuView.center.x + translation.x)")
                //                 print(">>??>>??>>??>>\(translation.x)")
            }else if (initialPos?.x)! < sideTouch{
                self.displaySideMenu()
                if sideMenuView.center.x + translation.x <= self.sideMenuView.bounds.width / 2{
                    
                    self.addShadowView().alpha = self.sideMenuView.frame.maxX/(self.view.frame.maxX * 1.5)
                    self.containerView.addSubview(self.addShadowView())
                    self.sideMenuView.center = CGPoint(x: sideMenuView.center.x + translation.x, y: sideMenuView.bounds.midY)
                    //                    print("????????????\(sideMenuView.center.x + translation.x)")
                    //                    print(">>??>>??>>??>>\(translation.x)")
                    panGesture.setTranslation(CGPoint.zero, in: self.view)
                }
            }
            
        }else if panGesture.state == .ended{
            
            if self.sideMenuView.frame.maxX < self.sideMenuViewMidX!{
                
                UIView.animate(withDuration: 0.18, animations: {
                    self.sideMenuView.frame = CGRect(x: -self.sideMenuView.bounds.width, y: 0, width: self.sideMenuView.frame.width, height: self.sideMenuView.frame.height)
                    
                    shadowView?.alpha = 0
                    self.view.layoutIfNeeded()
                }) { (_) in
                    
                    self.sideMenuIsPresent = false
                    self.sideMenuLeadingConstraint.constant =  -self.sideMenuView.bounds.width
                    shadowView?.removeFromSuperview()
                    self.view.layoutIfNeeded()
                }
            }else{
                print("???>?>?>??>?>?>?>?\(String(describing: self.sideMenuViewMidX!))")
                UIView.animate(withDuration: 0.18, animations: {
                    self.sideMenuView.frame = CGRect(x: 0, y: 0, width: self.sideMenuView.frame.width, height: self.sideMenuView.frame.height)
                    self.view.layoutIfNeeded()
                }) { (_) in
                    self.sideMenuIsPresent = true
                    self.sideMenuLeadingConstraint.constant =  0
                    
                    self.view.layoutIfNeeded()
                    self.sideMenuViewMidX = self.sideMenuView.frame.midX
                    print("pangesture ended center position;;;;;;;;\(self.sideMenuView.center)")
                    print("side menu ended midpoint ;;;;;\(self.sideMenuView.bounds.width / 2)")
                }
            }
            
            
        }
    }
    
    
    @objc func openSideAndCloseMenu(){
        self.containerView.addSubview(self.addShadowView())
        self.displaySideMenu()
        //        UIView.animate(withDuration: 0.2) {
        //            self.addShadowView().alpha = self.sideMenuView.bounds.maxX/(self.view.frame.maxX * 1.5)
        //            self.sideMenuIsPresent = true
        //            self.sideMenuLeadingConstraint.constant = 0
        //            self.view.layoutIfNeeded()
        //        }
        //
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.addShadowView().alpha = self.sideMenuView.bounds.maxX/(self.view.frame.maxX * 1.5)
                        self.sideMenuIsPresent = true
                        self.sideMenuLeadingConstraint.constant = 0
                        self.view.layoutIfNeeded()
        }) { (_) in
            self.sideMenuViewMidX = self.sideMenuView.frame.midX
        }
        
    }
    
    @objc func closeSideMenu(){
        let shadowView = self.view.viewWithTag(shadowViewTag)
        //        UIView.animate(withDuration: 0.2) {
        //            self.sideMenuIsPresent = false
        //            self.sideMenuLeadingConstraint.constant = -self.sideMenuView.bounds.width
        //            shadowView?.alpha = 0
        //            shadowView?.removeFromSuperview()
        //            self.view.layoutIfNeeded()
        //        }
        
        UIView.animate(withDuration: 0.2,
                       animations: {
                        self.sideMenuIsPresent = false
                        self.sideMenuLeadingConstraint.constant = -self.sideMenuView.bounds.width
                        shadowView?.alpha = 0
                        shadowView?.removeFromSuperview()
                        self.view.layoutIfNeeded()
        }) { (_) in
            self.sideMenuViewMidX = self.sideMenuView.frame.midX
        }
    }
    
    @objc func closeWithoutAnimation(){
        let shadowView = self.view.viewWithTag(shadowViewTag)
        self.sideMenuIsPresent = false
        self.sideMenuLeadingConstraint.constant = -self.sideMenuView.bounds.width
        shadowView?.alpha = 0
        shadowView?.removeFromSuperview()
        self.view.layoutIfNeeded()
        self.sideMenuViewMidX = self.sideMenuView.frame.midX
    }
    
    //MARK: - Shadow View
    func addShadowView() -> UIView{
        let shadowView = self.containerView.viewWithTag(shadowViewTag)
        if shadowView != nil{
            return shadowView!
        }else{
            let sView = UIView(frame: self.containerView.bounds)
            sView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            sView.tag = shadowViewTag
            sView.alpha = 0
            sView.backgroundColor = UIColor.black
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(closeSideMenu))
            sView.addGestureRecognizer(recognizer)
            return sView
        }
        
        
    }
    
}

class HamburgerMenu{
    //Class To Implement Easy Functions To Open Or Close RearView
    //Make object of this class and call functions
    func triggerSideMenu(){
        let notificationOpenOrCloseSideMenu = Notification.Name("notificationOpenOrCloseSideMenu")
        NotificationCenter.default.post(name: notificationOpenOrCloseSideMenu, object: nil)
    }
    
    func closeSideMenu(){
        let notificationCloseSideMenu = Notification.Name("notificationCloseSideMenu")
        NotificationCenter.default.post(name: notificationCloseSideMenu, object: nil)
    }
    
    func closeSideMenuWithoutAnimation(){
        let notificationCloseSideMenuWithoutAnimation = Notification.Name("notificationCloseSideMenuWithoutAnimation")
        NotificationCenter.default.post(name: notificationCloseSideMenuWithoutAnimation, object: nil)
    }
    
}
