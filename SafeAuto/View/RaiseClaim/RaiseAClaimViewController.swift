//
//  RaiseAClaimViewController.swift
//  PractiseMajesco
//
//  Created by ems on 22/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit
import CoreLocation

class RaiseAClaimViewController: BaseVC {
    
    //MARK: - Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mainContentView: UIView!
    //Reason for Claim outlets
    @IBOutlet weak var reasonForClaimCollectionViewContainerView: UIView!
    @IBOutlet weak var reasonForClaimCollectionView: UICollectionView!
    
    //Vehicles Affected outlets
    @IBOutlet weak var vehicleAffectedTableView: UITableView!
    @IBOutlet weak var vehicleAffectedContainerView: UIView!
    
    //Incident Time and location outlets
    @IBOutlet weak var incidentTimeAndLocationMainView: UIView!
    @IBOutlet weak var showDateTextField: UITextField!
    @IBOutlet weak var showTimeTextField: UITextField!
    @IBOutlet weak var pickDateBtn: UIButton!
    @IBOutlet weak var selectDateAndTimeJustNowImageView: UIImageView!
    @IBOutlet weak var showAddressTextField: UITextField!
    @IBOutlet weak var selectCurrentLocationImageView: UIImageView!
    
    //Need Assistance Outlets
    @IBOutlet weak var needAssistanceView: UIView!
    @IBOutlet weak var needAssistanceCollectionView: UICollectionView!
    
    //Who was Driving
    @IBOutlet weak var whoWasDrivingContainerView: UIView!
    @IBOutlet weak var whoWasDrivingTableView: UITableView!
    @IBOutlet weak var addUnlistedDriverBtn: UIButton!
    
    //Passenger Info
    @IBOutlet weak var passengerInfoContainerView: UIView!
    @IBOutlet weak var numberOfPassengerSegmentedControl: UISegmentedControl!
    @IBOutlet weak var commentDescriptionTextView: UITextView!
    @IBOutlet weak var commentDescriptionPlaceholderLabel: UILabel!
    
    
    //Submit Claim Btn
    @IBOutlet weak var submitClaimBtn: UIButton!
    
    //MARK: - Constraints
    @IBOutlet weak var collectionViewContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var vehicleAffectedContainerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var incidentTimeAndLocationMainViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var needAsistanceViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var whoWasDrivingContainerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var whoWasDrivingTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var passengerInfoContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var uploadPhotoViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var uploadPhotoCollectionViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Variables
    //Arrays
    var reasonForClaimArr: [ReasonForClaimModel] = [ReasonForClaimModel]()
    var vehicleAffectedArr: [VehicleModel] = [VehicleModel]()
    var needAssistanceArr: [NeedAssistanceModel] = [NeedAssistanceModel]()
    var whoWasDrivingArr: [WhoWasDrivingObjectModel] = [WhoWasDrivingObjectModel]()
    //reason for claim variables
    var isTheftReasonSelected: Bool = false
    // incident Date and Time Variables
    var incidentDatePicker = UIDatePicker()
    var incidentDatePickerContainerView: UIView?
    var incidentDatePickerToolbar: UIToolbar?
    var incidentdatePickerToolbarDoneBtn: UIBarButtonItem?
    var incidentdatePickerToolbarCancelBtn: UIBarButtonItem?
    var isSelectDateAndTimeJustNowImageViewSelected: Bool = false
    var isSelectCurrentLocationImageViewSelected: Bool = false
    // Need Asistance For Variables
    
    //--------------------------
    var backgorundShadowView : UIView? = nil
    var raiseClaimViewModel : RaiseClaimViewModel? = nil
    
    //MARK: - Variable for upload photo view
    @IBOutlet weak var uploadPhotoView: UIView!
    @IBOutlet weak var uploadPhotoSubView: UIView!
    @IBOutlet weak var uploadPhotoCollectionView: UICollectionView!
    let uploadPhotoCellIdentifier = "uploadPhotoCollectionView"
    var imagePicker = UIImagePickerController()
    var selectedImageFromImagePicker: UIImage? = nil
    var activeField: UITextField?
    @IBOutlet weak var locationPin: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var choosePhotoBtn: UIButton!
    
    //Location Manager for current location
    let locationManager = LocationManager.init()
    
    
    //MARK: - Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        tempData()
        setUpGestures()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Setup UI
    func setUpUI(){
        initViews()
        setUpCollectionView()
        setUpTableView()
        setUpNavigationBar()
        setUpIncidentTimeAndLocation()
        setUpWhoWasDrivingView()
        setUpPassengerInfoView()
        setUpUploadPhotoView()
        
        setUpMainHeight()
        registerForKeyboardNotifications()
        self.submitClaimBtn.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
        self.submitClaimBtn.tintColor = UIColor.white
        self.registerTapGesture()
    }
    
    func setUpNavigationBar(){
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 20)!]
    }
    
    func initViews(){
        raiseClaimViewModel = RaiseClaimViewModel(raiseAClaimViewController: self)
        locationManager.startLocationService()
        AppUtils.addFontAwsomToButton(button: pickDateBtn, fontAwsomName: "calendar", textSize: 20, textcolour: UIColor().hexStringToUIColor(hex: "#535353"))
        AppUtils.addFontAwsomToButton(button: locationPin, fontAwsomName: "mapmarker", textSize: 20, textcolour: UIColor().hexStringToUIColor(hex: "#535353"))
        AppUtils.addFontAwsomToButton(button: cameraButton, fontAwsomName: "camera", textSize: 20, textcolour: UIColor().hexStringToUIColor(hex: "#535353"))
        self.mainContentView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
        AppUtils.createRoundCornerView(view: pickDateBtn, cornerRadius: 1, borderWidth: 0.5, borderColor: UIColor.gray.cgColor)
    }
    
    func setUpCollectionView(){
        let cellHeight = 165
        let reasonForClaimContainerViewTitleHeight = 35
        let extraSpacingHeight = 40
        self.collectionViewContainerHeightConstraint.constant = CGFloat(cellHeight * 3 + reasonForClaimContainerViewTitleHeight + extraSpacingHeight)
        self.reasonForClaimCollectionViewContainerView.addBorders(borderWidth: 1.0, borderColor: UIColor.lightGray.cgColor)
        self.reasonForClaimCollectionViewContainerView.roundCorners(cornerRadius: 10.0)
        
        let nib1 = UINib(nibName: "ReasonForClaimCollectionViewCell", bundle: nil)
        self.reasonForClaimCollectionView.register(nib1, forCellWithReuseIdentifier: "reasonForClaimCell")
        self.reasonForClaimCollectionView.delegate = self
        self.reasonForClaimCollectionView.dataSource = self
        
        let needAssistanceTitleViewHeight = 35
        let needAssistanceTitleViewHeightExtraSpacing = 40
        self.needAsistanceViewHeightConstraint.constant = CGFloat(cellHeight * 2 + needAssistanceTitleViewHeight + needAssistanceTitleViewHeightExtraSpacing)
        self.needAssistanceView.addBorders(borderWidth: 1.0, borderColor: UIColor.lightGray.cgColor)
        self.needAssistanceView.roundCorners(cornerRadius: 10.0)
        self.needAssistanceCollectionView.register(nib1, forCellWithReuseIdentifier: "reasonForClaimCell")
        self.needAssistanceCollectionView.delegate = self
        self.needAssistanceCollectionView.dataSource = self
        
    }
    
    func setUpTableView(){
        raiseClaimViewModel!.setUpTableView()
        self.vehicleAffectedTableView.estimatedRowHeight = UITableView.automaticDimension
        let nib = UINib(nibName: "VehicleAffectedTableViewCell", bundle: nil)
        self.vehicleAffectedTableView.register(nib, forCellReuseIdentifier: "vehicleAffectedCell")
        self.vehicleAffectedTableView.delegate = self
        self.vehicleAffectedTableView.dataSource = self
    }
    
    func setUpIncidentTimeAndLocation(){
        raiseClaimViewModel!.setUpIncidentTimeAndLocation()
    }
    
    func setUpWhoWasDrivingView(){
        raiseClaimViewModel!.setUpWhoWasDrivingView()
        let nib = UINib(nibName: "WhoWasDrivingTableViewCell", bundle: nil)
        self.whoWasDrivingTableView.register(nib, forCellReuseIdentifier: "whoWasDrivingCell")
        self.whoWasDrivingTableView.delegate = self
        self.whoWasDrivingTableView.dataSource = self
        
        self.whoWasDrivingTableView.rowHeight = 60
        self.whoWasDrivingTableView.estimatedRowHeight = 60
    }
    
    func setUpPassengerInfoView(){
        commentDescriptionTextView.delegate = self
        self.raiseClaimViewModel?.setUpPassengerInfoView()        
    }
    
    func setUpUploadPhotoView(){
        AppUtils.createRoundCornerView(view: self.choosePhotoBtn, cornerRadius: 1, borderWidth: 0.5, borderColor: UIColor.gray.cgColor)
        self.uploadPhotoCollectionView.register(UINib(nibName: "ClaimPhotoCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: uploadPhotoCellIdentifier)
        self.raiseClaimViewModel?.setUpUploadPhotoMainView()
    }
    
    func setUpMainHeight(){
        
        let height1 = self.collectionViewContainerHeightConstraint.constant + self.vehicleAffectedContainerViewHeightConstraint.constant + self.incidentTimeAndLocationMainViewHeightConstraint.constant +
            self.needAsistanceViewHeightConstraint.constant
        let height2 = self.whoWasDrivingContainerViewHeightConstraint.constant +
            self.passengerInfoContainerHeightConstraint.constant +
            self.uploadPhotoViewHeightConstraint.constant
        let submitClaimBtnHeight = 40
        let extraTempHeight = (15 * 7) + 35 + submitClaimBtnHeight
        print("this is height:::::::::::: \(height1 + height2)")
        self.mainViewHeightConstraint.constant = height1 + height2 + CGFloat(extraTempHeight)
    }
    
    
    //MARK: - Setup Gesture
    func setUpGestures(){
        self.selectDateAndTimeJustNowImageView.isUserInteractionEnabled = true
        let selectDateAndTimeJustNowImageTap = UITapGestureRecognizer(target: self, action: #selector(selectDateAndTimeJustNowImageTapped))
        self.selectDateAndTimeJustNowImageView.addGestureRecognizer(selectDateAndTimeJustNowImageTap)
        
        self.selectCurrentLocationImageView.isUserInteractionEnabled = true
        let selectCurrentLocationImageTap = UITapGestureRecognizer(target: self, action: #selector(selectCurrentLocation))
        self.selectCurrentLocationImageView.addGestureRecognizer(selectCurrentLocationImageTap)
        
    }
    
    
    
    //MARK: - Temp Data
    func tempData(){
        if(raiseClaimViewModel != nil){
            raiseClaimViewModel!.reasonForClaimData()
            raiseClaimViewModel!.vehicleAffectedData()
            raiseClaimViewModel?.needAssistanceData()
            raiseClaimViewModel?.whoWasDrivingData()
        }
    }
    
    
    
    //MARK: - Action Methods
    
    @IBAction func backBarbuttonAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pickDateBtnAction(_ sender: UIButton) {
        showDatePicker()
    }
    
    @IBAction func choosePhotoBtnAction(_ sender: UIButton) {
        openGalleryToSelectPhoto()
    }
    
    @IBAction func clickPhotoBtnAction(_ sender: UIButton) {
        openCameraToClickPhoto()
    }
    
    @IBAction func addUnlistedDriverBtnAction(_ sender: UIButton) {
        createPopoverVC(identifier: "AddDriverViewController")        
    }
    
    @IBAction func submitClaimBtnAction(_ sender: UIButton) {
        createPopoverVC(identifier: "ConfirmContactViewController")
    }
    
    @IBAction func noOfPassengerSegmentedControlAction(_ sender: UISegmentedControl) {
        self.numberOfPassengerSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], for: UIControl.State.normal)
        self.numberOfPassengerSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white], for: .selected)
        self.numberOfPassengerSegmentedControl.tintColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
        self.numberOfPassengerSegmentedControl.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
        
    }
    
    
    
    
    //MARK: - Create Date Picker
    func showDatePicker(){
        //Formate Date
        self.incidentDatePickerContainerView = UIView()
        self.incidentDatePickerContainerView?.frame.size = CGSize(width: self.incidentTimeAndLocationMainView.bounds.width * 0.8, height: self.incidentTimeAndLocationMainView.bounds.height * 0.8)
        self.incidentDatePickerContainerView?.backgroundColor = UIColor.white
        self.incidentDatePickerContainerView?.tag = 20191
        self.incidentDatePickerContainerView?.center = CGPoint(x: self.incidentTimeAndLocationMainView.bounds.midX, y: self.incidentTimeAndLocationMainView.bounds.midY)
        
        self.incidentDatePickerContainerView?.addBorders()
        self.incidentDatePickerContainerView?.roundCorners()
        
        //ToolBar
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: (self.incidentDatePickerContainerView?.bounds.width)!, height: (self.incidentDatePickerContainerView?.bounds.height)! * 0.2))
        toolbar.tag = 20192
        
        //  toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        self.incidentDatePicker = UIDatePicker(frame: CGRect(x: 0, y: toolbar.bounds.maxY, width: (self.incidentDatePickerContainerView?.bounds.width)!, height: (self.incidentDatePickerContainerView?.bounds.height)! * 0.8))
        incidentDatePicker.datePickerMode = .dateAndTime
        
        self.incidentDatePickerContainerView?.addSubview(toolbar)
        self.incidentDatePickerContainerView?.addSubview(incidentDatePicker)
        
        UIView.animate(withDuration: 0.5) {
            self.incidentTimeAndLocationMainView.addSubview(self.incidentDatePickerContainerView!)
        }
        
    }
    
    //MARK: - Selector Methods
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = AppConstants.DATE_FORMAT
        showDateTextField.text = formatter.string(from: incidentDatePicker.date)
        
        formatter.dateFormat = AppConstants.TIME_FORMAT
        showTimeTextField.text = formatter.string(from: incidentDatePicker.date)
        
        self.incidentDatePickerContainerView?.removeFromSuperview()
        
        
        if let v = UIView().viewWithTag(20192){
            v.removeFromSuperview()
        }
    }
    
    @objc func cancelDatePicker(){
        self.incidentDatePickerContainerView?.removeFromSuperview()
    }
    
    @objc func selectDateAndTimeJustNowImageTapped(){
        
        self.raiseClaimViewModel?.selectIncidentTimeAndLocationJustNow()
        
    }
    
    @objc func selectCurrentLocation(){
        self.raiseClaimViewModel?.selectCurrentLocation()
    }
    
    deinit {
        deregisterFromKeyboardNotifications()
    }
    
}
//MARK: - RaiseClaimCellDelegateProtocol
extension RaiseAClaimViewController: RaiseClaimCellDelegateProtocol{
    func whoWasDrivingSelected(indexNo: Int) {
        for i in 0..<self.whoWasDrivingArr.count{
            if i == indexNo{
                self.whoWasDrivingArr[i].isDriverSelected = true
            }else{
                self.whoWasDrivingArr[i].isDriverSelected = false
            }
        }
        
        self.whoWasDrivingTableView.reloadData()
    }
    
    func ifNeedAsistanceTypeSelected(indexNo: Int) {
        if indexNo == 0{
            for i in 0..<self.needAssistanceArr.count{
                if i == 1{
                    self.needAssistanceArr[i].isAssistanceTypeSelected = false
                }else{
                    self.needAssistanceArr[i].isAssistanceTypeSelected = true
                }
            }
        }else{
            for i in 0..<self.needAssistanceArr.count{
                if i == indexNo{
                    self.needAssistanceArr[i].isAssistanceTypeSelected = true
                }else{
                    self.needAssistanceArr[i].isAssistanceTypeSelected = false
                }
            }
        }
        
        self.needAssistanceCollectionView.reloadData()
    }
    
    func reasonTypeSelected(indexNo: Int) {
        for i in 0..<self.reasonForClaimArr.count{
            if self.reasonForClaimArr[indexNo].reasonName == "Theft"
                || self.reasonForClaimArr[indexNo].reasonName == "Glass"
                || self.reasonForClaimArr[indexNo].reasonName == "Weather"
            {
                self.isTheftReasonSelected = true
            }else{
                self.isTheftReasonSelected = false
            }
            
            if i == indexNo{
                self.reasonForClaimArr[i].isReasonSelected = true
            }else{
                self.reasonForClaimArr[i].isReasonSelected = false
            }
        }
        self.resetVehicleType()
        self.vehicleAffectedTableView.reloadData()
        self.reasonForClaimCollectionView.reloadData()
        
    }
    
    func resetVehicleType(){
        for i in 0..<self.vehicleAffectedArr.count{
            self.vehicleAffectedArr[i].isVehicleAffected = false
        }
    }
    
    func ifGlassTypeSelected() {
        let storyBoard = UIStoryboard(name: "RaiseClaim", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "reasonTypePopUpVC")
            as! SelectGlassTypeAffectedViewController
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func vehicleAffectedSelected(indexNo: Int) {
        for i in 0..<self.vehicleAffectedArr.count{
            if isTheftReasonSelected{
                if i == indexNo{
                    if self.vehicleAffectedArr[i].isVehicleAffected == false{
                        self.vehicleAffectedArr[i].isVehicleAffected = true
                    }else{
                        self.vehicleAffectedArr[i].isVehicleAffected = false
                    }
                }
            }else{
                if i == indexNo{
                    self.vehicleAffectedArr[i].isVehicleAffected = true
                }else{
                    self.vehicleAffectedArr[i].isVehicleAffected = false
                }
            }
            
            
        }
        self.vehicleAffectedTableView.reloadData()
    }
}
//MARK: - Collection View
extension RaiseAClaimViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == uploadPhotoCollectionView){
            return 3
        }else if collectionView == reasonForClaimCollectionView{
            return reasonForClaimArr.count
        }else{
            return needAssistanceArr.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == uploadPhotoCollectionView){
            let cell: ClaimPhotoCollectionViewCell = uploadPhotoCollectionView.dequeueReusableCell(withReuseIdentifier: uploadPhotoCellIdentifier, for: indexPath) as! ClaimPhotoCollectionViewCell
            cell.setImageOfClaimVeicle(indexPath: indexPath, identifier: AppConstants.VEHICLE_IMAGE_OF_RAISE_CLAIM_SCR, image: selectedImageFromImagePicker)
            return cell
        }
        else if collectionView == reasonForClaimCollectionView{
            let cell: ReasonForClaimCollectionViewCell = reasonForClaimCollectionView.dequeueReusableCell(withReuseIdentifier: "reasonForClaimCell", for: indexPath) as! ReasonForClaimCollectionViewCell
            
            //UI Setup
            cell.setUpCell(indexPath: indexPath, reasonForClaimArr: reasonForClaimArr)
            cell.raiseClaimCellDelegateProtocol = self
            cell.indexNo = indexPath.item
            cell.isCallFromNeedAssistanceCollectionView = false
            
            return cell
        }else{
            let cell: ReasonForClaimCollectionViewCell = needAssistanceCollectionView.dequeueReusableCell(withReuseIdentifier: "reasonForClaimCell", for: indexPath) as! ReasonForClaimCollectionViewCell
            cell.setUpCellForNeedAssistance(indexPath: indexPath, needAssistanceArr: self.needAssistanceArr)
            cell.raiseClaimCellDelegateProtocol = self
            cell.indexNo = indexPath.item
            cell.isCallFromNeedAssistanceCollectionView = true
            cell.layoutIfNeeded()
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size : CGSize? = nil
        if(collectionView == reasonForClaimCollectionView){
            size = CGSize(width: self.reasonForClaimCollectionView.bounds.width / 2.5, height: 162)
        }else if collectionView == needAssistanceCollectionView{
            size = CGSize(width: self.needAssistanceCollectionView.bounds.width / 2.5, height: 162)
        }
        else{
            size = CGSize(width: self.uploadPhotoCollectionView.frame.width/2, height: self.uploadPhotoCollectionView.frame.height)
        }
        return size!
    }
    
    
}
//MARK: - Table View
extension RaiseAClaimViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == vehicleAffectedTableView{
            return self.vehicleAffectedArr.count
        }else{
            return self.whoWasDrivingArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == vehicleAffectedTableView{
            let cell: VehicleAffectedTableViewCell = vehicleAffectedTableView.dequeueReusableCell(withIdentifier: "vehicleAffectedCell") as! VehicleAffectedTableViewCell
            cell.raiseClaimCellDelegateProtocol = self
            cell.indexNo = indexPath.row
            cell.isTheftReasonSelected = self.isTheftReasonSelected
            cell.setUpCell(indexPath: indexPath, vehicleAffectedArr: vehicleAffectedArr)
            return cell
            
        }else{
            let cell: WhoWasDrivingTableViewCell = whoWasDrivingTableView.dequeueReusableCell(withIdentifier: "whoWasDrivingCell")
                as! WhoWasDrivingTableViewCell
            cell.setUpCell(indexPath: indexPath, whoWasDrivingModel: self.whoWasDrivingArr[indexPath.row])
            cell.raiseClaimCellDelegateProtocol = self
            cell.indexNo = indexPath.row
            return cell
        }
        
    }
    
    /*
     * Show Popover
     */
    func createPopoverVC(identifier: String){
        if(identifier == "AddDriverViewController"){
            let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "AddDriverViewController") as! AddDriverViewController
            VC1.delegate = self as! ScreenCloseProtocol
            let size = CGSize(width: VC1.view.frame.width, height: VC1.view.frame.height/0.5)
            let frame = CGRect(x: 0, y: 20, width: 0, height: VC1.view.frame.height - 20)
            presentPopOverVC(VC1: VC1, frame: frame, size: size)
        }
        else if(identifier == "ConfirmContactViewController"){
            let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "ConfirmContactViewController") as! ConfirmContactViewController
            VC1.delegate = self as! ScreenCloseProtocol
            VC1.navController = self.navigationController
            let size = CGSize(width: VC1.view.frame.width, height: VC1.view.frame.height/2)
            let frame =  CGRect(x: view.center.x, y: view.center.y, width: 0, height: 0)
            presentPopOverVC(VC1: VC1, frame: frame, size: size)
        }
    }
    
    func presentPopOverVC(VC1 : UIViewController?,frame : CGRect?, size : CGSize?){
        VC1!.modalPresentationStyle = .popover
        VC1!.preferredContentSize = size!
        VC1!.popoverPresentationController!.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        VC1!.popoverPresentationController!.delegate = self
        VC1!.popoverPresentationController!.sourceView = self.view
        VC1!.popoverPresentationController!.sourceRect = frame!
        AppUtils.createRoundCornerView(view: VC1!.view, cornerRadius: 0)
        backgorundShadowView = AppUtils.createBackgroundShadowView(X: 0, Y: 0, Width: Int(view.frame.width), Height: Int(self.view.bounds.size.height) - 20)!
        self.view.addSubview(backgorundShadowView!)
        self.present(VC1!, animated: true, completion: nil)
    }
}

/*
 * MARK: - Extension Of UIPopoverPresentationController
 */
extension RaiseAClaimViewController: ScreenCloseProtocol, UIPopoverPresentationControllerDelegate{
    func onScreenClose(isClosed: Bool, value: Any?) {
        self.backgorundShadowView?.removeFromSuperview()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { context in
            // self.popOverViewController?.dismiss(animated: true)
        })
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}

/*
 * CAPTURE FOR CLAIM IMAGE OR CHOOSE FROM GALLERY
 */
extension RaiseAClaimViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func openCameraToClickPhoto()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true) {
                self.updateMainViewHeightAndPhotoViewHeight()
            }
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGalleryToSelectPhoto()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true) {
            self.updateMainViewHeightAndPhotoViewHeight()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            selectedImageFromImagePicker = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            selectedImageFromImagePicker = originalImage
        }
        dismiss(animated: true)
        uploadPhotoCollectionView.reloadData()
    }
    
    func updateMainViewHeightAndPhotoViewHeight(){
        let uploadPhotoCollectionViewHeight = 120
        let uploadPlotoCollectionViewHeight = 120
        let submitClaimBtnHeight = 40
        self.uploadPhotoCollectionViewHeightConstraint.constant = CGFloat(uploadPlotoCollectionViewHeight)
        self.uploadPhotoViewHeightConstraint.constant = CGFloat(uploadPhotoCollectionViewHeight + uploadPhotoCollectionViewHeight)
        self.uploadPhotoCollectionView.isHidden = false
        self.uploadPhotoSubView.isHidden = false
        
        let height1 = self.collectionViewContainerHeightConstraint.constant + self.vehicleAffectedContainerViewHeightConstraint.constant + self.incidentTimeAndLocationMainViewHeightConstraint.constant
        let height2 = self.needAsistanceViewHeightConstraint.constant +
            self.whoWasDrivingContainerViewHeightConstraint.constant +
            self.passengerInfoContainerHeightConstraint.constant +
            self.uploadPhotoViewHeightConstraint.constant
        
        let extraTempHeight = (15 * 7) + 35 + submitClaimBtnHeight
        print("this is height:::::::::::: \(height1)")
        self.mainViewHeightConstraint.constant = height1 + height2 + CGFloat(extraTempHeight)
    }
    
}

//MARK: - Keyboard Methods
/*********************** CODE TO SCROLL VIEW ABOVE KEYBOARD WHEN IT OPENS ***********************/
extension RaiseAClaimViewController: UITextFieldDelegate{
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        print("self.scrollView initial: \(self.scrollView!)")
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
        print("self.scrollView on open: \(self.scrollView!)")
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        //        var info = notification.userInfo!
        //        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        //        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
        self.scrollView.contentInset = UIEdgeInsets.zero
        self.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = true
        print("self.scrollView on close: \(self.scrollView!)")
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
}

extension RaiseAClaimViewController: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == commentDescriptionTextView{
            self.commentDescriptionPlaceholderLabel.isHidden = true
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == commentDescriptionTextView{
            if textView.text == ""{
                self.commentDescriptionPlaceholderLabel.isHidden = false
            }else{
                self.commentDescriptionPlaceholderLabel.isHidden = true
            }
        }
    }
}
