//
//  NotificationPopoverViewController.swift
//  SafeAuto
//
//  Created by ashwini on 27/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class NotificationPopoverViewController: BaseVC {
    
    @IBOutlet weak var viewAllButton: UIButton!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var notificationTableview: UITableView!
    @IBOutlet weak var headerView: UIView!
    var delegate: ScreenCloseProtocol? = nil
    var navController: UINavigationController? = nil
    var notificationDataArray : [NotificationModel] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        registerTableview()
        setNotificationData()
    }
    
    override func viewWillLayoutSubviews() {

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    /*
     * Initialize all view before use
     */
    func initViews(){
        AppUtils.addFontAwsomToButton(button: settingButton, fontAwsomName: "cog", textSize: 25, textcolour: UIColor.lightGray)
        headerView.addBottomBorderWithColor(color: UIColor.lightGray, width: 0.5)
        viewAllButton.addBottomBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#007AFF"), width: 1)
    }
    
    /*
     * Register Tableview
     */
    func registerTableview(){
        notificationTableview.register(UINib(nibName: "NotificationPopoverCell", bundle: nil), forCellReuseIdentifier: "NotificationPopoverCell")
    }
    
    
    @IBAction func viewAllButoonClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            //Give call back to DashboardViewController after popup close, to remove background view from superview
            self.delegate!.onScreenClose(isClosed: true, value: nil)
        }
         let noficicationViewController = self.storyboard!.instantiateViewController(withIdentifier: "NoficicationListViewController") as! NoficicationListViewController
        self.navController!.pushViewController(noficicationViewController, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.delegate!.onScreenClose(isClosed: true, value: nil)
    }
    
    @IBAction func settingButtonClicked(_ sender: Any) {
        
    }
    
    func setNotificationData(){
        let notificationModel1 = NotificationModel(paymentDetail: "Operator Removed - Mark Rogger removed from policy.", date: "09/15/2016 - 10:00 AM", newNotification: true)
        notificationDataArray.append(notificationModel1)
        let notificationModel2 = NotificationModel(paymentDetail: "Payment Invoice has been generated.", date: "08/12/2016 - 02:00 PM", newNotification: true)
        notificationDataArray.append(notificationModel2)
        let notificationModel3 = NotificationModel(paymentDetail: "Operator Removed - Natasha Shaun removed from policy", date: "06/30/2016 - 10:00 AM", newNotification: true)
        notificationDataArray.append(notificationModel3)
        let notificationModel4 = NotificationModel(paymentDetail: "Operator Removed - Mark Rogger removed from policy.", date: "04/15/2016 - 10:00 AM", newNotification: false)
        notificationDataArray.append(notificationModel4)
        let notificationModel5 = NotificationModel(paymentDetail: "Payment Invoice has been generated.", date: "02/12/2016 - 02:00 PM", newNotification: false)
        notificationDataArray.append(notificationModel5)
    }
}


extension NotificationPopoverViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationPopoverCell", for: indexPath) as! NotificationPopoverCell
        cell.setData(notificationModel: notificationDataArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            //Give call back to DashboardViewController after popup close, to remove background view from superview
            self.delegate!.onScreenClose(isClosed: true, value: nil)
            
        }
    }
    
}
