//
//  SplashViewController.swift
//  SafeAuto
//
//  Created by ashwini on 21/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit
import LocalAuthentication

class SplashViewController: BaseVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let isLogin = AppPreference.getInstance().getBoolean(AppConstants.PREFERENCE_IS_LOGIN, defaultvalue: false)
        if(isLogin){
            if #available(iOS 8.0, *) {
            loginWithFingerPrint()
            }else{
                self.launchDashboardSreen()
            }
        }else{
            launchScreen()
        }
    }
    
    func launchDashboardSreen(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let hamburgerVC = storyBoard.instantiateViewController(withIdentifier: AppConstants.HAMBURGER_CONTAINER_VC) as! HamburgerContainerViewController
        self.present(hamburgerVC, animated: true, completion: nil)
    }
    
    func launchScreen(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0) {
            let storyBoard = UIStoryboard(name: "InitialScreen", bundle: nil)
            let VC1 = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(VC1, animated: true, completion: nil)
        }
    }
    
}


/*************** FINGERPRINT LOGIN CODE *******************/

extension SplashViewController{
    
    func loginWithFingerPrint(){
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: AppConstants.TOUCH_ID_ALERT_MESSAGE2) { (success, error) in
                DispatchQueue.main.async {
                    
                    if error != nil {
                        self.displayError()
                    } else {
                        self.launchDashboardSreen()
                    }
                }
            }
        } else {
            // Device cannot use biometric authentication
            if let err = error {
                self.displayError(err: err)
            }
        }
    }
    
    func displayError(){
        displayConfirmationAlert(title: AppConstants.TOUCH_ID_ALER_ERROR_TITLE, message: AppConstants.TOUCH_ID_ALERT_MESSAGE1, btn1: AppConstants.OK_BUTTON, btn2: nil, handler: reopenToucIDAlert)
    }
    
    func displayError(err:Error){
        displayConfirmationAlert(title: AppConstants.TOUCH_ID_ALER_ERROR_TITLE, message: err as! String, btn1: AppConstants.OK_BUTTON, btn2: nil, handler: reopenToucIDAlert)
    }
    
    func reopenToucIDAlert(action: UIAlertAction) {
        loginWithFingerPrint()
    }
}




