//
//  RegisterViewController.swift
//  SafeAuto
//
//  Created by ashwini on 21/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DLRadioButton

class RegisterViewController: BaseVC {
    
    //MARK: - Outlets
    @IBOutlet weak var emailText: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordText: SkyFloatingLabelTextField!
    @IBOutlet weak var policyNumberText: SkyFloatingLabelTextField!
    @IBOutlet weak var firstNameText: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameText: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNubmerText: SkyFloatingLabelTextField!
    @IBOutlet weak var vehicleNumberText: SkyFloatingLabelTextField!
    @IBOutlet weak var femaleBtn: DLRadioButton!
    @IBOutlet weak var maleBtn: DLRadioButton!
    @IBOutlet weak var scrollview: UIScrollView!
    
    //MARK: - Variables
    var registerViewModel: RegisterViewModel?
    var gender : String? = "Male"
    var activeField: UITextField?
    var previousActiveField: UITextField?
    var fbOrGmailData = [String:String]()
    var identifier: String? = nil
    
    
    //MARK: - Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerViewModel = RegisterViewModel(registerViewController: self)
        registerForKeyboardNotifications()
        self.registerTapGesture()
        initviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(identifier != nil && (identifier == AppConstants.FACEBOOK_LOGIN || identifier == AppConstants.GMAIL_LOGIN)){
            emailText.text = fbOrGmailData["email"]
            firstNameText.text = fbOrGmailData["name"]
        }
    }
    
    //MARK: - Setup Views
    func initviews(){
        maleBtn.isSelected = true //default select male btn
    }
    
    //MARK: - Action Methods
    @IBAction func radioButtonClicked(_ sender: DLRadioButton) {
        if(sender.tag == 1){
            gender = "Male"
            femaleBtn.isSelected = false
        }
        else if(sender.tag == 2){
            gender = "Female"
            maleBtn.isSelected = false
        }
    }
    
   
    
    @IBAction func registerBtnClicked(_ sender: Any) {
//        AppPreference.getInstance().setString(AppConstants.PREFERENCE_POLICY_NUMBER, value: "SF294AK887")
//        AppPreference.getInstance().setString(AppConstants.PREFERENCE_PASSWORD, value: "test")
//        var registerModel = RegisterModel()
//        registerModel.email = "test@gmail.com"
//        registerModel.password = "test"
//        registerModel.policyNumber = "SF294AK887"
//        registerModel.firstName = "Pratham"
//        registerModel.lastName = "Salvi"
//        registerModel.phoneNumber = "8181818182"
//        registerModel.vehicleNumber = "LJCPCBLCX11000499"
//        self.registerViewModel?.registerUser(registerModel: registerModel)
//        if(identifier != nil && (identifier == AppConstants.FACEBOOK_LOGIN || identifier == AppConstants.GMAIL_LOGIN)){
//            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//            let navController = storyBoard.instantiateViewController(withIdentifier: "HOME_VIEW_CONTROLLER") as! UINavigationController
//            self.present(navController, animated: true, completion: nil)
//        }else{
//            self.dismiss(animated: true, completion: nil)
//        }
        
        if(emailText.text!.isEmpty || !(AppUtils.isValidEmail(testStr: emailText.text!))){
            emailText.errorMessage = AppConstants.EMAIL_ERROR_MESSAGE
        }
        else if(passwordText.text!.isEmpty){
            passwordText.errorMessage = AppConstants.PASSWORD_ERROR_MESSAGE
        }
        else if(policyNumberText.text!.isEmpty){
            policyNumberText.errorMessage = AppConstants.POLICY_NUMBER_ERROR_MESSAGE
        }
        else if(firstNameText.text!.isEmpty){
            firstNameText.errorMessage = AppConstants.FIRST_NAME_ERROR_MESSAGE
        }
        else if(lastNameText.text!.isEmpty){
            lastNameText.errorMessage = AppConstants.LAST_NAME_ERROR_MESSAGE
        }
        else if(phoneNubmerText.text!.isEmpty){
            phoneNubmerText.errorMessage = AppConstants.PHONE_ERROR_MESSAGE
        }
        else if(vehicleNumberText.text!.isEmpty){
            vehicleNumberText.errorMessage = AppConstants.VEHICLE_NUMBER_ERROR_MESSAGE
        }
        else{
            AppPreference.getInstance().setString(AppConstants.PREFERENCE_POLICY_NUMBER, value: self.policyNumberText.text!)
            AppPreference.getInstance().setString(AppConstants.PREFERENCE_PASSWORD, value: self.passwordText.text!)
            var registerModel = RegisterModel()
            registerModel.email = self.emailText.text!
            registerModel.password = self.passwordText.text!
            registerModel.policyNumber = self.policyNumberText.text!
            registerModel.firstName = self.firstNameText.text!
            registerModel.lastName = self.lastNameText.text!
            registerModel.phoneNumber = self.phoneNubmerText.text!
            registerModel.vehicleNumber = self.vehicleNumberText.text!
            self.registerViewModel?.registerUser(registerModel: registerModel)
            if(identifier != nil && (identifier == AppConstants.FACEBOOK_LOGIN || identifier == AppConstants.GMAIL_LOGIN)){
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let hamburgerVC = storyBoard.instantiateViewController(withIdentifier: AppConstants.HAMBURGER_CONTAINER_VC) as! HamburgerContainerViewController
                self.present(hamburgerVC, animated: true, completion: nil)
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func alreadyRegisterBtnClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        deregisterFromKeyboardNotifications()
    }
}


/*********************** CODE TO SCROLL VIEW ABOVE KEYBOARD WHEN IT OPENS ***********************/
extension RegisterViewController: UITextFieldDelegate{
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollview.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollview.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollview.isScrollEnabled = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if let floatingLabelTextField = previousActiveField as? SkyFloatingLabelTextField {
            if(floatingLabelTextField.errorMessage != ""){
                floatingLabelTextField.errorMessage = ""
            }
        }
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        previousActiveField = activeField
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailText:
            passwordText.becomeFirstResponder()
        case passwordText:
            policyNumberText.becomeFirstResponder()
        case policyNumberText:
            firstNameText.becomeFirstResponder()
        case firstNameText:
            lastNameText.becomeFirstResponder()
        case lastNameText:
            phoneNubmerText.becomeFirstResponder()
        case phoneNubmerText:
            vehicleNumberText.becomeFirstResponder()
        default:
            vehicleNumberText.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let floatingLabelTextField = textField as? SkyFloatingLabelTextField {
            if(floatingLabelTextField.errorMessage != nil || floatingLabelTextField.errorMessage != "" || !(floatingLabelTextField.errorMessage!.isEmpty)){
                if let error = floatingLabelTextField.errorMessage{
                    print("Error in register: \(error)")
                    floatingLabelTextField.errorMessage = ""
                }
            }
        }
        return true
    }
    
}
