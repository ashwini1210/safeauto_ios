//
//  LoginViewController.swift
//  SafeAuto
//
//  Created by ashwini on 21/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import FacebookLogin
import FBSDKLoginKit
import GoogleSignIn

class LoginViewController: BaseVC {
    
    //MARK: - Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var policyNumberText: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordText: SkyFloatingLabelTextField!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    //MARK: - Variables
    var activeField: UITextField?
    
    //MARK: - Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        registerForKeyboardNotifications()
        initializeGoogleSignIn()
        self.registerTapGesture()
    }
    
    
    func initViews(){
        loader.stopAnimating()
        loader.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let policyNumber: String? = AppPreference.getInstance().getString(AppConstants.PREFERENCE_POLICY_NUMBER, defaultvalue: "")
        let password: String? = AppPreference.getInstance().getString(AppConstants.PREFERENCE_PASSWORD, defaultvalue: "")

        if(policyNumber != "" && password != ""){
            policyNumberText.text = policyNumber
            passwordText.text = password
        }
        
    }
    
    //MARK: - Action Methods
    @IBAction func loginButtonClicked(_ sender: UIButton) {
        if(policyNumberText.text!.isEmpty){
            policyNumberText.errorMessage = AppConstants.POLICY_ERROR_MESSAGE
        }
        else if(passwordText.text!.isEmpty){
            passwordText.errorMessage = AppConstants.PASSWORD_ERROR_MESSAGE
        }else{
            let registerRealm = RegisterRealmManager().findByProperty(property: "policyNumber", value: policyNumberText.text!)
            for regRealm in registerRealm{
                if(policyNumberText.text == regRealm.policyNumber){
                    AppPreference.getInstance().setBoolean(AppConstants.PREFERENCE_IS_LOGIN, value: true)
                    launchDashboardScreen()
                }
            }
            if(registerRealm.count == 0){
                //show error = "Policy Number does not exist"
                self.displayAlert(title: AppConstants.ALERT_TITLE, message: AppConstants.POLICY_NUMBER_NOT_EXIST_ERROR, btn1: AppConstants.OK_BUTTON, btn2: nil)
            }
            
        }
    }
    
    func launchDashboardScreen(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let hamburgerVC = storyBoard.instantiateViewController(withIdentifier: AppConstants.HAMBURGER_CONTAINER_VC) as! HamburgerContainerViewController
        self.present(hamburgerVC, animated: true, completion: nil)
    }
    
    func launchRegsterScreen(data:[String:String]?, identifier:String){
        let storyBoard = UIStoryboard(name: "InitialScreen", bundle: nil)
        let VC1 = storyBoard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        if(data != nil){
        VC1.fbOrGmailData = data!
        }
        VC1.identifier = identifier
        self.present(VC1, animated: true, completion: nil)
    }
    
    @IBAction func openRegisterScreen(_ sender: UIButton) {
        launchRegsterScreen(data: nil, identifier: AppConstants.NORMAL_LOGIN)
    }
    

    
    deinit {
        deregisterFromKeyboardNotifications()
    }
}

/*************** FACEBOOK LOGIN CODE *******************/
extension LoginViewController{
    @IBAction func facebookButtonClicked(_ sender: UIButton) {
        loader.isHidden = false
        loader.startAnimating()
        let loginManager: LoginManager = LoginManager()
        if AccessToken.current != nil {
            loginManager.logOut()
        }
        
        loginManager.logIn(permissions: ["email","public_profile"], from: self, handler: { (loginResults: LoginManagerLoginResult?, error: Error?) -> Void in
            if !(loginResults?.isCancelled)! {
                self.getFBUserData()
            }
            else {    // Sign in request cancelled
                let err = NSError()
                print("FB error: \(err)")
                self.displayAlert(title: AppConstants.ALERT_TITLE, message: AppConstants.TRY_AGAIN_ERROR , btn1: AppConstants.OK_BUTTON, btn2: nil)
                self.loader.stopAnimating()
            }
        })
        
    }
    
    //function is fetching the user data
    func getFBUserData(){
        loader.stopAnimating()
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id,name,email,gender,picture.type(large),age_range,locale,link,cover,timezone"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result as! [String : AnyObject])
                }
                
                // Details received successfully
                let dictionary = result as! [String: AnyObject]
                var data = [String:String]()
                data["email"] = dictionary["email"] as? String
                data["facebook_id"] = dictionary["id"] as? String
                data["name"] = dictionary["name"] as? String
                data["gender"] = dictionary[""] as? String
                data["home_city"] = dictionary[""] as? String
                self.launchRegsterScreen(data: data, identifier: AppConstants.FACEBOOK_LOGIN)
                //self.launchDashboardScreen()
                
            })
        }
    }
}


/*************** GMAIL LOGIN CODE *******************/
extension LoginViewController: GIDSignInDelegate, GIDSignInUIDelegate{
    
    @IBAction func googleButtonClicked(_ sender: UIButton) {
        if GIDSignIn.sharedInstance().hasAuthInKeychain() {
            GIDSignIn.sharedInstance().signOut()
        }
        GIDSignIn.sharedInstance().signIn()
    }
    
    func initializeGoogleSignIn(){
        // Initialize sign-in
        GIDSignIn.sharedInstance().clientID = "54902133674-rnmltmj1hma3uuiiidbtn80k4rqk4ecv.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        loader.isHidden = false
        loader.startAnimating()
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,withError error: Error!) {
        loader.stopAnimating()
        if let error = error {
            print("\(error.localizedDescription)")
            self.displayAlert(title: AppConstants.ALERT_TITLE, message: AppConstants.TRY_AGAIN_ERROR , btn1: AppConstants.OK_BUTTON, btn2: nil)        } else {
            print("Google user: \(user!)")
            var data = [String:String]()
            data["email"] = user.profile.email
            data["id"] = user.userID
            data["name"] = user.profile.name
            data["gender"] = ""
            let pic =  user.profile.imageURL(withDimension: 200)!.absoluteString
            data["pic"] = pic
            self.launchRegsterScreen(data: data, identifier: AppConstants.GMAIL_LOGIN)

//            launchDashboardScreen()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        loader.stopAnimating()
    }
}

/*********************** CODE TO SCROLL VIEW ABOVE KEYBOARD WHEN IT OPENS ***********************/
extension LoginViewController: UITextFieldDelegate{
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollview.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollview.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
        self.scrollview.contentInset = contentInsets
        self.scrollview.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollview.isScrollEnabled = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
}


