//
//  ClaimViewController.swift
//  SafeAuto
//
//  Created by ashwini on 25/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class ClaimViewController: BaseVC {
    
    @IBOutlet weak var claimDateLabel: UILabel!
    @IBOutlet weak var dateOfLossLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var detailofLossView: UIView!
    @IBOutlet weak var detailOfLossLabel: UILabel!
    @IBOutlet weak var detailOfLossTextview: UITextView!
    @IBOutlet weak var photographLabel: UILabel!
    @IBOutlet weak var photographCollectionview: UICollectionView!
    @IBOutlet weak var adjusterCommentLabel: UILabel!
    @IBOutlet weak var adjusterTextview: UITextView!
    let claimPhotoCellIdentifier = "PhotoCellIdentifier"
    var claimViewModel : ClaimViewModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addDrawerButton()
    }
    
    func initViews(){
        claimViewModel = ClaimViewModel(claimViewController: self)
        AppUtils.createRoundCornerView(view: detailofLossView, cornerRadius: 5)
        registerCollectioView()
    }
    
    func registerCollectioView(){
        photographCollectionview.register(UINib(nibName: "ClaimPhotoCollectionViewCell", bundle: .main), forCellWithReuseIdentifier: claimPhotoCellIdentifier)
    }
    
    
    /*
     * Method to add Buttons on Navigation Bar
     */
    func addDrawerButton(){
        guard claimViewModel != nil else {
            return
        }
        self.navigationItem.leftBarButtonItems = claimViewModel!.addLeftBarButtonItem()
        self.navigationItem.rightBarButtonItems = claimViewModel!.addRightBarButtonItem()
        
        guard let titleView = claimViewModel!.addTextInTitleBar() else{
            return
        }
        self.navigationItem.titleView = titleView
    }
    
    override func fontAwsomButtonClicked(sender: Any) {
        let button = sender as! UIButton
        if (button.tag == AppConstants.BACK_BUTTON){         
                self.dismiss(animated: true, completion: nil)
        }
        else if(button.tag == AppConstants.NAVIGATION_NOTIFICATION_BUTTON){
            
        }
        else if(button.tag == AppConstants.NAVIGATION_ADDRESS_CARD_BUTTON){
            
        }
    }
}


extension ClaimViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: claimPhotoCellIdentifier, for: indexPath) as! ClaimPhotoCollectionViewCell
        cell.setImageOfClaimVeicle(indexPath: indexPath, identifier: AppConstants.VEHICLE_IMAGE_OF_CLAIM_DETAIL_SCR, image: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.photographCollectionview.bounds.width/3, height: self.photographCollectionview.bounds.height)
    }
    
}
