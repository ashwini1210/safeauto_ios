//
//  PolicyViewController.swift
//  PractiseMajesco
//
//  Created by ems on 20/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class PolicyViewController: BaseVC {
    
    //MARK: - Outlets
    @IBOutlet weak var upcomingPaymentTitleLabel: UILabel!
    
    @IBOutlet weak var upcomingPaymentLabel: UILabel!
    
    @IBOutlet weak var paynowButton: UIButton!
    
    @IBOutlet weak var scheduleOnLabel: UILabel!
    
    @IBOutlet weak var policyModulesTabSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var policyModulesContainerView: UIView!
    
    @IBOutlet weak var raiseClaimBtnContainerView: UIView!
    
    @IBOutlet weak var policyContentContainerView: UIView!
    
    @IBOutlet weak var policyPeriodLabel: UILabel!
    
    var policyViewModel : PolicyViewModel? = nil
    let dropDown = MakeDropDown()
    var dropDownRowHeight: CGFloat = 40
    var policyNumber: String = ""
    //MARK: - Variables
    lazy var vehiclesListVC: UIViewController? = {
        let vlVC = self.storyboard?.instantiateViewController(withIdentifier: "vehicleListVC")
        return vlVC
    }()
    
    lazy var driversListVC: UIViewController? = {
        let vlVC = self.storyboard?.instantiateViewController(withIdentifier: "driversListVC")
        return vlVC
    }()
    
    lazy var coveragesListVC: UIViewController? = {
        let vlVC = self.storyboard?.instantiateViewController(withIdentifier: "coveragesListVC")
        return vlVC
    }()
    
    lazy var claimsListVC: UIViewController? = {
        let vlVC = self.storyboard?.instantiateViewController(withIdentifier: "claimsListVC")
        return vlVC
    }()
    
    weak var currentViewController: UIViewController?
    // Policy Period TableView
    var policyPeriodTableView: UITableView!
    var policyPeriodTableContainerView: UIView!
    var policyPeriodDateArr: [String] = ["01/01/2018 - 01/12/2018", "01/01/2017 - 01/12/2017"]
    @IBOutlet weak var policyDetailDateContainerView: UIView!
    
    //MARK: - Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        setUpGestures()
        postNotifications()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.policyModulesTabSegmentedControl.addUnderlineForSelectedSegment()
    }
    
    //MARK: - Post Notifications
    
    func postNotifications(){
        let policyNumber: [String: String] = ["policyNumber": self.policyNumber]
        
        // post a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppConstants.NOTIFICATION_POLICY_NUMBER), object: nil, userInfo: policyNumber)
    }
    
    //MARK: - Setup UI
    
    func setUpUI(){
        setUpUILogic()
        displayCurrentTab(0)
        setUpSegmentedController()
        
        setUpDropDown()
        //     NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 20)!
        
        self.view.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
        self.paynowButton.makeRoundCorners(radius: self.paynowButton.bounds.height/2)
        
        self.policyContentContainerView.addBorderAndShadow()
        
        policyViewModel = PolicyViewModel.init(policyViewController: self)
        addDrawerButton()
    }
    
    func setUpDropDown(){
        dropDown.makeDropDownIdentifier = "DROP_DOWN_NEW"
        dropDown.cellReusableIdentifier = "policyDetailsCell"
        dropDown.makeDropDownDataSourceProtocol = self
        let testFrame = self.policyContentContainerView.convert(policyDetailDateContainerView.bounds, from: self.policyDetailDateContainerView)
        dropDown.setUpDropDown(viewPositionReference: (testFrame), offset: 0)
        dropDown.nib = UINib(nibName: "PolicyPeriodTableViewCell", bundle: nil)
        dropDown.setRowHeight(height: self.dropDownRowHeight)
        dropDown.width = self.policyDetailDateContainerView.bounds.width
        dropDown.setNeedsDisplay()
        self.policyContentContainerView.addSubview(dropDown)
    }
    
    /*
     * Method to add Buttons on Navigation Bar
     */
    func addDrawerButton(){
        guard policyViewModel != nil else {
            return
        }
        self.navigationItem.leftBarButtonItems = policyViewModel!.addLeftBarButtonItem()
        self.navigationItem.rightBarButtonItems = policyViewModel!.addRightBarButtonItem()
        
        guard let titleView = policyViewModel!.addTextInTitleBar() else{
            return
        }
        
        self.navigationItem.titleView = titleView
    }
    
    override func fontAwsomButtonClicked(sender: Any) {
        let button = sender as! UIButton
        if (button.tag == AppConstants.BACK_BUTTON){
            let previousScreen = AppPreference.getInstance().getString(AppConstants.PREVIOUS_SCREEN, defaultvalue: "")
            if(previousScreen == AppConstants.SUBMIT_CLAIM_VC){
                //Launch DashboardVC if we previous launching screen is SubitClaimViewController. Else just dismiss current screen.
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let navController = storyBoard.instantiateViewController(withIdentifier: "HOME_VIEW_CONTROLLER") as! UINavigationController
                self.present(navController, animated: true, completion: nil)
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        else if(button.tag == AppConstants.NAVIGATION_NOTIFICATION_BUTTON){
            
        }
        else if(button.tag == AppConstants.NAVIGATION_ADDRESS_CARD_BUTTON){
            
        }
    }
    
    
    func setUpSegmentedController(){
        
        self.policyModulesTabSegmentedControl.setTitleTextAttributes([ NSAttributedString.Key.foregroundColor : UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)], for: .selected)
        
        //        self.policyModulesTabSegmentedControl.addUnderlineForSelectedSegment()
        //        self.policyModulesTabSegmentedControl.setNeedsLayout()
        self.policyModulesTabSegmentedControl.removeAllSegments()
        
        
        self.policyModulesTabSegmentedControl.insertSegment(withTitle: "Vehicles", at: 0, animated: true)
        self.policyModulesTabSegmentedControl.insertSegment(withTitle: "Drivers", at: 1, animated: true)
        self.policyModulesTabSegmentedControl.insertSegment(withTitle: "Coverages", at: 2, animated:  true)
        self.policyModulesTabSegmentedControl.insertSegment(withTitle: "Claims", at: 3, animated:     true)
        self.policyModulesTabSegmentedControl.selectedSegmentIndex = 0
        
    }
    
    func setUpUILogic(){
        self.raiseClaimBtnContainerView.isHidden = true
        self.policyDetailDateContainerView.addBorders(borderWidth: 0.5, borderColor: UIColor.gray.cgColor)
        self.policyDetailDateContainerView.roundCorners()
        self.policyDetailDateContainerView.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_GRAY_COLOR)
    }
    
    //MARK: - Segmented Control
    func viewControllerForSelectedSegmentedIndex(_ index: Int) -> UIViewController? {
        //        var vc: UIViewController?
        switch index {
        case 0 :
            let vc = vehiclesListVC
            self.raiseClaimBtnContainerView.isHidden = true
            return vc
        case 1 :
            let vc = driversListVC
            self.raiseClaimBtnContainerView.isHidden = true
            return vc
        case 2 :
            let vc = coveragesListVC as! CoveragesViewController
            vc.policyNumber = self.policyNumber
            self.raiseClaimBtnContainerView.isHidden = true
            return vc
        case 3 :
            let vc = claimsListVC
            self.raiseClaimBtnContainerView.isHidden = false
            return vc
        default:
            return nil
        }
        
        //        return vc
    }
    
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentedIndex(tabIndex) {
            
            self.addChild(vc)
            vc.didMove(toParent: self)
            
            vc.view.frame = self.policyModulesContainerView.bounds
            self.policyModulesContainerView.addSubview(vc.view)
            self.currentViewController = vc
        }
    }
    
    //MARK: - Setup Gestures
    func setUpGestures(){
        self.policyPeriodLabel.isUserInteractionEnabled = true
        let policyPeriodLabelTapGesture = UITapGestureRecognizer(target: self, action: #selector(policyPeriodLabelTapped))
        self.policyPeriodLabel.addGestureRecognizer(policyPeriodLabelTapGesture)
    }
    
    //MARK: - Action Methods
    
    @IBAction func raiseAClaimBtnAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "RaiseClaim", bundle: nil)
        let navController = storyBoard.instantiateViewController(withIdentifier: "RaiseClaimNavController") as! UINavigationController
        self.present(navController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func policyModulesTabSegmentedControllerAction(_ sender: UISegmentedControl) {
        self.policyModulesTabSegmentedControl.changeUnderlinePosition()
        self.currentViewController!.view.removeFromSuperview()
        self.currentViewController!.removeFromParent()
        
        displayCurrentTab(sender.selectedSegmentIndex)
    }
    
    
    
    //MARK: - Selector Methods
    @objc func policyPeriodLabelTapped(sender: UITapGestureRecognizer){
        self.dropDown.showDropDown(height: self.dropDownRowHeight * 2)
        //        if UIView().viewWithTag(AppConstants.MAKE_TABLEVIEW_TAG) != nil{
        //            UIView.animate(withDuration: 0.5) {
        //                self.policyContentContainerView.isHidden = false
        //                self.policyPeriodTableContainerView.isHidden = true
        //            }
        //        }else{
        //            let testFrame = self.policyContentContainerView.convert(policyDetailDateContainerView.bounds, from: self.policyDetailDateContainerView)
        //
        ////            self.policyPeriodTableContainerView = UIView().createTableView(senderView: (self.policyDetailDateContainerView.frame))
        //            self.policyPeriodTableContainerView = UIView().createTableView(senderView: (testFrame))
        //            if let tableView = policyPeriodTableContainerView.viewWithTag(AppConstants.MAKE_TABLEVIEW_TAG){
        //                self.policyPeriodTableView = tableView as? UITableView
        //                self.policyPeriodTableView.register(UINib(nibName: "PolicyPeriodTableViewCell", bundle: nil), forCellReuseIdentifier: "policyDetailsCell")
        //                self.policyPeriodTableView.delegate = self
        //                self.policyPeriodTableView.dataSource = self
        //                self.policyPeriodTableView.rowHeight = 40
        //                self.policyPeriodTableView.estimatedRowHeight = 40
        //                self.policyPeriodTableContainerView.addBorders(borderWidth: 0.5, borderColor: UIColor.gray.cgColor)
        //                self.policyPeriodTableContainerView.roundCorners()
        //                self.policyPeriodTableView.reloadData()
        //                self.policyPeriodTableContainerView.frame = CGRect(x: testFrame.minX, y: 38, width: 0, height: 0)
        //                self.policyContentContainerView.addSubview(self.policyPeriodTableContainerView)
        //
        //                self.policyPeriodTableContainerView.frame.size = CGSize(width: (self.policyDetailDateContainerView.frame.width), height: 0)
        //                self.policyPeriodTableView.frame.size = CGSize(width: (self.policyDetailDateContainerView.frame.width), height: 0)
        //
        //                UIView.animate(withDuration: 0.5) {
        //                    self.policyPeriodTableContainerView.frame.size = CGSize(width: (self.policyDetailDateContainerView.frame.width), height: 80)
        //                    self.policyPeriodTableView.frame.size = CGSize(width: (self.policyDetailDateContainerView.frame.width), height: 80)
        //                    self.policyPeriodTableContainerView.layoutIfNeeded()
        //                }
        //            }
        //        }
        
        
    }
    
}

extension PolicyViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.policyPeriodDateArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PolicyPeriodTableViewCell = self.policyPeriodTableView.dequeueReusableCell(withIdentifier: "policyDetailsCell") as! PolicyPeriodTableViewCell
        cell.policyPeriodLabel.text = self.policyPeriodDateArr[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.policyPeriodLabel.text = self.policyPeriodDateArr[indexPath.row]
        
        UIView.animate(withDuration: 0.5) {
            self.policyPeriodTableContainerView.isHidden = true
        }
        
    }
    
}
//MARK: - Drop Down
extension PolicyViewController: MakeDropDownDataSourceProtocol{
    func getDataToDropDown(cell: UITableViewCell, indexPos: Int, makeDropDownIdentifier: String) {
        if makeDropDownIdentifier == "DROP_DOWN_NEW"{
            let customCell = cell as! PolicyPeriodTableViewCell
            customCell.policyPeriodLabel.text = self.policyPeriodDateArr[indexPos]
        }
    }
    
    func numberOfRows(makeDropDownIdentifier: String) -> Int {
        return self.policyPeriodDateArr.count
    }
    
    func selectItemInDropDown(indexPos: Int, makeDropDownIdentifier: String) {
        self.policyPeriodLabel.text = self.policyPeriodDateArr[indexPos]
        self.dropDown.hideDropDown()
    }
    
}
