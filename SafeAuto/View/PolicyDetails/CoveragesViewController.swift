//
//  CoveragesViewController.swift
//  PractiseMajesco
//
//  Created by ems on 20/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class CoveragesViewController: BaseVC {
    //MARK: - Outlets
    @IBOutlet weak var coverageTableView: UITableView!
    
    @IBOutlet weak var nonCoverageCollectionView: UICollectionView!
    
    //MARK: - Constraints
    
    @IBOutlet weak var mainViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var coverageTableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var nonCoverageTableViewHeightConstraint: UICollectionView!
    
    
    //MARK: - Variables
    var liabilityTypeArr = [LiabilityCoverageModel]()
    var vehiclesCoverageArr = [VehicleCoverageModel]()
    var coverageDetailArr = [CoverageDetailModel]()
    var nonCoverageArr = [NonCoverageModel]()
    var coveragesViewModel : CoveragesViewModel? = nil
    var vehicleListArray: [VehicleModel] = [VehicleModel]()
    var policyNumber: String = ""
    
    //MARK: - Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        if(coveragesViewModel != nil){
            coveragesViewModel!.tempData()
        }
        updateHeight()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func setUpUI(){
        coveragesViewModel = CoveragesViewModel.init(coveragesViewController: self)
        setUpTableView()
        setUpCollectionView()
    }
    
    func setUpTableView(){
        let nib1 = UINib(nibName: "CoverageTableViewCell", bundle: nil)
        self.coverageTableView.register(nib1, forCellReuseIdentifier: "coverageCell")
        
        let nib2 = UINib(nibName: "VehicleCoverageTableViewCell", bundle: nil)
        self.coverageTableView.register(nib2, forCellReuseIdentifier: "vehicleCoverageCell")
        
        self.coverageTableView.rowHeight = 50
        self.coverageTableView.estimatedRowHeight = 50
        
        self.coverageTableView.addBorderAndShadow()
        
    }
    
    func setUpCollectionView(){
        let nib = UINib(nibName: "NonCoveragesCollectionViewCell", bundle: nil)
        self.nonCoverageCollectionView.register(nib, forCellWithReuseIdentifier: "nonCoverageCell")
        
        self.nonCoverageCollectionView.addBorders(borderWidth: 0.5, borderColor: UIColor.lightGray.cgColor)
    }
    
    
    
    func updateHeight(){
        let fixtableHeight = 6 * 50
        let dynamicTableHeight = (self.coverageDetailArr.count * 5) * 50
        let collectionViewHeight = 200
        let otherHeight = 50
        
        self.coverageTableViewHeightConstraint.constant = CGFloat(fixtableHeight + dynamicTableHeight)
        
        self.mainViewHeightConstraint.constant = CGFloat(fixtableHeight + dynamicTableHeight + collectionViewHeight + otherHeight)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension CoveragesViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.coverageDetailArr.count + 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            let firstSectionTitleView: CoverageTableViewCell = coverageTableView.dequeueReusableCell(withIdentifier: "coverageCell") as! CoverageTableViewCell
            firstSectionTitleView.setDateForFirstSection()
            return firstSectionTitleView
        }
//        else if section == 1{
//            let secondSectionTitleView: VehicleCoverageTableViewCell = coverageTableView.dequeueReusableCell(withIdentifier: "vehicleCoverageCell") as! VehicleCoverageTableViewCell
//            secondSectionTitleView.setUIOfSection(vehicleName: "")
//            return secondSectionTitleView
//        }
        else{
            let thirdSectionTitleView: VehicleCoverageTableViewCell = coverageTableView.dequeueReusableCell(withIdentifier: "vehicleCoverageCell") as! VehicleCoverageTableViewCell
            thirdSectionTitleView.setUIOfSection(vehicleName: self.coverageDetailArr[section - 1].vehicleName)
            return thirdSectionTitleView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 5
        } else {
            return 4
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            let cell: CoverageTableViewCell = coverageTableView.dequeueReusableCell(withIdentifier: "coverageCell") as! CoverageTableViewCell
            cell.setDateOfCoverageCell(indexPath: indexPath, liabilityTypeArr: liabilityTypeArr)
            return cell
        }
//        else if indexPath.section == 1{
//            let cell: VehicleCoverageTableViewCell = coverageTableView.dequeueReusableCell(withIdentifier: "vehicleCoverageCell") as! VehicleCoverageTableViewCell
//            cell.setDataForVehicleCell1(indexPath: indexPath, vehiclesCoverageArr: vehiclesCoverageArr)
//            return cell
//        }
        else{
            let cell: VehicleCoverageTableViewCell = coverageTableView.dequeueReusableCell(withIdentifier: "vehicleCoverageCell") as! VehicleCoverageTableViewCell
            cell.setDataForVehicleCell2(indexPath: indexPath, vehiclesCoverageArr: self.coverageDetailArr[indexPath.section - 1].vehicleCoverageArr)
            return cell
        }
        
        
    }
    
}

extension CoveragesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: NonCoveragesCollectionViewCell = nonCoverageCollectionView.dequeueReusableCell(withReuseIdentifier: "nonCoverageCell", for: indexPath) as! NonCoveragesCollectionViewCell
        cell.setUIOfCell(indexPath: indexPath, nonCoverageArr: self.nonCoverageArr)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: self.nonCoverageCollectionView.bounds.width / 3.2, height: self.nonCoverageCollectionView.bounds.height)
        
        return size
        
    }
    
}

