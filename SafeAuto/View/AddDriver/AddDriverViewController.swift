//
//  AddDriverViewController.swift
//  SafeAuto
//
//  Created by ashwini on 02/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class AddDriverViewController: BaseVC {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleSegmentController: UISegmentedControl!
    @IBOutlet weak var suffixSegmentController: UISegmentedControl!
    @IBOutlet weak var genderSegmentController: UISegmentedControl!
    @IBOutlet weak var acciedentSegmentController: UISegmentedControl!
    @IBOutlet weak var noOfvoilenceSegmentController: UISegmentedControl!
    @IBOutlet weak var noOfAccidentSegmentController: UISegmentedControl!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var DOBTextField: UITextField!
    @IBOutlet weak var licenceTextField: UITextField!
    @IBOutlet weak var accidentView: UIView!
    @IBOutlet weak var informationView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    var addDriverViewModel : AddDriverViewModel? = nil
    var delegate: ScreenCloseProtocol? = nil
    var activeField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        setScrollViewSize()
        registerForKeyboardNotifications()
        self.registerTapGesture()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppUtils.createRoundCornerView(view: self.view, cornerRadius: 0)
        view.clipsToBounds = false
    }
    
    func initViews(){
        addDriverViewModel = AddDriverViewModel(addDriverViewController: self)
        if(addDriverViewModel != nil){
            addDriverViewModel!.changeBorderOfSegmentView()
        }
    }
    
    func setScrollViewSize(){
        //initially set scrollview size till informationView
        accidentView.isHidden = true
        mainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.informationView.frame.size.height)
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.mainView.frame.size.height)
    }
    
    
    
    @IBAction func titleSegmentClicked(_ sender: UISegmentedControl) {
        selectedSegmentedIndex(index: sender.selectedSegmentIndex, segmentIdentifier: AppConstants.TITLE_SEGMENT_CLICKED)
    }
    
    @IBAction func suffixSegmentClicked(_ sender: UISegmentedControl) {
        selectedSegmentedIndex(index: sender.selectedSegmentIndex, segmentIdentifier: AppConstants.SUFFIX_SEGMENT_CLICKED)
    }
    
    @IBAction func genderSegmentClicked(_ sender: UISegmentedControl) {
        selectedSegmentedIndex(index: sender.selectedSegmentIndex, segmentIdentifier: AppConstants.GENDER_SEGMENT_CLICKED)
    }
    
    @IBAction func accidentSegmentClicked(_ sender: UISegmentedControl) {
        selectedSegmentedIndex(index: sender.selectedSegmentIndex, segmentIdentifier: AppConstants.ACCIDENT_SEGMENT_CLICKED)
    }
    
    @IBAction func noOfviolenceSegmentClicked(_ sender: UISegmentedControl) {
        selectedSegmentedIndex(index: sender.selectedSegmentIndex, segmentIdentifier: AppConstants.NO_OF_VIOLENCE_SEGMENT_CLICKED)
    }
    
    @IBAction func noOfaccidentSegmentClicked(_ sender: UISegmentedControl) {
        selectedSegmentedIndex(index: sender.selectedSegmentIndex, segmentIdentifier: AppConstants.NO_OF_ACCIDENT_SEGMENT_CLICKED)
    }
    
    @IBAction func addDriverButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
     * Method to get index of selected segment controller and do according functionality
     */
    func selectedSegmentedIndex(index: Int,segmentIdentifier: Int){
        if(segmentIdentifier == AppConstants.TITLE_SEGMENT_CLICKED){
            titleSegmentController.tintColor = UIColor().hexStringToUIColor(hex: "#337ab7")
            addDriverViewModel!.onTitleIndex(index: index)
        }
        else if(segmentIdentifier == AppConstants.SUFFIX_SEGMENT_CLICKED){
            suffixSegmentController.tintColor = UIColor().hexStringToUIColor(hex: "#337ab7")
            addDriverViewModel!.onSuffixIndex(index: index)
        }
        else if(segmentIdentifier == AppConstants.GENDER_SEGMENT_CLICKED){
            genderSegmentController.tintColor = UIColor().hexStringToUIColor(hex: "#337ab7")
            addDriverViewModel!.onGenderIndex(index: index)
        }
        else if(segmentIdentifier == AppConstants.ACCIDENT_SEGMENT_CLICKED){
            acciedentSegmentController.tintColor = UIColor().hexStringToUIColor(hex: "#337ab7")
            addDriverViewModel!.onAccidentIndex(index: index)
        }
        else if(segmentIdentifier == AppConstants.NO_OF_VIOLENCE_SEGMENT_CLICKED){
            noOfvoilenceSegmentController.tintColor = UIColor().hexStringToUIColor(hex: "#337ab7")
            addDriverViewModel!.onNoOfVoilenceIndex(index: index)
        }
        else if(segmentIdentifier == AppConstants.NO_OF_ACCIDENT_SEGMENT_CLICKED){
            noOfAccidentSegmentController.tintColor = UIColor().hexStringToUIColor(hex: "#337ab7")
            addDriverViewModel!.onNoOfAccidentIndex(index: index)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.delegate!.onScreenClose(isClosed: true, value: nil)
    }
    
    deinit {
        deregisterFromKeyboardNotifications()
    }
}


/*********************** CODE TO SCROLL VIEW ABOVE KEYBOARD WHEN IT OPENS ***********************/
extension AddDriverViewController: UITextFieldDelegate{
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
}
