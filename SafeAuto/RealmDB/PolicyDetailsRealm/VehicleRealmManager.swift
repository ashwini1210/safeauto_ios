//
//  VehicleRealmManager.swift
//  SafeAuto
//
//  Created by ems on 16/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
protocol VehicleRealmManagerProtocol{
    func saveVehicleList(vehicleArr: [VehicleModel])
    //    func getVehicleList() -> Results<VehicleRealm>
    func addVehicle(vehicleModel: VehicleModel)
    func getVehicleList(policyNumber: String) -> [VehicleModel]
    func getVehicleList() -> [VehicleModel]
    func removeVehicleList()
    
}
class VehicleRealmManager: VehicleRealmManagerProtocol{
    
    func saveVehicleList(vehicleArr: [VehicleModel]){
        
        let realm = try! Realm()
        try! realm.write {
            for vehicleObj in vehicleArr{
                let vehicleRealm = VehicleRealm()
                vehicleRealm.vehicleName = vehicleObj.vehicleName
                vehicleRealm.vehicleCode = vehicleObj.vehicleCode
                vehicleRealm.isVehicleAffected = vehicleObj.isVehicleAffected
                vehicleRealm.policyNumber = vehicleObj.policyNumber
                
                realm.add(vehicleRealm)
                
            }
        }
    }
    
    func addVehicle(vehicleModel: VehicleModel){
        let realm = try! Realm()
        
        try! realm.write {
            let vehicleRealm = VehicleRealm()
            vehicleRealm.vehicleName = vehicleModel.vehicleName
            vehicleRealm.vehicleCode = vehicleModel.vehicleCode
            vehicleRealm.policyNumber = vehicleModel.policyNumber
            vehicleRealm.isVehicleAffected = vehicleModel.isVehicleAffected
            
            realm.add(vehicleRealm)
        }
    }
    
    func getVehicleList() -> Results<VehicleRealm>{
        
        var vehicleListResult: Results<VehicleRealm>?
        let realm = try! Realm()
        vehicleListResult = realm.objects(VehicleRealm.self)
        return vehicleListResult!
    }
    
    func getVehicleList() -> [VehicleModel]{
        
        var vehicleListResult: Results<VehicleRealm>?
        var vehicleArr = [VehicleModel]()
        let realm = try! Realm()
        vehicleListResult = realm.objects(VehicleRealm.self)
        
        for vehicleObj in vehicleListResult!{
            let vehicleModel = VehicleModel(vehicleName: vehicleObj.vehicleName, vehicleCode: vehicleObj.vehicleCode, isVehicleAffected: vehicleObj.isVehicleAffected, policyNumber: vehicleObj.policyNumber)
            
            vehicleArr.append(vehicleModel)
        }
        
        return vehicleArr
    }
    
    func getVehicleList(policyNumber: String) -> [VehicleModel]{
        var vehicleListResult: Results<VehicleRealm>?
        //        var vehicleArr: [VehicleModel]? =  nil
        let realm = try! Realm()
        vehicleListResult = realm.objects(VehicleRealm.self).filter("policyNumber == %@",policyNumber)
        //          vehicleListResult = realm.objects(VehicleRealm.self).filter("isVehicleAffected == false")
        //          vehicleListResult = realm.objects(VehicleRealm.self)
        print("\(String(describing: vehicleListResult?.count))")
        
        
        return self.getVehicleModelArrFromRealmResults(vehicleListResults: vehicleListResult!)
        
    }
    
    func removeVehicleList(){
        
        let realm = try! Realm()
        let result = realm.objects(VehicleRealm.self)
        try! realm.write {
            realm.delete(result)
            
        }
    }
    
    func getVehicleModelArrFromRealmResults(vehicleListResults: Results<VehicleRealm>) -> [VehicleModel]{
        var vehicleArr = [VehicleModel]()
        
        print("\(vehicleListResults.count)")
        for vehicleObj in vehicleListResults{
            
            print(">>>>>>>>> \(vehicleObj.isVehicleAffected)")
            let vehicleModel = VehicleModel(vehicleName: vehicleObj.vehicleName, vehicleCode: vehicleObj.vehicleCode, isVehicleAffected: vehicleObj.isVehicleAffected, policyNumber: vehicleObj.policyNumber)
            
            vehicleArr.append(vehicleModel)
        }
        
        return vehicleArr
    }
}
