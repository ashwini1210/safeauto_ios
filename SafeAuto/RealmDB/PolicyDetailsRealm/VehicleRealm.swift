//
//  VehicleRealm.swift
//  SafeAuto
//
//  Created by ems on 16/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import RealmSwift
class VehicleRealm: Object{
    
    @objc dynamic var vehicleName: String = ""
    @objc dynamic var vehicleCode: String = ""
    @objc dynamic var isVehicleAffected: Bool = false
    @objc dynamic var policyNumber: String = ""
    
}

