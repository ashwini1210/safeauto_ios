//
//  ClaimRealm.swift
//  SafeAuto
//
//  Created by ems on 16/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import RealmSwift

class ClaimRealm: Object{
    
    @objc dynamic var claimName: String = ""
    @objc dynamic var claimDate: String = ""
    @objc dynamic var claimStatus: String = ""
    
}
