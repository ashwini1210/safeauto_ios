//
//  VehicleCoverageRealm.swift
//  SafeAuto
//
//  Created by Prathamesh Salvi on 29/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import RealmSwift

class VehicleCoverageRealm: Object{
    @objc dynamic var vehicleCode: String = ""
    @objc dynamic var coverageType: String = ""
    @objc dynamic var isCoverageProvided: Bool = false
}
