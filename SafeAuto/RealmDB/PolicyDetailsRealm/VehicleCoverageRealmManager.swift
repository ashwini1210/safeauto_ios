//
//  VehicleCoverageRealmManager.swift
//  SafeAuto
//
//  Created by Prathamesh Salvi on 29/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import RealmSwift
protocol VehicleCoverageRealmManagerProtocol {
    func saveVehicleCoverageList(vehicleCoverageList: [VehicleCoverageModel])
    func getVehicleCoverageList() -> [VehicleCoverageModel]
    func getVehicleCoverageList(vehicleCode: String) -> [VehicleCoverageModel]
    func removeVehicleCoverageList()
}

class VehicleCoverageRealmManager: VehicleCoverageRealmManagerProtocol{
    
    func saveVehicleCoverageList(vehicleCoverageList: [VehicleCoverageModel]){
        let realm = try! Realm()
        try! realm.write {
            for vehicleCovObj in vehicleCoverageList{
                let vehicleCovRealm = VehicleCoverageRealm()
                vehicleCovRealm.vehicleCode = vehicleCovObj.vehicleCode
                vehicleCovRealm.coverageType = vehicleCovObj.coverageType
                vehicleCovRealm.isCoverageProvided = vehicleCovObj.isCoverageProvided
                
                realm.add(vehicleCovRealm)
                
            }
        }
    }
    
    func getVehicleCoverageList() -> [VehicleCoverageModel]{
        
        var vehicleCovListResult: Results<VehicleCoverageRealm>?
        let realm = try! Realm()
        vehicleCovListResult = realm.objects(VehicleCoverageRealm.self)
        
        
        return getVehicleCoverageModelArrFromRealmResults(vehicleCovListResults: vehicleCovListResult!)
    }
    
    func getVehicleCoverageList(vehicleCode: String) -> [VehicleCoverageModel]{
        
        var vehicleCovListResult: Results<VehicleCoverageRealm>?
        let realm = try! Realm()
        vehicleCovListResult = realm.objects(VehicleCoverageRealm.self).filter("vehicleCode == %@", vehicleCode)
        
        
        return getVehicleCoverageModelArrFromRealmResults(vehicleCovListResults: vehicleCovListResult!)
    }
    
    func getVehicleCoverageModelArrFromRealmResults(vehicleCovListResults: Results<VehicleCoverageRealm>) -> [VehicleCoverageModel]{
        var vehicleArr = [VehicleCoverageModel]()
        
        for vehicleCovObj in vehicleCovListResults{
            let vehicleCovModel = VehicleCoverageModel(vehicleCode: vehicleCovObj.vehicleCode, coverageType: vehicleCovObj.coverageType, isCoverageProvided: vehicleCovObj.isCoverageProvided)
            
            vehicleArr.append(vehicleCovModel)
        }
        
        return vehicleArr
    }
    
    func removeVehicleCoverageList(){
        
        let realm = try! Realm()
        let result = realm.objects(VehicleCoverageRealm.self)
        try! realm.write {
            realm.delete(result)
            
        }
    }
    
}
