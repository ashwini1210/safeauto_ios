//
//  ClaimRealmManager.swift
//  SafeAuto
//
//  Created by ems on 16/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
protocol ClaimRealmManagerProtocol{
    func saveclaimList(claimListArr: [ClaimModel])
    func addClaim(claimModel: ClaimModel)
    func getClaimList() -> Results<ClaimRealm>
    func removeClaimList()
}
class ClaimRealmManager: ClaimRealmManagerProtocol{
    
    func saveclaimList(claimListArr: [ClaimModel]){
        let realm = try! Realm()
        try! realm.write {
            for claimObj in claimListArr{
                let claimRealm = ClaimRealm()
                claimRealm.claimName = claimObj.claimName
                claimRealm.claimDate = claimObj.claimDate
                claimRealm.claimStatus = claimObj.claimStatus
                
                realm.add(claimRealm)
                print("Activity leader board write successful")
            }
        }
        
    }
    
    func addClaim(claimModel: ClaimModel){
        let realm = try! Realm()
        try! realm.write {
            let claimRealm = ClaimRealm()
            claimRealm.claimName = claimModel.claimName
            claimRealm.claimDate = claimModel.claimDate
            claimRealm.claimStatus = claimModel.claimStatus
            
            realm.add(claimRealm)
        }
    }
    
    func getClaimList() -> Results<ClaimRealm>{
        var claimListRealm: Results<ClaimRealm>?
        let realm = try! Realm()
        claimListRealm = realm.objects(ClaimRealm.self)
        return claimListRealm!
    }
    
    func removeClaimList(){
        let realm = try! Realm()
        let result = realm.objects(ClaimRealm.self)
        try! realm.write {
            realm.delete(result)
        }
    }
}
