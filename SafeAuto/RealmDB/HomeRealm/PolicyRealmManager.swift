//
//  PolicyRealmManager.swift
//  SafeAuto
//
//  Created by Prathamesh Salvi on 29/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//


import Foundation
import RealmSwift
protocol PolicyRealmManagerProtocol{
    func savePolicyList(policyArr: [PolicyModel])
    func getPolicyList() -> [PolicyModel]
    func addPolicy(policyObj: PolicyModel)
    func removePolicyList()
    
}
class PolicyRealmManager: PolicyRealmManagerProtocol{
    func savePolicyList(policyArr: [PolicyModel]){
        
        let realm = try! Realm()
        try! realm.write {
            for policyObj in policyArr{
                let policyRealm = PolicyRealm()
                policyRealm.policyNumber = policyObj.policyNumber
                realm.add(policyRealm)
            }
        }
    }
    
    func addPolicy(policyObj: PolicyModel){
        let realm = try! Realm()
        
        try! realm.write {
            let policyRealm = PolicyRealm()
            policyRealm.policyNumber = policyObj.policyNumber
            realm.add(policyRealm)
        }
    }
    
    
    func getPolicyList() -> [PolicyModel]{
        
        var policyListResult: Results<PolicyRealm>?
        var policyListArr: [PolicyModel] = [PolicyModel]()
        let realm = try! Realm()
        policyListResult = realm.objects(PolicyRealm.self)
        
        for policyObj in (policyListResult)!{
            let policyModel = PolicyModel(policyNumber: policyObj.policyNumber)
            policyListArr.append(policyModel)
        }
        
        return policyListArr
    }
    
    func removePolicyList(){
        
        let realm = try! Realm()
        let result = realm.objects(PolicyRealm.self)
        try! realm.write {
            realm.delete(result)
            
        }
    }
    
}
