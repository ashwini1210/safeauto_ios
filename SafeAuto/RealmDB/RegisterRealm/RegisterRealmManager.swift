//
//  RegisterRealmManager.swift
//  SafeAuto
//
//  Created by ashwini on 22/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
protocol RegisterRealmManagerProtocol{
    var realm: Realm { get set }
    func saveClaim(registerModel: RegisterModel)
    func getRegisterModel() -> RegisterModel
    func findByProperty(property:String,  value: String) -> Results<RegisterRealm>
}


class RegisterRealmManager: RegisterRealmManagerProtocol {
 
    var realm = try! Realm()

    func saveClaim(registerModel: RegisterModel){
        try! realm.write {
            let registerRealm = RegisterRealm()
            registerRealm.email = registerModel.email
            registerRealm.password = registerModel.password
            registerRealm.policyNumber = registerModel.policyNumber
            registerRealm.firstName = registerModel.firstName
            registerRealm.lastName = registerModel.lastName
            registerRealm.phoneNumber = registerModel.phoneNumber
            registerRealm.vehicleNumber = registerModel.vehicleNumber
            registerRealm.gender = registerModel.gender
            realm.add(registerRealm)
        }
    }
    
    func findByProperty(property:String,  value: String) -> Results<RegisterRealm>
    {
        return realm.objects(RegisterRealm.self).filter("\(property)='\(value)'")
    }
    
    func getRegisterModel() -> RegisterModel{
        
        let results = realm.objects(RegisterRealm.self)
        let user = results[0]
        
        let registerModel = RegisterModel(email: user.email, password: user.password, policyNumber: user.policyNumber, firstName: user.firstName, lastName: user.lastName, phoneNumber: user.phoneNumber, vehicleNumber: user.vehicleNumber, gender: user.gender)
        return registerModel
        
        
    }
}
