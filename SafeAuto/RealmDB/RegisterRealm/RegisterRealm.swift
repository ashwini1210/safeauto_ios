//
//  RegisterRealm.swift
//  SafeAuto
//
//  Created by ashwini on 22/05/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import Foundation
import RealmSwift



class RegisterRealm: Object {
 
    @objc dynamic var email: String = ""
    @objc dynamic var password: String = ""
    @objc dynamic var policyNumber: String = ""
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var phoneNumber: String = ""
    @objc dynamic var vehicleNumber: String = ""
    @objc dynamic var gender: String = ""

}
