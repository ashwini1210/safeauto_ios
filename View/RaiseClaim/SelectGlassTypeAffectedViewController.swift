//
//  SelectGlassTypeAffectedViewController.swift
//  PractiseMajesco
//
//  Created by ems on 01/04/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class SelectGlassTypeAffectedViewController: BaseVC {

    @IBOutlet weak var glassTypePopUpContainerView: UIView!
    
    @IBOutlet weak var glassTypePopUpCollectionView: UICollectionView!
    
    @IBOutlet weak var doneBtn: UIButton!
    
    //MARK: - Constraints
    
    @IBOutlet weak var glassTypePopUpContainerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var glassTypePopUpCollectionViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Variables
    var glassTypeArr: [GlassTypeModel] = [GlassTypeModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        populateTempData()
        // Do any additional setup after loading the view.
    }
    
    func setUpUI(){
        self.doneBtn.backgroundColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
        
        
        setUpCollectionView()
        setUpPopUpHeight()
    }
    
    func setUpCollectionView(){
        let nib1 = UINib(nibName: "GlassTypePopUpCollectionViewCell", bundle: nil)
        self.glassTypePopUpCollectionView.register(nib1, forCellWithReuseIdentifier: "glassTypeCollectionCell")
        
        let nib2 = UINib(nibName: "GlassTypeBothWindowsCollectionViewCell", bundle: nil)
        self.glassTypePopUpCollectionView.register(nib2, forCellWithReuseIdentifier: "bothGlassTypeCollectionCell")
        
        
        self.glassTypePopUpCollectionView.delegate = self
        self.glassTypePopUpCollectionView.dataSource = self
    }
    
    func setUpPopUpHeight(){
        let collectionCellHeight = 130
        let collectonViewInsetsHeight = 20
        let collectionViewHeight = CGFloat(collectionCellHeight * 2) + CGFloat(collectonViewInsetsHeight)
        let doneButtonHeight = 40
        let topBottomConstraintHeight = 30
        
        self.glassTypePopUpCollectionViewHeightConstraint.constant = collectionViewHeight
        
        self.glassTypePopUpContainerViewHeightConstraint.constant = collectionViewHeight + CGFloat(doneButtonHeight) + CGFloat(topBottomConstraintHeight)
        
        
    }
    
    //MARK: - TempData
    func populateTempData(){
        let glassTypeModel1 = GlassTypeModel(glassTypeName: "Windshield", glassTypeFirstImageName: "carFrontGlassIcon", glassTypeSecondImageName: "carSideOrRearIcon", isGlassTypeSelected: false)
        let glassTypeModel2 = GlassTypeModel(glassTypeName: "Side or rear glass", glassTypeFirstImageName: "carFrontGlassIcon", glassTypeSecondImageName: "carSideOrRearIcon", isGlassTypeSelected: false)
        let glassTypeModel3 = GlassTypeModel(glassTypeName: "Both", glassTypeFirstImageName: "carFrontGlassIcon", glassTypeSecondImageName: "carSideOrRearIcon", isGlassTypeSelected: false)
        
        glassTypeArr.append(glassTypeModel1)
        glassTypeArr.append(glassTypeModel2)
        glassTypeArr.append(glassTypeModel3)
        
        self.glassTypePopUpCollectionView.reloadData()
    }
    
    
    //MARK: - Action Methods
    
    @IBAction func doneBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SelectGlassTypeAffectedViewController: SelectGlassTypeCellDelegateProtocol{
    func glassTypeSelected(indexNo: Int) {
        for i in 0..<self.glassTypeArr.count{
            if i == indexNo{
                self.glassTypeArr[i].isGlassTypeSelected = true
            }else{
                self.glassTypeArr[i].isGlassTypeSelected = false
            }
            
        }
        
        self.glassTypePopUpCollectionView.reloadData()
    }
    
    
}

extension SelectGlassTypeAffectedViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.glassTypeArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item == 0 || indexPath.item == 1{
        let cell: GlassTypePopUpCollectionViewCell = glassTypePopUpCollectionView.dequeueReusableCell(withReuseIdentifier: "glassTypeCollectionCell", for: indexPath) as! GlassTypePopUpCollectionViewCell
           //UI Setup
           cell.setUpGestures()
            
            //Assign Values to Cell
            cell.selectGlassTypeCellDelegateProtocol = self
            cell.indexNo = indexPath.item
            
            if indexPath.item == 0{
                
                cell.glassTypeImageView.image = UIImage(named: self.glassTypeArr[indexPath.row].glassTypeFirstImageName)
                cell.glassTypeLabel.text = self.glassTypeArr[indexPath.row].glassTypeName
            }else if indexPath.row == 1{
                cell.glassTypeImageView.image = UIImage(named: self.glassTypeArr[indexPath.row].glassTypeSecondImageName)
                cell.glassTypeLabel.text = self.glassTypeArr[indexPath.row].glassTypeName
            }
            
            //if is selected logic
            if self.glassTypeArr[indexPath.row].isGlassTypeSelected{
                cell.checkGlassTypeMainView.addBorders(borderWidth: 2.0, borderColor: UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR).cgColor)
                cell.checkGlassTypeImageView.image = UIImage(named: "checkCircleIcon")
                cell.checkGlassTypeImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
                cell.checkGlassTypeImageView.isHidden = false
            }else{
                 cell.setUpAppearance()
            }

             return cell
        }else{
        let cell: GlassTypeBothWindowsCollectionViewCell = glassTypePopUpCollectionView.dequeueReusableCell(withReuseIdentifier: "bothGlassTypeCollectionCell", for: indexPath) as! GlassTypeBothWindowsCollectionViewCell
            //UI Setup
            cell.setUpGestures()
            
            
            //Assign Values to Cell
            cell.selectGlassTypeCellDelegateProtocol = self
            cell.indexNo = indexPath.item

            cell.frontGlassTypeImageView.image = UIImage(named: self.glassTypeArr[indexPath.row].glassTypeFirstImageName)
            cell.sideOrRearGlassTypeImageView.image = UIImage(named: self.glassTypeArr[indexPath.row].glassTypeSecondImageName)
            
            cell.glassTypeLabel.text = self.glassTypeArr[indexPath.row].glassTypeName
            
            //if it is selected logic
            if self.glassTypeArr[indexPath.row].isGlassTypeSelected{
                cell.checkBothGlassTypeMainView.addBorders(borderWidth: 2.0, borderColor: UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR).cgColor)
                cell.checkGlassTypeImageView.image = UIImage(named: "checkCircleIcon")
                cell.checkGlassTypeImageView.tintColor = UIColor(rgb: AppConstants.CUSTOM_BLUE_COLOR)
                 cell.checkGlassTypeImageView.isHidden = false
            }else{
                 cell.setUpAppearance()
                
            }
            
            
            return cell
        }

    }
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 || indexPath.row == 1{
            let size = CGSize(width: self.glassTypePopUpCollectionView.bounds.width/2.5, height: 130)
            
            return size
        }else{
            let size = CGSize(width: self.glassTypePopUpCollectionView.bounds.width * 0.86 , height: 130)
            
            return size
        }
    }
    

    
}
