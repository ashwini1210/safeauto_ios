//
//  NotificationPopoverViewController.swift
//  SafeAuto
//
//  Created by ashwini on 27/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class NotificationPopoverViewController: BaseVC {
    
    @IBOutlet weak var viewAllButton: UIButton!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var notificationTableview: UITableView!
    @IBOutlet weak var headerView: UIView!
    var delegate: PopoverProtocol? = nil
    var navController: UINavigationController? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        registerTableview()
    }
    
    override func viewWillLayoutSubviews() {

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    /*
     * Initialize all view before use
     */
    func initViews(){
        AppUtils.addFontAwsomToButton(button: settingButton, fontAwsomName: "cog", textSize: 25, textcolour: UIColor.lightGray)
        headerView.addBottomBorderWithColor(color: UIColor.lightGray, width: 0.5)
        viewAllButton.addBottomBorderWithColor(color: UIColor().hexStringToUIColor(hex: "#007AFF"), width: 1)
    }
    
    /*
     * Register Tableview
     */
    func registerTableview(){
        notificationTableview.register(UINib(nibName: "NotificationPopoverCell", bundle: nil), forCellReuseIdentifier: "NotificationPopoverCell")
    }
    
    
    @IBAction func viewAllButoonClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            //Give call back to DashboardViewController after popup close, to remove background view from superview
            self.delegate!.onPopOverClose(isClosed: true)
        }
         let noficicationViewController = self.storyboard!.instantiateViewController(withIdentifier: "NoficicationListViewController") as! NoficicationListViewController
        self.navController!.pushViewController(noficicationViewController, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.delegate!.onPopOverClose(isClosed: true)
    }
    
    @IBAction func settingButtonClicked(_ sender: Any) {
        
    }
}


extension NotificationPopoverViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationPopoverCell", for: indexPath) as! NotificationPopoverCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true) {
            //Give call back to DashboardViewController after popup close, to remove background view from superview
            self.delegate!.onPopOverClose(isClosed: true)
        }
    }
    
}
