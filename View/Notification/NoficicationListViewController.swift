//
//  AllNoficicationViewController.swift
//  SafeAuto
//
//  Created by ashwini on 28/03/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class NoficicationListViewController: BaseVC {
    
    var noficicationViewModel : NotificationListViewModel? = nil
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var notificationListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        registerTableview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addDrawerButton()
    }
    
    /*
     * Initialize all view before use
     */
    func initViews(){
        noficicationViewModel = NotificationListViewModel(noticicationListViewController: self)
        if(noficicationViewModel != nil){
            noficicationViewModel!.resizeLabels()
            noficicationViewModel!.addBorderToView()
        }
    }
    
    /*
     * Register Tableview
     */
    func registerTableview(){
        notificationListTableView.register(UINib(nibName: "NotificationListViewCell", bundle: nil), forCellReuseIdentifier: "NotificationListViewCell")
    }
    
    
    
    /*
     * Method to add Buttons on Navigation Bar
     */
    func addDrawerButton(){
        guard noficicationViewModel != nil else {
            return
        }
        self.navigationItem.leftBarButtonItems = noficicationViewModel!.addLeftBarButtonItem()
        self.navigationItem.rightBarButtonItems = noficicationViewModel!.addRightBarButtonItem()
        
        guard let titleView = noficicationViewModel!.addTextInTitleBar() else{
            return
        }
        self.navigationItem.titleView = titleView
    }
    
    
    override func fontAwsomButtonClicked(sender: Any) {
        let button = sender as! UIButton
        if (button.tag == AppConstants.BACK_BUTTON){
            self.navigationController?.popViewController(animated: true)
        }
        else if(button.tag == AppConstants.NAVIGATION_ADDRESS_CARD_BUTTON){
            
        }
        else if(button.tag == AppConstants.NAVIGATION_NOTIFICATION_BUTTON){
            
        }
    }
    
    
}


/*
 * Extension Of UITableView
 */
extension NoficicationListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationListViewCell", for: indexPath) as! NotificationListViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
