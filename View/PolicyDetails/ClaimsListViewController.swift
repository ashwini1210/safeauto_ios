//
//  ClaimsListViewController.swift
//  PractiseMajesco
//
//  Created by ems on 19/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class ClaimsListViewController: BaseVC {
    //MARK: - Outlets
    
    @IBOutlet weak var claimsListTableView: UITableView!
    
    //MARK: - Variables
    var claimArr: [ClaimModel] = [ClaimModel]()
    
    //MARK: - Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        tempData()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - SetUpUI
    func setUpUI(){
        setUpTableView()
    }
    
    func tempData(){
        let claim1 = ClaimModel(claimName: "CLM1032", claimDate: "Jan 12, 2019", claimStatus: "Approved")
        let claim2 = ClaimModel(claimName: "CLM1045", claimDate: "Dec 26, 2018", claimStatus: "Settled")
        let claim3 = ClaimModel(claimName: "CLM1223", claimDate: "Nov 15, 2018", claimStatus: "Settled")
        
        self.claimArr.append(claim1)
        self.claimArr.append(claim2)
        self.claimArr.append(claim3)
        
    }
    
    func setUpTableView(){
        let nib = UINib(nibName: "ClaimsListTableViewCell", bundle: nil)
        self.claimsListTableView.register(nib, forCellReuseIdentifier: "claimsListCell")
        
        self.claimsListTableView.delegate = self
        self.claimsListTableView.dataSource = self
        
        self.claimsListTableView.rowHeight = 60
        self.claimsListTableView.estimatedRowHeight = 60
        
        self.claimsListTableView.tableFooterView = UIView()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ClaimsListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ClaimsListTableViewCell = claimsListTableView.dequeueReusableCell(withIdentifier: "claimsListCell") as! ClaimsListTableViewCell
        cell.claimIdLabel.text = self.claimArr[indexPath.row].claimName
        cell.claimDateLabel.text = self.claimArr[indexPath.row].claimDate
        cell.claimStatusLabel.text = self.claimArr[indexPath.row].claimStatus
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let VC1 = storyBoard.instantiateViewController(withIdentifier: "ClaimViewController") as! UINavigationController
        self.present(VC1, animated: true, completion: nil)
    }
}
