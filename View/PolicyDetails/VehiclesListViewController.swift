//
//  VehiclesListViewController.swift
//  PractiseMajesco
//
//  Created by ems on 19/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class VehiclesListViewController: BaseVC {
    
    //MARK: - Outlets
    @IBOutlet weak var vehiclesListTableView: UITableView!
    
    //MARK: - Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - UISetUp
    func setUpUI(){
        setUpTableView()
    }
    
    func setUpTableView(){
        
        let nib = UINib(nibName: "VehiclesListTableViewCell", bundle: nil)
        self.vehiclesListTableView.register(nib, forCellReuseIdentifier: "vehiclesListCell")
        
        self.vehiclesListTableView.delegate = self
        self.vehiclesListTableView.dataSource = self
        
        self.vehiclesListTableView.rowHeight = 60
        self.vehiclesListTableView.estimatedRowHeight = 60
        
        self.vehiclesListTableView.tableFooterView = UIView()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension VehiclesListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: VehiclesListTableViewCell = vehiclesListTableView.dequeueReusableCell(withIdentifier: "vehiclesListCell") as! VehiclesListTableViewCell
        return cell
    }
    
    
}
