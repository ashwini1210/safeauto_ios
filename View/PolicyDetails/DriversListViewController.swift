//
//  DriversListViewController.swift
//  PractiseMajesco
//
//  Created by ems on 19/03/19.
//  Copyright © 2019 Majesco. All rights reserved.
//

import UIKit

class DriversListViewController: BaseVC {
    //MARK: - Outlets
    @IBOutlet weak var driversListTableView: UITableView!
    
    //MARK: - Variables
    var driverArr: [DriverModel] = [DriverModel]()
    
    
    //MARK: - Controller Methods
    override func viewDidLoad() {
        super.viewDidLoad()
         setUpUI()
         temp()
        
        // Do any additional setup after loading the view.
    }
    
    func setUpUI(){
        setUpTableView()
    }
    
    func setUpTableView(){
        let nib = UINib(nibName: "DriversListTableViewCell", bundle: nil)
        self.driversListTableView.register(nib, forCellReuseIdentifier: "driversListCell")
        
        self.driversListTableView.delegate = self
        self.driversListTableView.dataSource = self
        
        self.driversListTableView.rowHeight = 60
        self.driversListTableView.estimatedRowHeight = 60
        
        self.driversListTableView.tableFooterView = UIView()
    }
 
    func temp(){
        let driver1 = DriverModel(name: "Ted Lawson", dateOfBirth: "11-09-1970", license: "NJ 123456789", profileImage: "driver1")
        let driver2 = DriverModel(name: "Jimmy Lawson", dateOfBirth: "12-01-1987", license: "NJ 543216789", profileImage: "driver2")
        let driver3 = DriverModel(name: "Martin Lawson", dateOfBirth: "16-06-1990", license: "NJ 123897645", profileImage: "driver3")
        
        self.driverArr.append(driver1)
        self.driverArr.append(driver2)
        self.driverArr.append(driver3)
        self.driversListTableView.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DriversListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DriversListTableViewCell = driversListTableView.dequeueReusableCell(withIdentifier: "driversListCell") as! DriversListTableViewCell
        cell.driversProfilePicImageView.image = UIImage(named: self.driverArr[indexPath.row].profileImage)
        cell.driversNameLabel.text = self.driverArr[indexPath.row].name
        cell.driversDateOfBirthLabel.text = self.driverArr[indexPath.row].dateOfBirth
        cell.driversLicenseLabel.text = self.driverArr[indexPath.row].license
        cell.driversEditBtn.titleLabel?.text = "temp"
        
        return cell
        
    }
    
    
    
}
