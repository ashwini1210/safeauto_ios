//
//  ConfirmContactViewController.swift
//  SafeAuto
//
//  Created by ashwini on 04/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ConfirmContactViewController: BaseVC {

    var delegate: PopoverProtocol? = nil
    var activeField: UITextField?
    var navController : UINavigationController? = nil
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mobileTextFiled: SkyFloatingLabelTextField!
    @IBOutlet weak var homePhoneTextFiled: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForKeyboardNotifications()
        self.registerTapGesture()
    }
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate!.onPopOverClose(isClosed: true)
            
            let VC = self.storyboard!.instantiateViewController(withIdentifier: "SubitClaimViewController") as! SubitClaimViewController
            self.navController!.pushViewController(VC, animated: false)
            
//            let subitClaimViewController = SubitClaimViewController()
//            self.present(subitClaimViewController, animated: true, completion: nil)
        }
    }
    
}

/*********************** CODE TO SCROLL VIEW ABOVE KEYBOARD WHEN IT OPENS ***********************/
extension ConfirmContactViewController: UITextFieldDelegate{
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height, right: 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if let activeField = self.activeField {
            if (!aRect.contains(activeField.frame.origin)){
                self.scrollView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        var info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.isScrollEnabled = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        activeField = nil
    }
}
