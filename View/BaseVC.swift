import UIKit
import SwiftyJSON
import Alamofire



class BaseVC: UIViewController,HttpResponseProtocol   {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var activityIndicator: UIActivityIndicatorView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //changeStatusBarColour()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func fontAwsomButtonClicked(sender: Any) {
        super.fontAwsomButtonClicked(sender: sender)
    }
    
    /*
     * Method to change colour of Staus bar
     */
    func changeStatusBarColour(){
        guard let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else {
            return
        }
        statusBarView.backgroundColor = UIColor.white
    }
    
    /*
     * Callback method of HttpresponseProtocol
     */
    func onHttpResponse(isSuccess: Bool, responseJson: JSON?, error: AnyObject?, anyIdentifier: String?) {
        
    }
    
    
    
    
    
    
    
}
