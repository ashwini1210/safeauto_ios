//
//  SubitClaimViewController.swift
//  SafeAuto
//
//  Created by ashwini on 09/04/19.
//  Copyright © 2019 com.majesco.ashwini. All rights reserved.
//

import UIKit

class SubitClaimViewController: BaseVC {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var claimLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    var submitViewModel : SubmitViewModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addDrawerButton()
    }
    
    func initViews(){
        submitViewModel = SubmitViewModel.init(subitClaimViewController: self)
        mainView.addShadowToView()
        changeClaimLabelToBold()
        changeTimeLabelToBold()
    }
    
    @IBAction func dashboardButtonClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let navController = storyBoard.instantiateViewController(withIdentifier: "HOME_VIEW_CONTROLLER") as! UINavigationController
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func policyDetailButtonClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "PolicyDetails", bundle: nil)
        let navController = storyBoard.instantiateViewController(withIdentifier: "DASHBOARD_NAV_CONTROLLER") as! UINavigationController
        self.present(navController, animated: true, completion: nil)
        AppPreference.getInstance().setString(AppConstants.PREVIOUS_SCREEN, value: AppConstants.SUBMIT_CLAIM_VC)
        
    }
    
    /*
     * Method to add Buttons on Navigation Bar
     */
    func addDrawerButton(){
        guard submitViewModel != nil else {
            return
        }
        self.navigationItem.leftBarButtonItems = submitViewModel!.addLeftBarButtonItem()
        self.navigationItem.rightBarButtonItems = submitViewModel!.addRightBarButtonItem()
        
        guard let titleView = submitViewModel!.addTextInTitleBar() else{
            return
        }
        self.navigationItem.titleView = titleView
    }
    
    override func fontAwsomButtonClicked(sender: Any) {
        let button = sender as! UIButton
        if (button.tag == AppConstants.BACK_BUTTON){
            //  self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }
        else if(button.tag == AppConstants.NAVIGATION_NOTIFICATION_BUTTON){
            
        }
        else if(button.tag == AppConstants.NAVIGATION_ADDRESS_CARD_BUTTON){
            
        }
    }
    
    func changeClaimLabelToBold(){
        let text1 = "Your claim "
        let text2 = "#CLM10321"
        let text3 = " has been opened."
        
        claimLabel.textColor = UIColor.black
        claimLabel.font = UIFont(name: "HelveticaNeue-Medium", size: CGFloat(15))
        claimLabel.textAlignment = NSTextAlignment.center
        let formattedString = NSMutableAttributedString()
        formattedString.normal("\(text1)", textSize: 15, textColor: UIColor.black).bold("\(text2)", textSize: 15, textColor: UIColor.black).normal("\(text3)", textSize: 15, textColor: UIColor.black)
        claimLabel.attributedText = formattedString
    }
    
    func changeTimeLabelToBold(){
        let text1 = "Your inspection is scheduled on "
        let text2 = "March 20, 2019"
        let text3 = " in between "
        let text4 = "9AM to 12PM"
        
        timeLabel.textColor = UIColor.black
        timeLabel.font = UIFont(name: "HelveticaNeue-Medium", size: CGFloat(15))
        timeLabel.textAlignment = NSTextAlignment.center
        let formattedString = NSMutableAttributedString()
        formattedString.normal("\(text1)", textSize: 15, textColor: UIColor.black).bold("\(text2)", textSize: 15, textColor: UIColor.black).normal("\(text3)", textSize: 15, textColor: UIColor.black).bold("\(text4)", textSize: 15, textColor: UIColor.black)
        timeLabel.attributedText = formattedString
    }
}
